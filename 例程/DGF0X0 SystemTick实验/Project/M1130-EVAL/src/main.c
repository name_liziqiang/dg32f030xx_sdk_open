/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "m1130_uart.h" 
#include "m1130_gpio.h" 
#include "m1130_rcc.h"	 
#include "misc.h"
#include "m1130_dma.h"
#include "m1130_utility_usart.h"
#include "m1130_tim.h"

void delay_ms(uint16_t ms)
{
	uint16_t i;
	for(; ms>0; ms--)
		for(i=0; i<4000; i++);
}

	#define 	LED_PIN		GPIO_Pin_20
	#define 	LED_PORT	GPIO0
void LED_Init(void)
{
 
    GPIO_SetPinMux(LED_PORT, LED_PIN, GPIO_FUNCTION_0);
	GPIO_SetPinDir(LED_PORT, LED_PIN, GPIO_Mode_OUT);
	GPIO_SetPin(LED_PORT, LED_PIN);
}
/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
  /* Decrement the TimingDelay variable */
    GPIO0->DT_TOG=GPIO_Pin_0;
}
int main()
{
    LED_Init();
    GPIO_SetPinMux(GPIO0, GPIO_Pin_0,GPIO_FUNCTION_0);
	 GPIO_SetPinDir(GPIO0, GPIO_Pin_0,GPIO_Mode_OUT);    
    RCC->SYSTICKCLKDIV=8;
    SysTick_Config(12000);//1ms
    SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK_Div8);
	while(1){
         
    }
}


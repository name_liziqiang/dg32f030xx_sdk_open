
========================================================================

** ELF Header Information

    File Name: .\Obj\1130_StdPeriph_Lib.axf

    Machine class: ELFCLASS32 (32-bit)
    Data encoding: ELFDATA2LSB (Little endian)
    Header version: EV_CURRENT (Current version)
    Operating System ABI: none
    ABI Version: 0
    File Type: ET_EXEC (Executable) (2)
    Machine: EM_ARM (ARM)

    Image Entry point: 0x00000101
    Flags: EF_ARM_HASENTRY + EF_ARM_ABI_FLOAT_SOFT (0x05000202)

    ARM ELF revision: 5 (ABI version 2)

    Conforms to Soft float procedure-call standard

    Built with
    Component: ARM Compiler 5.06 update 2 (build 183) Tool: armasm [4d35b2]
    Component: ARM Compiler 5.06 update 2 (build 183) Tool: armlink [4d35b7]

    Header size: 52 bytes (0x34)
    Program header entry size: 32 bytes (0x20)
    Section header entry size: 40 bytes (0x28)

    Program header entries: 1
    Section header entries: 19

    Program header offset: 100200 (0x00018768)
    Section header offset: 100232 (0x00018788)

    Section header string table index: 18

========================================================================

** Program header #0 (PT_LOAD) [PF_X + PF_W + PF_R + PF_ARM_ENTRY]
    Size : 1848 bytes (820 bytes in file)
    Virtual address: 0x00000000 (Alignment 8)


========================================================================

** Section #1 'ER_RESET' (SHT_PROGBITS) [SHF_ALLOC]
    Size   : 176 bytes (alignment 4)
    Address: 0x00000000


** Section #2 'ER_INT' (SHT_PROGBITS) [SHF_ALLOC + SHF_EXECINSTR]
    Size   : 80 bytes (alignment 4)
    Address: 0x000000b0

    $t
    .text
    Reset_Handler
        0x000000b0:    4804        .H      LDR      r0,[pc,#16] ; [0xc4] = 0x27d
        0x000000b2:    4780        .G      BLX      r0
        0x000000b4:    4804        .H      LDR      r0,[pc,#16] ; [0xc8] = 0x101
        0x000000b6:    4700        .G      BX       r0
    NMI_Handler
        0x000000b8:    e7fe        ..      B        NMI_Handler ; 0xb8
        0x000000ba:    e7fe        ..      B        0xba ; NMI_Handler + 2
        0x000000bc:    e7fe        ..      B        0xbc ; NMI_Handler + 4
        0x000000be:    e7fe        ..      B        0xbe ; NMI_Handler + 6
        0x000000c0:    e7fe        ..      B        0xc0 ; NMI_Handler + 8
    ADC0_IRQHandler
    BITCOPY_IRQHandler
    BOD_IRQHandler
    BOR_IRQHandler
    CMP_IRQHandler
    DMA_IRQHandler
    GPIO0_IRQHandler
    GPIO1_IRQHandler
    I2C0_IRQHandler
    LED_IRQHandler
    QSPI0_IRQHandler
    QSPI1_IRQHandler
    QSPI2_IRQHandler
    RTC_IRQHandler
    TIM16_IRQHandler
    TIM17_IRQHandler
    TIM1_IRQHandler
    TIM4_IRQHandler
    UART0_IRQHandler
    UART1_IRQHandler
    UART2_IRQHandler
    USB0_DMA_IRQHandler
    USB0_IRQHandler
    WKT_IRQHandler
        0x000000c2:    e7fe        ..      B        ADC0_IRQHandler ; 0xc2
    $d
        0x000000c4:    0000027d    }...    DCD    637
        0x000000c8:    00000101    ....    DCD    257
    $t
    i.HardFault_Handler
    HardFault_Handler
        0x000000cc:    e7fe        ..      B        HardFault_Handler ; 0xcc
    i.PendSV_Handler
    PendSV_Handler
        0x000000ce:    4770        pG      BX       lr
    i.SVC_Handler
    SVC_Handler
        0x000000d0:    4770        pG      BX       lr
        0x000000d2:    0000        ..      MOVS     r0,r0
    i.TIM15_IRQHandler
    TIM15_IRQHandler
        0x000000d4:    4903        .I      LDR      r1,[pc,#12] ; [0xe4] = 0x4000b000
        0x000000d6:    2000        .       MOVS     r0,#0
        0x000000d8:    8208        ..      STRH     r0,[r1,#0x10]
        0x000000da:    4803        .H      LDR      r0,[pc,#12] ; [0xe8] = 0x20000000
        0x000000dc:    6801        .h      LDR      r1,[r0,#0]
        0x000000de:    1c49        I.      ADDS     r1,r1,#1
        0x000000e0:    6001        .`      STR      r1,[r0,#0]
        0x000000e2:    4770        pG      BX       lr
    $d
        0x000000e4:    4000b000    ...@    DCD    1073786880
        0x000000e8:    20000000    ...     DCD    536870912
    $t
    i.commonDelay
    commonDelay
        0x000000ec:    b501        ..      PUSH     {r0,lr}
        0x000000ee:    e002        ..      B        0xf6 ; commonDelay + 10
        0x000000f0:    9800        ..      LDR      r0,[sp,#0]
        0x000000f2:    1e40        @.      SUBS     r0,r0,#1
        0x000000f4:    9000        ..      STR      r0,[sp,#0]
        0x000000f6:    9800        ..      LDR      r0,[sp,#0]
        0x000000f8:    2800        .(      CMP      r0,#0
        0x000000fa:    d1f9        ..      BNE      0xf0 ; commonDelay + 4
        0x000000fc:    bd08        ..      POP      {r3,pc}
        0x000000fe:    0000        ..      MOVS     r0,r0

** Section #3 'ER_LIB' (SHT_PROGBITS) [SHF_ALLOC + SHF_EXECINSTR]
    Size   : 120 bytes (alignment 4)
    Address: 0x00000100

    $t
    .ARM.Collect$$$$00000000
    .ARM.Collect$$$$00000001
    __main
    _main_stk
        0x00000100:    4803        .H      LDR      r0,__lit__00000000 ; [0x110] = 0x20000408
        0x00000102:    4685        .F      MOV      sp,r0
    .ARM.Collect$$$$00000004
    _main_scatterload
        0x00000104:    f000f806    ....    BL       __scatterload ; 0x114
    .ARM.Collect$$$$00000008
    .ARM.Collect$$$$0000000A
    .ARM.Collect$$$$0000000B
    __main_after_scatterload
    _main_clock
    _main_cpp_init
    _main_init
        0x00000108:    4800        .H      LDR      r0,[pc,#0] ; [0x10c] = 0x2d9
        0x0000010a:    4700        .G      BX       r0
    $d
        0x0000010c:    000002d9    ....    DCD    729
    .ARM.Collect$$$$00002712
    __lit__00000000
    .ARM.Collect$$$$0000000D
    .ARM.Collect$$$$0000000F
    __rt_final_cpp
    __rt_final_exit
        0x00000110:    20000408    ...     DCD    536871944
    $t
    .text
    __scatterload
    __scatterload_rt2
        0x00000114:    4c06        .L      LDR      r4,[pc,#24] ; [0x130] = 0x158
        0x00000116:    2501        .%      MOVS     r5,#1
        0x00000118:    4e06        .N      LDR      r6,[pc,#24] ; [0x134] = 0x178
        0x0000011a:    e005        ..      B        0x128 ; __scatterload + 20
        0x0000011c:    68e3        .h      LDR      r3,[r4,#0xc]
        0x0000011e:    cc07        ..      LDM      r4!,{r0-r2}
        0x00000120:    432b        +C      ORRS     r3,r3,r5
        0x00000122:    3c0c        .<      SUBS     r4,r4,#0xc
        0x00000124:    4798        .G      BLX      r3
        0x00000126:    3410        .4      ADDS     r4,r4,#0x10
        0x00000128:    42b4        .B      CMP      r4,r6
        0x0000012a:    d3f7        ..      BCC      0x11c ; __scatterload + 8
        0x0000012c:    f7ffffec    ....    BL       __main_after_scatterload ; 0x108
    $d
        0x00000130:    00000158    X...    DCD    344
        0x00000134:    00000178    x...    DCD    376
    $t
    i.__scatterload_copy
    __scatterload_copy
        0x00000138:    e002        ..      B        0x140 ; __scatterload_copy + 8
        0x0000013a:    c808        ..      LDM      r0!,{r3}
        0x0000013c:    1f12        ..      SUBS     r2,r2,#4
        0x0000013e:    c108        ..      STM      r1!,{r3}
        0x00000140:    2a00        .*      CMP      r2,#0
        0x00000142:    d1fa        ..      BNE      0x13a ; __scatterload_copy + 2
        0x00000144:    4770        pG      BX       lr
    i.__scatterload_null
    __scatterload_null
        0x00000146:    4770        pG      BX       lr
    i.__scatterload_zeroinit
    __scatterload_zeroinit
        0x00000148:    2000        .       MOVS     r0,#0
        0x0000014a:    e001        ..      B        0x150 ; __scatterload_zeroinit + 8
        0x0000014c:    c101        ..      STM      r1!,{r0}
        0x0000014e:    1f12        ..      SUBS     r2,r2,#4
        0x00000150:    2a00        .*      CMP      r2,#0
        0x00000152:    d1fb        ..      BNE      0x14c ; __scatterload_zeroinit + 4
        0x00000154:    4770        pG      BX       lr
        0x00000156:    0000        ..      MOVS     r0,r0
    $d.realdata
    Region$$Table$$Base
        0x00000158:    00000330    0...    DCD    816
        0x0000015c:    20000000    ...     DCD    536870912
        0x00000160:    00000004    ....    DCD    4
        0x00000164:    00000138    8...    DCD    312
        0x00000168:    00000334    4...    DCD    820
        0x0000016c:    20000004    ...     DCD    536870916
        0x00000170:    00000404    ....    DCD    1028
        0x00000174:    00000148    H...    DCD    328
    Region$$Table$$Limit

** Section #4 'ER_ANY' (SHT_PROGBITS) [SHF_ALLOC + SHF_EXECINSTR]
    Size   : 440 bytes (alignment 4)
    Address: 0x00000178

    $t
    i.GPIO_SetPin
    GPIO_SetPin
        0x00000178:    6041        A`      STR      r1,[r0,#4]
        0x0000017a:    4770        pG      BX       lr
    i.GPIO_SetPinDir
    GPIO_SetPinDir
        0x0000017c:    b570        p.      PUSH     {r4-r6,lr}
        0x0000017e:    4605        .F      MOV      r5,r0
        0x00000180:    460c        .F      MOV      r4,r1
        0x00000182:    4616        .F      MOV      r6,r2
        0x00000184:    2200        ."      MOVS     r2,#0
        0x00000186:    4621        !F      MOV      r1,r4
        0x00000188:    4628        (F      MOV      r0,r5
        0x0000018a:    f000f819    ....    BL       GPIO_SetPinMux ; 0x1c0
        0x0000018e:    4809        .H      LDR      r0,[pc,#36] ; [0x1b4] = 0x40020000
        0x00000190:    4909        .I      LDR      r1,[pc,#36] ; [0x1b8] = 0x40028000
        0x00000192:    4a0a        .J      LDR      r2,[pc,#40] ; [0x1bc] = 0x40038000
        0x00000194:    2e01        ..      CMP      r6,#1
        0x00000196:    d005        ..      BEQ      0x1a4 ; GPIO_SetPinDir + 40
        0x00000198:    2e00        ..      CMP      r6,#0
        0x0000019a:    d102        ..      BNE      0x1a2 ; GPIO_SetPinDir + 38
        0x0000019c:    4285        .B      CMP      r5,r0
        0x0000019e:    d107        ..      BNE      0x1b0 ; GPIO_SetPinDir + 52
        0x000001a0:    608c        .`      STR      r4,[r1,#8]
        0x000001a2:    bd70        p.      POP      {r4-r6,pc}
        0x000001a4:    4285        .B      CMP      r5,r0
        0x000001a6:    d101        ..      BNE      0x1ac ; GPIO_SetPinDir + 48
        0x000001a8:    604c        L`      STR      r4,[r1,#4]
        0x000001aa:    bd70        p.      POP      {r4-r6,pc}
        0x000001ac:    6054        T`      STR      r4,[r2,#4]
        0x000001ae:    bd70        p.      POP      {r4-r6,pc}
        0x000001b0:    6094        .`      STR      r4,[r2,#8]
        0x000001b2:    bd70        p.      POP      {r4-r6,pc}
    $d
        0x000001b4:    40020000    ...@    DCD    1073872896
        0x000001b8:    40028000    ...@    DCD    1073905664
        0x000001bc:    40038000    ...@    DCD    1073971200
    $t
    i.GPIO_SetPinMux
    GPIO_SetPinMux
        0x000001c0:    b530        0.      PUSH     {r4,r5,lr}
        0x000001c2:    4b17        .K      LDR      r3,[pc,#92] ; [0x220] = 0x40020000
        0x000001c4:    4298        .B      CMP      r0,r3
        0x000001c6:    d114        ..      BNE      0x1f2 ; GPIO_SetPinMux + 50
        0x000001c8:    2000        .       MOVS     r0,#0
        0x000001ca:    4c16        .L      LDR      r4,[pc,#88] ; [0x224] = 0x40001000
        0x000001cc:    460b        .F      MOV      r3,r1
        0x000001ce:    40c3        .@      LSRS     r3,r3,r0
        0x000001d0:    07db        ..      LSLS     r3,r3,#31
        0x000001d2:    0fdb        ..      LSRS     r3,r3,#31
        0x000001d4:    2b00        .+      CMP      r3,#0
        0x000001d6:    d008        ..      BEQ      0x1ea ; GPIO_SetPinMux + 42
        0x000001d8:    0083        ..      LSLS     r3,r0,#2
        0x000001da:    191b        ..      ADDS     r3,r3,r4
        0x000001dc:    681d        .h      LDR      r5,[r3,#0]
        0x000001de:    08ed        ..      LSRS     r5,r5,#3
        0x000001e0:    00ed        ..      LSLS     r5,r5,#3
        0x000001e2:    601d        .`      STR      r5,[r3,#0]
        0x000001e4:    681d        .h      LDR      r5,[r3,#0]
        0x000001e6:    4315        .C      ORRS     r5,r5,r2
        0x000001e8:    601d        .`      STR      r5,[r3,#0]
        0x000001ea:    1c40        @.      ADDS     r0,r0,#1
        0x000001ec:    2820         (      CMP      r0,#0x20
        0x000001ee:    d3ed        ..      BCC      0x1cc ; GPIO_SetPinMux + 12
        0x000001f0:    bd30        0.      POP      {r4,r5,pc}
        0x000001f2:    2000        .       MOVS     r0,#0
        0x000001f4:    4c0b        .L      LDR      r4,[pc,#44] ; [0x224] = 0x40001000
        0x000001f6:    3480        .4      ADDS     r4,r4,#0x80
        0x000001f8:    460b        .F      MOV      r3,r1
        0x000001fa:    40c3        .@      LSRS     r3,r3,r0
        0x000001fc:    07db        ..      LSLS     r3,r3,#31
        0x000001fe:    0fdb        ..      LSRS     r3,r3,#31
        0x00000200:    2b00        .+      CMP      r3,#0
        0x00000202:    d008        ..      BEQ      0x216 ; GPIO_SetPinMux + 86
        0x00000204:    0083        ..      LSLS     r3,r0,#2
        0x00000206:    191b        ..      ADDS     r3,r3,r4
        0x00000208:    681d        .h      LDR      r5,[r3,#0]
        0x0000020a:    08ed        ..      LSRS     r5,r5,#3
        0x0000020c:    00ed        ..      LSLS     r5,r5,#3
        0x0000020e:    601d        .`      STR      r5,[r3,#0]
        0x00000210:    681d        .h      LDR      r5,[r3,#0]
        0x00000212:    4315        .C      ORRS     r5,r5,r2
        0x00000214:    601d        .`      STR      r5,[r3,#0]
        0x00000216:    1c40        @.      ADDS     r0,r0,#1
        0x00000218:    2820         (      CMP      r0,#0x20
        0x0000021a:    d3ed        ..      BCC      0x1f8 ; GPIO_SetPinMux + 56
        0x0000021c:    bd30        0.      POP      {r4,r5,pc}
    $d
        0x0000021e:    0000        ..      DCW    0
        0x00000220:    40020000    ...@    DCD    1073872896
        0x00000224:    40001000    ...@    DCD    1073745920
    $t
    i.LED_Init
    LED_Init
        0x00000228:    b570        p.      PUSH     {r4-r6,lr}
        0x0000022a:    2401        .$      MOVS     r4,#1
        0x0000022c:    4d08        .M      LDR      r5,[pc,#32] ; [0x250] = 0x40020000
        0x0000022e:    0524        $.      LSLS     r4,r4,#20
        0x00000230:    2200        ."      MOVS     r2,#0
        0x00000232:    4621        !F      MOV      r1,r4
        0x00000234:    4628        (F      MOV      r0,r5
        0x00000236:    f7ffffc3    ....    BL       GPIO_SetPinMux ; 0x1c0
        0x0000023a:    2201        ."      MOVS     r2,#1
        0x0000023c:    4621        !F      MOV      r1,r4
        0x0000023e:    4628        (F      MOV      r0,r5
        0x00000240:    f7ffff9c    ....    BL       GPIO_SetPinDir ; 0x17c
        0x00000244:    4621        !F      MOV      r1,r4
        0x00000246:    4628        (F      MOV      r0,r5
        0x00000248:    f7ffff96    ....    BL       GPIO_SetPin ; 0x178
        0x0000024c:    bd70        p.      POP      {r4-r6,pc}
    $d
        0x0000024e:    0000        ..      DCW    0
        0x00000250:    40020000    ...@    DCD    1073872896
    $t
    i.SysTick_CLKSourceConfig
    SysTick_CLKSourceConfig
        0x00000254:    2204        ."      MOVS     r2,#4
        0x00000256:    4905        .I      LDR      r1,[pc,#20] ; [0x26c] = 0xe000e000
        0x00000258:    2804        .(      CMP      r0,#4
        0x0000025a:    d003        ..      BEQ      0x264 ; SysTick_CLKSourceConfig + 16
        0x0000025c:    6908        .i      LDR      r0,[r1,#0x10]
        0x0000025e:    4390        .C      BICS     r0,r0,r2
        0x00000260:    6108        .a      STR      r0,[r1,#0x10]
        0x00000262:    4770        pG      BX       lr
        0x00000264:    6908        .i      LDR      r0,[r1,#0x10]
        0x00000266:    4310        .C      ORRS     r0,r0,r2
        0x00000268:    6108        .a      STR      r0,[r1,#0x10]
        0x0000026a:    4770        pG      BX       lr
    $d
        0x0000026c:    e000e000    ....    DCD    3758153728
    $t
    i.SysTick_Handler
    SysTick_Handler
        0x00000270:    4901        .I      LDR      r1,[pc,#4] ; [0x278] = 0x40020000
        0x00000272:    2001        .       MOVS     r0,#1
        0x00000274:    60c8        .`      STR      r0,[r1,#0xc]
        0x00000276:    4770        pG      BX       lr
    $d
        0x00000278:    40020000    ...@    DCD    1073872896
    $t
    i.SystemInit
    SystemInit
        0x0000027c:    b510        ..      PUSH     {r4,lr}
        0x0000027e:    2000        .       MOVS     r0,#0
        0x00000280:    2101        .!      MOVS     r1,#1
        0x00000282:    4a11        .J      LDR      r2,[pc,#68] ; [0x2c8] = 0x818
        0x00000284:    0789        ..      LSLS     r1,r1,#30
        0x00000286:    614a        Ja      STR      r2,[r1,#0x14]
        0x00000288:    4b10        .K      LDR      r3,[pc,#64] ; [0x2cc] = 0x40000080
        0x0000028a:    2204        ."      MOVS     r2,#4
        0x0000028c:    601a        .`      STR      r2,[r3,#0]
        0x0000028e:    4a10        .J      LDR      r2,[pc,#64] ; [0x2d0] = 0x40000200
        0x00000290:    6893        .h      LDR      r3,[r2,#8]
        0x00000292:    24ff        .$      MOVS     r4,#0xff
        0x00000294:    3412        .4      ADDS     r4,r4,#0x12
        0x00000296:    43a3        .C      BICS     r3,r3,r4
        0x00000298:    6093        .`      STR      r3,[r2,#8]
        0x0000029a:    4a0e        .J      LDR      r2,[pc,#56] ; [0x2d4] = 0x80000016
        0x0000029c:    630a        .c      STR      r2,[r1,#0x30]
        0x0000029e:    13ca        ..      ASRS     r2,r1,#15
        0x000002a0:    e003        ..      B        0x2aa ; SystemInit + 46
        0x000002a2:    4603        .F      MOV      r3,r0
        0x000002a4:    1c40        @.      ADDS     r0,r0,#1
        0x000002a6:    4293        .B      CMP      r3,r2
        0x000002a8:    d202        ..      BCS      0x2b0 ; SystemInit + 52
        0x000002aa:    6b4b        Kk      LDR      r3,[r1,#0x34]
        0x000002ac:    2b01        .+      CMP      r3,#1
        0x000002ae:    d1f8        ..      BNE      0x2a2 ; SystemInit + 38
        0x000002b0:    207d        }       MOVS     r0,#0x7d
        0x000002b2:    00c0        ..      LSLS     r0,r0,#3
        0x000002b4:    f7ffff1a    ....    BL       commonDelay ; 0xec
        0x000002b8:    4804        .H      LDR      r0,[pc,#16] ; [0x2cc] = 0x40000080
        0x000002ba:    2101        .!      MOVS     r1,#1
        0x000002bc:    3840        @8      SUBS     r0,r0,#0x40
        0x000002be:    6101        .a      STR      r1,[r0,#0x10]
        0x000002c0:    2200        ."      MOVS     r2,#0
        0x000002c2:    6142        Ba      STR      r2,[r0,#0x14]
        0x000002c4:    6141        Aa      STR      r1,[r0,#0x14]
        0x000002c6:    bd10        ..      POP      {r4,pc}
    $d
        0x000002c8:    00000818    ....    DCD    2072
        0x000002cc:    40000080    ...@    DCD    1073741952
        0x000002d0:    40000200    ...@    DCD    1073742336
        0x000002d4:    80000016    ....    DCD    2147483670
    $t
    i.main
    main
        0x000002d8:    f7ffffa6    ....    BL       LED_Init ; 0x228
        0x000002dc:    4c0f        .L      LDR      r4,[pc,#60] ; [0x31c] = 0x40020000
        0x000002de:    2200        ."      MOVS     r2,#0
        0x000002e0:    2101        .!      MOVS     r1,#1
        0x000002e2:    4620         F      MOV      r0,r4
        0x000002e4:    f7ffff6c    ..l.    BL       GPIO_SetPinMux ; 0x1c0
        0x000002e8:    2201        ."      MOVS     r2,#1
        0x000002ea:    4611        .F      MOV      r1,r2
        0x000002ec:    4620         F      MOV      r0,r4
        0x000002ee:    f7ffff45    ..E.    BL       GPIO_SetPinDir ; 0x17c
        0x000002f2:    490b        .I      LDR      r1,[pc,#44] ; [0x320] = 0x40000080
        0x000002f4:    2008        .       MOVS     r0,#8
        0x000002f6:    6208        .b      STR      r0,[r1,#0x20]
        0x000002f8:    490a        .I      LDR      r1,[pc,#40] ; [0x324] = 0x2edf
        0x000002fa:    480b        .H      LDR      r0,[pc,#44] ; [0x328] = 0xe000e000
        0x000002fc:    6141        Aa      STR      r1,[r0,#0x14]
        0x000002fe:    490b        .I      LDR      r1,[pc,#44] ; [0x32c] = 0xe000ed04
        0x00000300:    2207        ."      MOVS     r2,#7
        0x00000302:    69cb        .i      LDR      r3,[r1,#0x1c]
        0x00000304:    021b        ..      LSLS     r3,r3,#8
        0x00000306:    0a1b        ..      LSRS     r3,r3,#8
        0x00000308:    0444        D.      LSLS     r4,r0,#17
        0x0000030a:    4323        #C      ORRS     r3,r3,r4
        0x0000030c:    61cb        .a      STR      r3,[r1,#0x1c]
        0x0000030e:    2100        .!      MOVS     r1,#0
        0x00000310:    6181        .a      STR      r1,[r0,#0x18]
        0x00000312:    6102        .a      STR      r2,[r0,#0x10]
        0x00000314:    1f48        H.      SUBS     r0,r1,#5
        0x00000316:    f7ffff9d    ....    BL       SysTick_CLKSourceConfig ; 0x254
        0x0000031a:    e7fe        ..      B        0x31a ; main + 66
    $d
        0x0000031c:    40020000    ...@    DCD    1073872896
        0x00000320:    40000080    ...@    DCD    1073741952
        0x00000324:    00002edf    ....    DCD    11999
        0x00000328:    e000e000    ....    DCD    3758153728
        0x0000032c:    e000ed04    ....    DCD    3758157060

** Section #5 'RW_IRAM1' (SHT_PROGBITS) [SHF_ALLOC + SHF_WRITE]
    Size   : 4 bytes (alignment 4)
    Address: 0x20000000


** Section #6 'RW_IRAM1' (SHT_NOBITS) [SHF_ALLOC + SHF_WRITE]
    Size   : 1028 bytes (alignment 8)
    Address: 0x20000004


** Section #7 '.debug_abbrev' (SHT_PROGBITS)
    Size   : 1476 bytes


** Section #8 '.debug_frame' (SHT_PROGBITS)
    Size   : 868 bytes


** Section #9 '.debug_info' (SHT_PROGBITS)
    Size   : 23172 bytes


** Section #10 '.debug_line' (SHT_PROGBITS)
    Size   : 2576 bytes


** Section #11 '.debug_loc' (SHT_PROGBITS)
    Size   : 628 bytes


** Section #12 '.debug_macinfo' (SHT_PROGBITS)
    Size   : 54980 bytes


** Section #13 '.debug_pubnames' (SHT_PROGBITS)
    Size   : 495 bytes


** Section #14 '.symtab' (SHT_SYMTAB)
    Size   : 2448 bytes (alignment 4)
    String table #15 '.strtab'
    Last local symbol no. 90


** Section #15 '.strtab' (SHT_STRTAB)
    Size   : 2496 bytes


** Section #16 '.note' (SHT_NOTE)
    Size   : 40 bytes (alignment 4)


** Section #17 '.comment' (SHT_PROGBITS)
    Size   : 9972 bytes


** Section #18 '.shstrtab' (SHT_STRTAB)
    Size   : 176 bytes



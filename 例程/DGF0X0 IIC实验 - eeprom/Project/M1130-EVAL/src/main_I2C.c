/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "m1130_uart.h"
#include "m1130_rcc.h" 
#include "m1130_gpio.h" 
//#include "m1130_eval_i2c.h"
#include "m1130_utility_usart.h"
#include "m1130_utility_i2c.h"

// #define EEPROM_2kb				//32 pages of 8 bytes			2kb
// #define EEPROM_4kb				//32 pages of 16 bytes   4kb
// #define EEPROM_8kb				//64 pages of 16 bytes 			8kb
// #define EEPROM_16kb				//128 pages of 16 bytes  16kb 
#define EEPROM_256kb				//512 pages of 64 bytes  256kb 

#if defined (EEPROM_2kb)
	#define EEPROM_INTERNEL_ADDRES_BITS			8  //eeprom内部寻址位数
	#define EEPROM_PAGE_SIZE								8  //1页多少字节

#elif defined (EEPROM_4kb)
	#define EEPROM_INTERNEL_ADDRES_BITS			9
	#define EEPROM_PAGE_SIZE								16

#elif defined (EEPROM_8kb)
	#define EEPROM_INTERNEL_ADDRES_BITS			10
	#define EEPROM_PAGE_SIZE								16

#elif defined (EEPROM_16kb)
	#define EEPROM_INTERNEL_ADDRES_BITS			11
	#define EEPROM_PAGE_SIZE								16

#elif defined (EEPROM_256kb)
	#define EEPROM_INTERNEL_ADDRES_BITS			15
	#define EEPROM_PAGE_SIZE								64
#endif

#define EEPROM_DEVICE_ADDRESS (0xa0 >> 1)//the last bit is r/w bit.

#define BYTENUM    16
#define SIM_FLAG                0x4000fffc
__align(4) char wbuf[BYTENUM];
__align(4) char rbuf[BYTENUM];
//int i2c_eeprom(void)
//{
//	int i;
//	int j = 0;
//  int count = 0;

//  for (i = 0; i < BYTENUM; i++)
//		wbuf[i] = (char)((0x2 + i) & 0xff);
//	for (i = 0; i < BYTENUM; i++)
//		rbuf[i] = 0xcc;

//  i2cOpen("24c02", I2C_GP0_0);

////#if 1

//  i2cWrite(wbuf, 0x0, BYTENUM);
// commonDelay(100000);
//	i2cRead(rbuf,0x0, BYTENUM);
////#elif 0
////	i2cWrite(wbuf, 1 << EEPROM_INTERNEL_ADDRES_BITS, BYTENUM);//和0地址数据比较是否相等,若相等,EEPROM_INTERNEL_ADDRES_BITS则为内部寻址的真实位数
////  // commonDelay(10000*8);
////	i2cRead(rbuf,0x0, BYTENUM);
////#else  //loop test
////  while (1) {
////    for (i = 0; i < BYTENUM; i++)
////      rbuf[i] = 0xcc;
////    i2cWrite(wbuf, 0x0, BYTENUM);
////    // commonDelay(10000*8);
////    i2cRead(rbuf, 0x0, BYTENUM);
////    for (i = 0; i < BYTENUM; i++)
////      if (wbuf[i] != rbuf[i])
////        goto ERR;
////    printf("i2c cmp correct,count=%d\r\n", count++);
////  }
////#endif

//  for (i = 0; i < BYTENUM; i++)
//    if (wbuf[i] != rbuf[i])
//      goto ERR;
//  i2cClose();
//  printf("i2c cmp correct\r\n");
//  outl(0x5555aaaa, SIM_FLAG);
//	return 0;

//	ERR:
//		i2cClose();
//		printf("read and write not match!\r\r\n");
//		return -1;
//}

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main()
{   
  UART_Configuration(UART1, 115200, UART1_GP2);
	printf("\r\n-----------------|- M1130 Standard Peripheral Library Driver -|-----------------\r\n");
    I2C_Configuration(I2C_SPEED_STD,I2C_GP0_A);
    	GPIO_ConfigPull(GPIO0,GPIO_Pin_0|GPIO_Pin_1, GPIO_PULL_UP);

    
     for (int i = 0; i < BYTENUM; i++)
		wbuf[i] = (char)((0x2 + i) & 0xff);
	for (int i = 0; i < BYTENUM; i++)
		rbuf[i] = 0xcc;
	while (1)
    {
        I2C_Write(0xA0>>1,0,1,(uint8_t *)wbuf,BYTENUM);
        commonDelay(100000);
        commonDelay(100000);
        I2C_Read(0xa0>>1,0,1,(uint8_t *)rbuf,BYTENUM);
        
     for (int i = 0; i < BYTENUM; i++)
    if (wbuf[i] != rbuf[i])
      goto ERR;
      printf("i2c cmp correct\r\n");
	return 0;

	ERR:
		printf("read and write not match!\r\r\n");
		return -1;
    }
	
}




========================================================================

** ELF Header Information

    File Name: .\Obj\1130_StdPeriph_Lib.axf

    Machine class: ELFCLASS32 (32-bit)
    Data encoding: ELFDATA2LSB (Little endian)
    Header version: EV_CURRENT (Current version)
    Operating System ABI: none
    ABI Version: 0
    File Type: ET_EXEC (Executable) (2)
    Machine: EM_ARM (ARM)

    Image Entry point: 0x000000e9
    Flags: EF_ARM_HASENTRY + EF_ARM_ABI_FLOAT_SOFT (0x05000202)

    ARM ELF revision: 5 (ABI version 2)

    Conforms to Soft float procedure-call standard

    Built with
    Component: ARM Compiler 5.06 update 2 (build 183) Tool: armasm [4d35b2]
    Component: ARM Compiler 5.06 update 2 (build 183) Tool: armlink [4d35b7]

    Header size: 52 bytes (0x34)
    Program header entry size: 32 bytes (0x20)
    Section header entry size: 40 bytes (0x28)

    Program header entries: 1
    Section header entries: 18

    Program header offset: 203016 (0x00031908)
    Section header offset: 203048 (0x00031928)

    Section header string table index: 17

========================================================================

** Program header #0 (PT_LOAD) [PF_X + PF_W + PF_R + PF_ARM_ENTRY]
    Size : 2064 bytes (1040 bytes in file)
    Virtual address: 0x00000000 (Alignment 8)


========================================================================

** Section #1 'ER_RESET' (SHT_PROGBITS) [SHF_ALLOC]
    Size   : 176 bytes (alignment 4)
    Address: 0x00000000


** Section #2 'ER_INT' (SHT_PROGBITS) [SHF_ALLOC + SHF_EXECINSTR]
    Size   : 56 bytes (alignment 4)
    Address: 0x000000b0

    $t
    .text
    Reset_Handler
        0x000000b0:    4804        .H      LDR      r0,[pc,#16] ; [0xc4] = 0x2b9
        0x000000b2:    4780        .G      BLX      r0
        0x000000b4:    4804        .H      LDR      r0,[pc,#16] ; [0xc8] = 0xe9
        0x000000b6:    4700        .G      BX       r0
    NMI_Handler
        0x000000b8:    e7fe        ..      B        NMI_Handler ; 0xb8
        0x000000ba:    e7fe        ..      B        0xba ; NMI_Handler + 2
        0x000000bc:    e7fe        ..      B        0xbc ; NMI_Handler + 4
        0x000000be:    e7fe        ..      B        0xbe ; NMI_Handler + 6
        0x000000c0:    e7fe        ..      B        0xc0 ; NMI_Handler + 8
    ADC0_IRQHandler
    BITCOPY_IRQHandler
    BOD_IRQHandler
    BOR_IRQHandler
    CMP_IRQHandler
    DMA_IRQHandler
    GPIO0_IRQHandler
    GPIO1_IRQHandler
    I2C0_IRQHandler
    LED_IRQHandler
    QSPI0_IRQHandler
    QSPI1_IRQHandler
    QSPI2_IRQHandler
    RTC_IRQHandler
    TIM16_IRQHandler
    TIM17_IRQHandler
    TIM1_IRQHandler
    TIM4_IRQHandler
    UART0_IRQHandler
    UART1_IRQHandler
    UART2_IRQHandler
    USB0_DMA_IRQHandler
    USB0_IRQHandler
    WKT_IRQHandler
        0x000000c2:    e7fe        ..      B        ADC0_IRQHandler ; 0xc2
    $d
        0x000000c4:    000002b9    ....    DCD    697
        0x000000c8:    000000e9    ....    DCD    233
    $t
    i.HardFault_Handler
    HardFault_Handler
        0x000000cc:    e7fe        ..      B        HardFault_Handler ; 0xcc
    i.PendSV_Handler
    PendSV_Handler
        0x000000ce:    4770        pG      BX       lr
    i.SVC_Handler
    SVC_Handler
        0x000000d0:    4770        pG      BX       lr
    i.SysTick_Handler
    SysTick_Handler
        0x000000d2:    4770        pG      BX       lr
    i.commonDelay
    commonDelay
        0x000000d4:    b501        ..      PUSH     {r0,lr}
        0x000000d6:    e002        ..      B        0xde ; commonDelay + 10
        0x000000d8:    9800        ..      LDR      r0,[sp,#0]
        0x000000da:    1e40        @.      SUBS     r0,r0,#1
        0x000000dc:    9000        ..      STR      r0,[sp,#0]
        0x000000de:    9800        ..      LDR      r0,[sp,#0]
        0x000000e0:    2800        .(      CMP      r0,#0
        0x000000e2:    d1f9        ..      BNE      0xd8 ; commonDelay + 4
        0x000000e4:    bd08        ..      POP      {r3,pc}
        0x000000e6:    0000        ..      MOVS     r0,r0

** Section #3 'ER_LIB' (SHT_PROGBITS) [SHF_ALLOC + SHF_EXECINSTR]
    Size   : 104 bytes (alignment 4)
    Address: 0x000000e8

    $t
    .ARM.Collect$$$$00000000
    .ARM.Collect$$$$00000001
    __main
    _main_stk
        0x000000e8:    4803        .H      LDR      r0,__lit__00000000 ; [0xf8] = 0x20000400
        0x000000ea:    4685        .F      MOV      sp,r0
    .ARM.Collect$$$$00000004
    _main_scatterload
        0x000000ec:    f000f806    ....    BL       __scatterload ; 0xfc
    .ARM.Collect$$$$00000008
    .ARM.Collect$$$$0000000A
    .ARM.Collect$$$$0000000B
    __main_after_scatterload
    _main_clock
    _main_cpp_init
    _main_init
        0x000000f0:    4800        .H      LDR      r0,[pc,#0] ; [0xf4] = 0x3ed
        0x000000f2:    4700        .G      BX       r0
    $d
        0x000000f4:    000003ed    ....    DCD    1005
    .ARM.Collect$$$$00002712
    __lit__00000000
    .ARM.Collect$$$$0000000D
    .ARM.Collect$$$$0000000F
    __rt_final_cpp
    __rt_final_exit
        0x000000f8:    20000400    ...     DCD    536871936
    $t
    .text
    __scatterload
    __scatterload_rt2
        0x000000fc:    4c06        .L      LDR      r4,[pc,#24] ; [0x118] = 0x140
        0x000000fe:    2501        .%      MOVS     r5,#1
        0x00000100:    4e06        .N      LDR      r6,[pc,#24] ; [0x11c] = 0x150
        0x00000102:    e005        ..      B        0x110 ; __scatterload + 20
        0x00000104:    68e3        .h      LDR      r3,[r4,#0xc]
        0x00000106:    cc07        ..      LDM      r4!,{r0-r2}
        0x00000108:    432b        +C      ORRS     r3,r3,r5
        0x0000010a:    3c0c        .<      SUBS     r4,r4,#0xc
        0x0000010c:    4798        .G      BLX      r3
        0x0000010e:    3410        .4      ADDS     r4,r4,#0x10
        0x00000110:    42b4        .B      CMP      r4,r6
        0x00000112:    d3f7        ..      BCC      0x104 ; __scatterload + 8
        0x00000114:    f7ffffec    ....    BL       __main_after_scatterload ; 0xf0
    $d
        0x00000118:    00000140    @...    DCD    320
        0x0000011c:    00000150    P...    DCD    336
    $t
    i.__scatterload_copy
    __scatterload_copy
        0x00000120:    e002        ..      B        0x128 ; __scatterload_copy + 8
        0x00000122:    c808        ..      LDM      r0!,{r3}
        0x00000124:    1f12        ..      SUBS     r2,r2,#4
        0x00000126:    c108        ..      STM      r1!,{r3}
        0x00000128:    2a00        .*      CMP      r2,#0
        0x0000012a:    d1fa        ..      BNE      0x122 ; __scatterload_copy + 2
        0x0000012c:    4770        pG      BX       lr
    i.__scatterload_null
    __scatterload_null
        0x0000012e:    4770        pG      BX       lr
    i.__scatterload_zeroinit
    __scatterload_zeroinit
        0x00000130:    2000        .       MOVS     r0,#0
        0x00000132:    e001        ..      B        0x138 ; __scatterload_zeroinit + 8
        0x00000134:    c101        ..      STM      r1!,{r0}
        0x00000136:    1f12        ..      SUBS     r2,r2,#4
        0x00000138:    2a00        .*      CMP      r2,#0
        0x0000013a:    d1fb        ..      BNE      0x134 ; __scatterload_zeroinit + 4
        0x0000013c:    4770        pG      BX       lr
        0x0000013e:    0000        ..      MOVS     r0,r0
    $d.realdata
    Region$$Table$$Base
        0x00000140:    00000410    ....    DCD    1040
        0x00000144:    20000000    ...     DCD    536870912
        0x00000148:    00000400    ....    DCD    1024
        0x0000014c:    00000130    0...    DCD    304
    Region$$Table$$Limit

** Section #4 'ER_ANY' (SHT_PROGBITS) [SHF_ALLOC + SHF_EXECINSTR]
    Size   : 704 bytes (alignment 4)
    Address: 0x00000150

    $t
    i.GPIO_SetPinDir
    GPIO_SetPinDir
        0x00000150:    b570        p.      PUSH     {r4-r6,lr}
        0x00000152:    4605        .F      MOV      r5,r0
        0x00000154:    460c        .F      MOV      r4,r1
        0x00000156:    4616        .F      MOV      r6,r2
        0x00000158:    2200        ."      MOVS     r2,#0
        0x0000015a:    4621        !F      MOV      r1,r4
        0x0000015c:    4628        (F      MOV      r0,r5
        0x0000015e:    f000f819    ....    BL       GPIO_SetPinMux ; 0x194
        0x00000162:    4809        .H      LDR      r0,[pc,#36] ; [0x188] = 0x40020000
        0x00000164:    4909        .I      LDR      r1,[pc,#36] ; [0x18c] = 0x40028000
        0x00000166:    4a0a        .J      LDR      r2,[pc,#40] ; [0x190] = 0x40038000
        0x00000168:    2e01        ..      CMP      r6,#1
        0x0000016a:    d005        ..      BEQ      0x178 ; GPIO_SetPinDir + 40
        0x0000016c:    2e00        ..      CMP      r6,#0
        0x0000016e:    d102        ..      BNE      0x176 ; GPIO_SetPinDir + 38
        0x00000170:    4285        .B      CMP      r5,r0
        0x00000172:    d107        ..      BNE      0x184 ; GPIO_SetPinDir + 52
        0x00000174:    608c        .`      STR      r4,[r1,#8]
        0x00000176:    bd70        p.      POP      {r4-r6,pc}
        0x00000178:    4285        .B      CMP      r5,r0
        0x0000017a:    d101        ..      BNE      0x180 ; GPIO_SetPinDir + 48
        0x0000017c:    604c        L`      STR      r4,[r1,#4]
        0x0000017e:    bd70        p.      POP      {r4-r6,pc}
        0x00000180:    6054        T`      STR      r4,[r2,#4]
        0x00000182:    bd70        p.      POP      {r4-r6,pc}
        0x00000184:    6094        .`      STR      r4,[r2,#8]
        0x00000186:    bd70        p.      POP      {r4-r6,pc}
    $d
        0x00000188:    40020000    ...@    DCD    1073872896
        0x0000018c:    40028000    ...@    DCD    1073905664
        0x00000190:    40038000    ...@    DCD    1073971200
    $t
    i.GPIO_SetPinMux
    GPIO_SetPinMux
        0x00000194:    b530        0.      PUSH     {r4,r5,lr}
        0x00000196:    4b17        .K      LDR      r3,[pc,#92] ; [0x1f4] = 0x40020000
        0x00000198:    4298        .B      CMP      r0,r3
        0x0000019a:    d114        ..      BNE      0x1c6 ; GPIO_SetPinMux + 50
        0x0000019c:    2000        .       MOVS     r0,#0
        0x0000019e:    4c16        .L      LDR      r4,[pc,#88] ; [0x1f8] = 0x40001000
        0x000001a0:    460b        .F      MOV      r3,r1
        0x000001a2:    40c3        .@      LSRS     r3,r3,r0
        0x000001a4:    07db        ..      LSLS     r3,r3,#31
        0x000001a6:    0fdb        ..      LSRS     r3,r3,#31
        0x000001a8:    2b00        .+      CMP      r3,#0
        0x000001aa:    d008        ..      BEQ      0x1be ; GPIO_SetPinMux + 42
        0x000001ac:    0083        ..      LSLS     r3,r0,#2
        0x000001ae:    191b        ..      ADDS     r3,r3,r4
        0x000001b0:    681d        .h      LDR      r5,[r3,#0]
        0x000001b2:    08ed        ..      LSRS     r5,r5,#3
        0x000001b4:    00ed        ..      LSLS     r5,r5,#3
        0x000001b6:    601d        .`      STR      r5,[r3,#0]
        0x000001b8:    681d        .h      LDR      r5,[r3,#0]
        0x000001ba:    4315        .C      ORRS     r5,r5,r2
        0x000001bc:    601d        .`      STR      r5,[r3,#0]
        0x000001be:    1c40        @.      ADDS     r0,r0,#1
        0x000001c0:    2820         (      CMP      r0,#0x20
        0x000001c2:    d3ed        ..      BCC      0x1a0 ; GPIO_SetPinMux + 12
        0x000001c4:    bd30        0.      POP      {r4,r5,pc}
        0x000001c6:    2000        .       MOVS     r0,#0
        0x000001c8:    4c0b        .L      LDR      r4,[pc,#44] ; [0x1f8] = 0x40001000
        0x000001ca:    3480        .4      ADDS     r4,r4,#0x80
        0x000001cc:    460b        .F      MOV      r3,r1
        0x000001ce:    40c3        .@      LSRS     r3,r3,r0
        0x000001d0:    07db        ..      LSLS     r3,r3,#31
        0x000001d2:    0fdb        ..      LSRS     r3,r3,#31
        0x000001d4:    2b00        .+      CMP      r3,#0
        0x000001d6:    d008        ..      BEQ      0x1ea ; GPIO_SetPinMux + 86
        0x000001d8:    0083        ..      LSLS     r3,r0,#2
        0x000001da:    191b        ..      ADDS     r3,r3,r4
        0x000001dc:    681d        .h      LDR      r5,[r3,#0]
        0x000001de:    08ed        ..      LSRS     r5,r5,#3
        0x000001e0:    00ed        ..      LSLS     r5,r5,#3
        0x000001e2:    601d        .`      STR      r5,[r3,#0]
        0x000001e4:    681d        .h      LDR      r5,[r3,#0]
        0x000001e6:    4315        .C      ORRS     r5,r5,r2
        0x000001e8:    601d        .`      STR      r5,[r3,#0]
        0x000001ea:    1c40        @.      ADDS     r0,r0,#1
        0x000001ec:    2820         (      CMP      r0,#0x20
        0x000001ee:    d3ed        ..      BCC      0x1cc ; GPIO_SetPinMux + 56
        0x000001f0:    bd30        0.      POP      {r4,r5,pc}
    $d
        0x000001f2:    0000        ..      DCW    0
        0x000001f4:    40020000    ...@    DCD    1073872896
        0x000001f8:    40001000    ...@    DCD    1073745920
    $t
    i.NVIC_Init
    NVIC_Init
        0x000001fc:    b530        0.      PUSH     {r4,r5,lr}
        0x000001fe:    7882        .x      LDRB     r2,[r0,#2]
        0x00000200:    2401        .$      MOVS     r4,#1
        0x00000202:    7801        .x      LDRB     r1,[r0,#0]
        0x00000204:    2a00        .*      CMP      r2,#0
        0x00000206:    d016        ..      BEQ      0x236 ; NVIC_Init + 58
        0x00000208:    088b        ..      LSRS     r3,r1,#2
        0x0000020a:    4a0e        .J      LDR      r2,[pc,#56] ; [0x244] = 0xe000e400
        0x0000020c:    009b        ..      LSLS     r3,r3,#2
        0x0000020e:    189a        ..      ADDS     r2,r3,r2
        0x00000210:    6813        .h      LDR      r3,[r2,#0]
        0x00000212:    0789        ..      LSLS     r1,r1,#30
        0x00000214:    0ec9        ..      LSRS     r1,r1,#27
        0x00000216:    25ff        .%      MOVS     r5,#0xff
        0x00000218:    408d        .@      LSLS     r5,r5,r1
        0x0000021a:    43ab        .C      BICS     r3,r3,r5
        0x0000021c:    7845        Ex      LDRB     r5,[r0,#1]
        0x0000021e:    07ad        ..      LSLS     r5,r5,#30
        0x00000220:    0e2d        -.      LSRS     r5,r5,#24
        0x00000222:    408d        .@      LSLS     r5,r5,r1
        0x00000224:    431d        .C      ORRS     r5,r5,r3
        0x00000226:    6015        .`      STR      r5,[r2,#0]
        0x00000228:    7800        .x      LDRB     r0,[r0,#0]
        0x0000022a:    06c0        ..      LSLS     r0,r0,#27
        0x0000022c:    0ec0        ..      LSRS     r0,r0,#27
        0x0000022e:    4084        .@      LSLS     r4,r4,r0
        0x00000230:    4805        .H      LDR      r0,[pc,#20] ; [0x248] = 0xe000e100
        0x00000232:    6004        .`      STR      r4,[r0,#0]
        0x00000234:    bd30        0.      POP      {r4,r5,pc}
        0x00000236:    06c8        ..      LSLS     r0,r1,#27
        0x00000238:    0ec0        ..      LSRS     r0,r0,#27
        0x0000023a:    4084        .@      LSLS     r4,r4,r0
        0x0000023c:    4802        .H      LDR      r0,[pc,#8] ; [0x248] = 0xe000e100
        0x0000023e:    3080        .0      ADDS     r0,r0,#0x80
        0x00000240:    6004        .`      STR      r4,[r0,#0]
        0x00000242:    bd30        0.      POP      {r4,r5,pc}
    $d
        0x00000244:    e000e400    ....    DCD    3758154752
        0x00000248:    e000e100    ....    DCD    3758153984
    $t
    i.SystemHeart_Init
    SystemHeart_Init
        0x0000024c:    b57f        ..      PUSH     {r0-r6,lr}
        0x0000024e:    2001        .       MOVS     r0,#1
        0x00000250:    0780        ..      LSLS     r0,r0,#30
        0x00000252:    6941        Ai      LDR      r1,[r0,#0x14]
        0x00000254:    1302        ..      ASRS     r2,r0,#12
        0x00000256:    4311        .C      ORRS     r1,r1,r2
        0x00000258:    6141        Aa      STR      r1,[r0,#0x14]
        0x0000025a:    a801        ..      ADD      r0,sp,#4
        0x0000025c:    f000f8bc    ....    BL       TIM_TimeBaseStructInit ; 0x3d8
        0x00000260:    205f        _       MOVS     r0,#0x5f
        0x00000262:    466a        jF      MOV      r2,sp
        0x00000264:    8090        ..      STRH     r0,[r2,#4]
        0x00000266:    4812        .H      LDR      r0,[pc,#72] ; [0x2b0] = 0x3e7
        0x00000268:    9002        ..      STR      r0,[sp,#8]
        0x0000026a:    2400        .$      MOVS     r4,#0
        0x0000026c:    8194        ..      STRH     r4,[r2,#0xc]
        0x0000026e:    80d4        ..      STRH     r4,[r2,#6]
        0x00000270:    4d10        .M      LDR      r5,[pc,#64] ; [0x2b4] = 0x4000b000
        0x00000272:    a901        ..      ADD      r1,sp,#4
        0x00000274:    4628        (F      MOV      r0,r5
        0x00000276:    f000f87d    ..}.    BL       TIM_TimeBaseInit ; 0x374
        0x0000027a:    2110        .!      MOVS     r1,#0x10
        0x0000027c:    4628        (F      MOV      r0,r5
        0x0000027e:    f000f871    ..q.    BL       TIM_SelectOutputTrigger ; 0x364
        0x00000282:    2101        .!      MOVS     r1,#1
        0x00000284:    4628        (F      MOV      r0,r5
        0x00000286:    f000f851    ..Q.    BL       TIM_ClearITPendingBit ; 0x32c
        0x0000028a:    2201        ."      MOVS     r2,#1
        0x0000028c:    4611        .F      MOV      r1,r2
        0x0000028e:    4628        (F      MOV      r0,r5
        0x00000290:    f000f85e    ..^.    BL       TIM_ITConfig ; 0x350
        0x00000294:    2102        .!      MOVS     r1,#2
        0x00000296:    466a        jF      MOV      r2,sp
        0x00000298:    7011        .p      STRB     r1,[r2,#0]
        0x0000029a:    2001        .       MOVS     r0,#1
        0x0000029c:    7090        .p      STRB     r0,[r2,#2]
        0x0000029e:    7054        Tp      STRB     r4,[r2,#1]
        0x000002a0:    4668        hF      MOV      r0,sp
        0x000002a2:    f7ffffab    ....    BL       NVIC_Init ; 0x1fc
        0x000002a6:    2101        .!      MOVS     r1,#1
        0x000002a8:    4628        (F      MOV      r0,r5
        0x000002aa:    f000f843    ..C.    BL       TIM_Cmd ; 0x334
        0x000002ae:    bd7f        ..      POP      {r0-r6,pc}
    $d
        0x000002b0:    000003e7    ....    DCD    999
        0x000002b4:    4000b000    ...@    DCD    1073786880
    $t
    i.SystemInit
    SystemInit
        0x000002b8:    b510        ..      PUSH     {r4,lr}
        0x000002ba:    2000        .       MOVS     r0,#0
        0x000002bc:    2101        .!      MOVS     r1,#1
        0x000002be:    4a11        .J      LDR      r2,[pc,#68] ; [0x304] = 0x818
        0x000002c0:    0789        ..      LSLS     r1,r1,#30
        0x000002c2:    614a        Ja      STR      r2,[r1,#0x14]
        0x000002c4:    4b10        .K      LDR      r3,[pc,#64] ; [0x308] = 0x40000080
        0x000002c6:    2204        ."      MOVS     r2,#4
        0x000002c8:    601a        .`      STR      r2,[r3,#0]
        0x000002ca:    4a10        .J      LDR      r2,[pc,#64] ; [0x30c] = 0x40000200
        0x000002cc:    6893        .h      LDR      r3,[r2,#8]
        0x000002ce:    24ff        .$      MOVS     r4,#0xff
        0x000002d0:    3412        .4      ADDS     r4,r4,#0x12
        0x000002d2:    43a3        .C      BICS     r3,r3,r4
        0x000002d4:    6093        .`      STR      r3,[r2,#8]
        0x000002d6:    4a0e        .J      LDR      r2,[pc,#56] ; [0x310] = 0x80000016
        0x000002d8:    630a        .c      STR      r2,[r1,#0x30]
        0x000002da:    13ca        ..      ASRS     r2,r1,#15
        0x000002dc:    e003        ..      B        0x2e6 ; SystemInit + 46
        0x000002de:    4603        .F      MOV      r3,r0
        0x000002e0:    1c40        @.      ADDS     r0,r0,#1
        0x000002e2:    4293        .B      CMP      r3,r2
        0x000002e4:    d202        ..      BCS      0x2ec ; SystemInit + 52
        0x000002e6:    6b4b        Kk      LDR      r3,[r1,#0x34]
        0x000002e8:    2b01        .+      CMP      r3,#1
        0x000002ea:    d1f8        ..      BNE      0x2de ; SystemInit + 38
        0x000002ec:    207d        }       MOVS     r0,#0x7d
        0x000002ee:    00c0        ..      LSLS     r0,r0,#3
        0x000002f0:    f7fffef0    ....    BL       commonDelay ; 0xd4
        0x000002f4:    4804        .H      LDR      r0,[pc,#16] ; [0x308] = 0x40000080
        0x000002f6:    2101        .!      MOVS     r1,#1
        0x000002f8:    3840        @8      SUBS     r0,r0,#0x40
        0x000002fa:    6101        .a      STR      r1,[r0,#0x10]
        0x000002fc:    2200        ."      MOVS     r2,#0
        0x000002fe:    6142        Ba      STR      r2,[r0,#0x14]
        0x00000300:    6141        Aa      STR      r1,[r0,#0x14]
        0x00000302:    bd10        ..      POP      {r4,pc}
    $d
        0x00000304:    00000818    ....    DCD    2072
        0x00000308:    40000080    ...@    DCD    1073741952
        0x0000030c:    40000200    ...@    DCD    1073742336
        0x00000310:    80000016    ....    DCD    2147483670
    $t
    i.TIM15_IRQHandler
    TIM15_IRQHandler
        0x00000314:    4903        .I      LDR      r1,[pc,#12] ; [0x324] = 0x4000b000
        0x00000316:    2000        .       MOVS     r0,#0
        0x00000318:    8208        ..      STRH     r0,[r1,#0x10]
        0x0000031a:    4903        .I      LDR      r1,[pc,#12] ; [0x328] = 0x40020000
        0x0000031c:    2001        .       MOVS     r0,#1
        0x0000031e:    60c8        .`      STR      r0,[r1,#0xc]
        0x00000320:    4770        pG      BX       lr
    $d
        0x00000322:    0000        ..      DCW    0
        0x00000324:    4000b000    ...@    DCD    1073786880
        0x00000328:    40020000    ...@    DCD    1073872896
    $t
    i.TIM_ClearITPendingBit
    TIM_ClearITPendingBit
        0x0000032c:    43c9        .C      MVNS     r1,r1
        0x0000032e:    8201        ..      STRH     r1,[r0,#0x10]
        0x00000330:    4770        pG      BX       lr
        0x00000332:    0000        ..      MOVS     r0,r0
    i.TIM_Cmd
    TIM_Cmd
        0x00000334:    2900        .)      CMP      r1,#0
        0x00000336:    d004        ..      BEQ      0x342 ; TIM_Cmd + 14
        0x00000338:    6801        .h      LDR      r1,[r0,#0]
        0x0000033a:    2201        ."      MOVS     r2,#1
        0x0000033c:    4311        .C      ORRS     r1,r1,r2
        0x0000033e:    6001        .`      STR      r1,[r0,#0]
        0x00000340:    4770        pG      BX       lr
        0x00000342:    6801        .h      LDR      r1,[r0,#0]
        0x00000344:    4a01        .J      LDR      r2,[pc,#4] ; [0x34c] = 0xfffe
        0x00000346:    4011        .@      ANDS     r1,r1,r2
        0x00000348:    6001        .`      STR      r1,[r0,#0]
        0x0000034a:    4770        pG      BX       lr
    $d
        0x0000034c:    0000fffe    ....    DCD    65534
    $t
    i.TIM_ITConfig
    TIM_ITConfig
        0x00000350:    2a00        .*      CMP      r2,#0
        0x00000352:    d003        ..      BEQ      0x35c ; TIM_ITConfig + 12
        0x00000354:    8982        ..      LDRH     r2,[r0,#0xc]
        0x00000356:    430a        .C      ORRS     r2,r2,r1
        0x00000358:    8182        ..      STRH     r2,[r0,#0xc]
        0x0000035a:    4770        pG      BX       lr
        0x0000035c:    8982        ..      LDRH     r2,[r0,#0xc]
        0x0000035e:    438a        .C      BICS     r2,r2,r1
        0x00000360:    8182        ..      STRH     r2,[r0,#0xc]
        0x00000362:    4770        pG      BX       lr
    i.TIM_SelectOutputTrigger
    TIM_SelectOutputTrigger
        0x00000364:    8882        ..      LDRH     r2,[r0,#4]
        0x00000366:    2370        p#      MOVS     r3,#0x70
        0x00000368:    439a        .C      BICS     r2,r2,r3
        0x0000036a:    8082        ..      STRH     r2,[r0,#4]
        0x0000036c:    8882        ..      LDRH     r2,[r0,#4]
        0x0000036e:    430a        .C      ORRS     r2,r2,r1
        0x00000370:    8082        ..      STRH     r2,[r0,#4]
        0x00000372:    4770        pG      BX       lr
    i.TIM_TimeBaseInit
    TIM_TimeBaseInit
        0x00000374:    b510        ..      PUSH     {r4,lr}
        0x00000376:    6802        .h      LDR      r2,[r0,#0]
        0x00000378:    b292        ..      UXTH     r2,r2
        0x0000037a:    4c11        .L      LDR      r4,[pc,#68] ; [0x3c0] = 0x4000a000
        0x0000037c:    42a0        .B      CMP      r0,r4
        0x0000037e:    d002        ..      BEQ      0x386 ; TIM_TimeBaseInit + 18
        0x00000380:    4b10        .K      LDR      r3,[pc,#64] ; [0x3c4] = 0x4000a800
        0x00000382:    4298        .B      CMP      r0,r3
        0x00000384:    d103        ..      BNE      0x38e ; TIM_TimeBaseInit + 26
        0x00000386:    2370        p#      MOVS     r3,#0x70
        0x00000388:    439a        .C      BICS     r2,r2,r3
        0x0000038a:    884b        K.      LDRH     r3,[r1,#2]
        0x0000038c:    431a        .C      ORRS     r2,r2,r3
        0x0000038e:    4b0e        .K      LDR      r3,[pc,#56] ; [0x3c8] = 0xfcff
        0x00000390:    401a        .@      ANDS     r2,r2,r3
        0x00000392:    890b        ..      LDRH     r3,[r1,#8]
        0x00000394:    4313        .C      ORRS     r3,r3,r2
        0x00000396:    6003        .`      STR      r3,[r0,#0]
        0x00000398:    888a        ..      LDRH     r2,[r1,#4]
        0x0000039a:    8582        ..      STRH     r2,[r0,#0x2c]
        0x0000039c:    880a        ..      LDRH     r2,[r1,#0]
        0x0000039e:    8502        ..      STRH     r2,[r0,#0x28]
        0x000003a0:    42a0        .B      CMP      r0,r4
        0x000003a2:    d008        ..      BEQ      0x3b6 ; TIM_TimeBaseInit + 66
        0x000003a4:    4a09        .J      LDR      r2,[pc,#36] ; [0x3cc] = 0x4000b000
        0x000003a6:    4290        .B      CMP      r0,r2
        0x000003a8:    d005        ..      BEQ      0x3b6 ; TIM_TimeBaseInit + 66
        0x000003aa:    4a09        .J      LDR      r2,[pc,#36] ; [0x3d0] = 0x4000c000
        0x000003ac:    4290        .B      CMP      r0,r2
        0x000003ae:    d002        ..      BEQ      0x3b6 ; TIM_TimeBaseInit + 66
        0x000003b0:    4a08        .J      LDR      r2,[pc,#32] ; [0x3d4] = 0x4000c800
        0x000003b2:    4290        .B      CMP      r0,r2
        0x000003b4:    d101        ..      BNE      0x3ba ; TIM_TimeBaseInit + 70
        0x000003b6:    7a89        .z      LDRB     r1,[r1,#0xa]
        0x000003b8:    8601        ..      STRH     r1,[r0,#0x30]
        0x000003ba:    2101        .!      MOVS     r1,#1
        0x000003bc:    8281        ..      STRH     r1,[r0,#0x14]
        0x000003be:    bd10        ..      POP      {r4,pc}
    $d
        0x000003c0:    4000a000    ...@    DCD    1073782784
        0x000003c4:    4000a800    ...@    DCD    1073784832
        0x000003c8:    0000fcff    ....    DCD    64767
        0x000003cc:    4000b000    ...@    DCD    1073786880
        0x000003d0:    4000c000    ...@    DCD    1073790976
        0x000003d4:    4000c800    ...@    DCD    1073793024
    $t
    i.TIM_TimeBaseStructInit
    TIM_TimeBaseStructInit
        0x000003d8:    2100        .!      MOVS     r1,#0
        0x000003da:    43c9        .C      MVNS     r1,r1
        0x000003dc:    6041        A`      STR      r1,[r0,#4]
        0x000003de:    2100        .!      MOVS     r1,#0
        0x000003e0:    8001        ..      STRH     r1,[r0,#0]
        0x000003e2:    8101        ..      STRH     r1,[r0,#8]
        0x000003e4:    8041        A.      STRH     r1,[r0,#2]
        0x000003e6:    7281        .r      STRB     r1,[r0,#0xa]
        0x000003e8:    4770        pG      BX       lr
        0x000003ea:    0000        ..      MOVS     r0,r0
    i.main
    main
        0x000003ec:    4c07        .L      LDR      r4,[pc,#28] ; [0x40c] = 0x40020000
        0x000003ee:    2200        ."      MOVS     r2,#0
        0x000003f0:    2101        .!      MOVS     r1,#1
        0x000003f2:    4620         F      MOV      r0,r4
        0x000003f4:    f7fffece    ....    BL       GPIO_SetPinMux ; 0x194
        0x000003f8:    2201        ."      MOVS     r2,#1
        0x000003fa:    4611        .F      MOV      r1,r2
        0x000003fc:    4620         F      MOV      r0,r4
        0x000003fe:    f7fffea7    ....    BL       GPIO_SetPinDir ; 0x150
        0x00000402:    2101        .!      MOVS     r1,#1
        0x00000404:    60a1        .`      STR      r1,[r4,#8]
        0x00000406:    f7ffff21    ..!.    BL       SystemHeart_Init ; 0x24c
        0x0000040a:    e7fe        ..      B        0x40a ; main + 30
    $d
        0x0000040c:    40020000    ...@    DCD    1073872896

** Section #5 'RW_IRAM1' (SHT_NOBITS) [SHF_ALLOC + SHF_WRITE]
    Size   : 1024 bytes (alignment 8)
    Address: 0x20000000


** Section #6 '.debug_abbrev' (SHT_PROGBITS)
    Size   : 1476 bytes


** Section #7 '.debug_frame' (SHT_PROGBITS)
    Size   : 968 bytes


** Section #8 '.debug_info' (SHT_PROGBITS)
    Size   : 42020 bytes


** Section #9 '.debug_line' (SHT_PROGBITS)
    Size   : 4296 bytes


** Section #10 '.debug_loc' (SHT_PROGBITS)
    Size   : 1020 bytes


** Section #11 '.debug_macinfo' (SHT_PROGBITS)
    Size   : 136012 bytes


** Section #12 '.debug_pubnames' (SHT_PROGBITS)
    Size   : 666 bytes


** Section #13 '.symtab' (SHT_SYMTAB)
    Size   : 2608 bytes (alignment 4)
    String table #14 '.strtab'
    Last local symbol no. 96


** Section #14 '.strtab' (SHT_STRTAB)
    Size   : 2672 bytes


** Section #15 '.note' (SHT_NOTE)
    Size   : 36 bytes (alignment 4)


** Section #16 '.comment' (SHT_PROGBITS)
    Size   : 9972 bytes


** Section #17 '.shstrtab' (SHT_STRTAB)
    Size   : 176 bytes



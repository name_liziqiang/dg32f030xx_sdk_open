/**
  ******************************************************************************
  * @file    m1130_eval_qspi.c
  * @author  Alpscale Application Team
  * @version V1.0.0
  * @date    27-November-2013
  * @brief   This file provides a set of functions needed to manage the QSPI FLASH 
  *              mounted on M1130-EVAL evaluation board.
  *         
  *              
  *          It implements a high level communication layer for read and write 
  *          from/to this memory. The needed Alpscale hardware resources (QSPI and 
  *          GPIO) are defined in m1130_eval.h file, and the initialization is 
  *          performed in QSPI_Dma_Init() function declared in m1130_eval.c 
  *          file.
  *          You can easily tailor this driver to any other development board, 
  *          by just adapting the defines for hardware resources and 
  *          QSPI_Dma_Init() function. 
  *               
  *
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alpscale</center></h2>
  ******************************************************************************  
  */ 

/* Includes ------------------------------------------------------------------*/
#include "m1130_eval_qspi.h"
#include "m1130_qspi.h"
#include "m1130_gpio.h"
#include <stdio.h>
#include "m1130_rcc.h"	 

#define NO_ESMT_FLASH


/** @addtogroup Utilities
  * @{
  */
  
/** @addtogroup M1130_EVAL
  * @{
  */
  
/** @addtogroup M1130_EVAL_QSPI
  * @brief      This file includes the QSPI FLASH driver of M1130-EVAL boards.
  * @{
  */ 

/** @addtogroup M1130_EVAL_QSPI_Private_Variables
  * @{
  */
QSPIxPOS  g_qspixpos;
uint8_t _4ByteAddrMode = 0;
/**
  * @}
  */

/** @defgroup M1130_EVAL_QSPI_Private_Defines
  * @{
  */   
#define MAX_BUSY_WAIT 0x00100000
#define FLASH_FIRST_BIT_MODE        QSPI_FirstBit_MSB //QSPI_FirstBit_MSB:MSB first, QSPI_FirstBit_LSB:LSB first
// #define TEST_SPI_MSB_TX_LSB_RX      1 //1:MSB TX,LSB RX;0:LSB TX，MSB RX.测试用单线测试,在Dual/Quad SPI模式，只支持MSB first。

#define SPI_CLK_MODE                3 //0/3: set spi clk mode 0/3
/**
  * @}
  */ 

/**
  * @brief  Switch pin as SPI  function or GPIO function. 
  * @param  GPIOx: where x can be (0..1) to select the GPIO peripheral. 
  * @param  pin: specifies the port bits to be written.
  *   This parameter can be one of GPIO_Pin_x where x can be (0..31). 
  * @param  func: specifies the pin multiplex function.
  *   This parameter can be one of the GPIOFUNC_TypeDef enum values:
  *   	@arg GPIO_FUNCTION_0: 0.
  *   	@arg GPIO_FUNCTION_1: 1.
  *   	@arg GPIO_FUNCTION_2: 2.
  *   	@arg GPIO_FUNCTION_3: 3.
  *   	@arg GPIO_FUNCTION_4: 4.
  *   	@arg GPIO_FUNCTION_5: 5.
  *   	@arg GPIO_FUNCTION_6: 6.
  *   	@arg GPIO_FUNCTION_7: 7. 
  * @param  isQuad: whether specifies D2 D3 as quad spi line or not. 
  * @retval  
  */
void switchQSPI(GPIO_TypeDef* GPIOx, uint32_t pin, GPIOFUNC_TypeDef func, int isQuad)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	if(isQuad){
		GPIO_InitStructure.GPIO_Pin = pin;
		GPIO_InitStructure.GPIO_Function = func;
		GPIO_Init(GPIOx, &GPIO_InitStructure);
	}else{
		GPIO_InitStructure.GPIO_Pin = pin;
		GPIO_InitStructure.GPIO_Function = GPIO_FUNCTION_0;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_Init(GPIOx, &GPIO_InitStructure);
		GPIO_SetPin(GPIOx, pin);
	}
}

/**
  * @brief  Switch D2 D3 as Quad SPI line
  * @param  QSPIptr: the QUAD spi controller base address. 
  * @param  isQuad: whether specifies D2 D3 as quad spi line or not. 
  * @retval None 
  */
void QSPI_PinSwitch(QSPI_TypeDef* QSPIptr, int isQuad)
{
	switch (g_qspixpos)
	{
		case QSPI0_GP4:
			switchQSPI(GPIO1, GPIO_Pin_0, GPIO_FUNCTION_1, isQuad);//gp4_0 d3
			switchQSPI(GPIO1, GPIO_Pin_3, GPIO_FUNCTION_1, isQuad);//gp4_3 d2
			break;
		case QSPI1_GP1:
			switchQSPI(GPIO0, GPIO_Pin_9, GPIO_FUNCTION_3, isQuad);//gp1_1 d3    
			switchQSPI(GPIO0, GPIO_Pin_12, GPIO_FUNCTION_3, isQuad);//gp1_4 d2
			// switchQSPI(GPIO0, GPIO_Pin_14, GPIO_FUNCTION_3, isQuad);//gp1_6 d2/swd_tck
			// switchQSPI(GPIO0, GPIO_Pin_15, GPIO_FUNCTION_3, isQuad);//gp1_7 d3/swd_io
			break;
		case QSPI1_GP3:
			switchQSPI(GPIO0, GPIO_Pin_28, GPIO_FUNCTION_3, isQuad);//gp3_4 d3
			switchQSPI(GPIO0, GPIO_Pin_29, GPIO_FUNCTION_3, isQuad);//gp3_5 d2
			break;
		case QSPI2_GP5:
			switchQSPI(GPIO1, GPIO_Pin_12, GPIO_FUNCTION_3, isQuad);//gp5_4 d3
			switchQSPI(GPIO1, GPIO_Pin_13, GPIO_FUNCTION_3, isQuad);//gp5_5 d2
			break;
	}
}

/**
  * @brief  initialize SPI function pin.
  * @param  QSPIptr: the QUAD spi controller base address. 
  * @param  qspixpos: QSPI GPIO pin init. 
  * 		@arg QSPI0_GP4: QSPSI0 gpio pin
  * 		@arg QSPI1_GP1, QSPI1_GP3: QSPI1 gpio pin 
  * 		@arg QSPI2_GP5: QSPI2 gpio pin 
  * @retval  
  */
void qspi_initPins(QSPI_TypeDef* QSPIptr, QSPIxPOS qspixpos)
{
	g_qspixpos = qspixpos;
	switch (qspixpos)
	{
		case QSPI0_GP4:
			switchQSPI(GPIO1, GPIO_Pin_1, GPIO_FUNCTION_1, 1);//gp4_1 clk
			switchQSPI(GPIO1, GPIO_Pin_2, GPIO_FUNCTION_1, 1);//gp4_2 d0
			switchQSPI(GPIO1, GPIO_Pin_4, GPIO_FUNCTION_1, 1);//gp4_4 d1
			switchQSPI(GPIO1, GPIO_Pin_5, GPIO_FUNCTION_1, 1);//gp4_5 cs
			break;
		case QSPI1_GP1:
			switchQSPI(GPIO0, GPIO_Pin_13, GPIO_FUNCTION_3, 1);//gp1_5 d0
			// switchQSPI(GPIO0, GPIO_Pin_7, GPIO_FUNCTION_3, 1);//gp0_7 clk
			switchQSPI(GPIO0, GPIO_Pin_10, GPIO_FUNCTION_3, 1);//gp1_2 clk
			switchQSPI(GPIO0, GPIO_Pin_6, GPIO_FUNCTION_3, 1);//gp0_6 d1
			switchQSPI(GPIO0, GPIO_Pin_5, GPIO_FUNCTION_3, 1);//gp0_5 cs
			break;
		case QSPI1_GP3:
			switchQSPI(GPIO0, GPIO_Pin_26, GPIO_FUNCTION_3, 1);//gp3_2 d0
			switchQSPI(GPIO0, GPIO_Pin_27, GPIO_FUNCTION_3, 1);//gp3_3 clk
			switchQSPI(GPIO0, GPIO_Pin_30, GPIO_FUNCTION_3, 1);//gp3_6 d1
			switchQSPI(GPIO0, GPIO_Pin_31, GPIO_FUNCTION_3, 1);//gp3_7 cs
			break;
		case QSPI2_GP5:
			switchQSPI(GPIO1, GPIO_Pin_10, GPIO_FUNCTION_3, 1);//gp5_2 d0
			switchQSPI(GPIO1, GPIO_Pin_14, GPIO_FUNCTION_3, 1);//gp5_6 d1
			switchQSPI(GPIO1, GPIO_Pin_11, GPIO_FUNCTION_3, 1);//gp5_3 clk
			switchQSPI(GPIO1, GPIO_Pin_15, GPIO_FUNCTION_3, 1);//gp5_7 cs
            
			// switchQSPI(GPIO1, GPIO_Pin_17, GPIO_FUNCTION_3, 1);//gp6_1 d1
			// switchQSPI(GPIO1, GPIO_Pin_18, GPIO_FUNCTION_3, 1);//gp6_2 cs
			break;
	}
}

/**
  * @brief  DeInitializes the QSPI controller and free the resource.
  * @param  QSPIptr: the QUAD spi controller base address. 
  * @retval None 
  */
void qspi_flash_deinit(QSPI_TypeDef* QSPIptr)
{
	QSPI_LowLevel_DeInit(QSPIptr);
}

/**
  * @brief  Initializes the QSPI controller and allocate the resource.
  * @param 	QSPIptr: the QUAD spi controller base address.
  * @param 	rcc_clk_div: config the QSPIx clock divider.
  * @param 	slaveMode: Specifies QSPI is master mode or slave mode.
  * 		@arg QSPI_MASTER_MODE:QSPI master mode
  * 		@arg QSPI_SLAVE_MODE:QSPI slave mode
  * @param  internal_clk_div: the QSPI clock internal clock setting coefficient.  
  * @param  internal_clk_rate: the QSPI clock internal clock setting coefficient.
  * 		Bit_rate= SPI_CLK/(internal_clk_div* (1+internal_clk_rate))
  * @param  qspixpos: QSPI GPIO pin init. 
  * 		@arg QSPI0_GP4: QSPSI0 gpio pin
  * 		@arg QSPI1_GP1, QSPI1_GP3: QSPI1 gpio pin
  * 		@arg QSPI2_GP5: QSPI2 gpio pin
  * @param  initD2D3:set 1/0 to specifies whether D2 and D3 line init or not. 
  * @retval 0: okay; else: fail.
  */
int qspi_flash_init(QSPI_TypeDef* QSPIptr,
                    uint8_t rcc_clk_div,
                    uint32_t slaveMode,
                    int internal_clk_div,
                    int internal_clk_rate,
                    QSPIxPOS qspixpos,
                    char initD2D3)
{
	QSPI_InitTypeDef QSPI_InitStructure;

  if(SPI_CLK_MODE == 0){
    QSPI_InitStructure.QSPI_CPOL = QSPI_CPOL_Low;
    QSPI_InitStructure.QSPI_CPHA = QSPI_CPHA_1Edge;
  }else{
    QSPI_InitStructure.QSPI_CPOL = QSPI_CPOL_High;
    QSPI_InitStructure.QSPI_CPHA = QSPI_CPHA_2Edge;
  }
  if(FLASH_FIRST_BIT_MODE == QSPI_FirstBit_MSB){
    QSPI_InitStructure.QSPI_FirstBit = QSPI_FirstBit_MSB;
  }else{
    QSPI_InitStructure.QSPI_FirstBit = QSPI_FirstBit_LSB;//只支持单线
  }
	QSPI_InitStructure.QSPI_ClockDiv = (internal_clk_div<<8);
	QSPI_InitStructure.QSPI_ClockRate = internal_clk_rate;
	QSPI_InitStructure.QSPI_SlaveMode = slaveMode;
	QSPI_InitStructure.QSPI_ModeSelect = QSPI_STD;
	QSPI_InitStructure.QSPI_FrameLength = QSPI_FRAME_LENGTH_8Bit;

	if(QSPIptr==QSPI0){
		RCC_SETCLKDivider(RCC_CLOCKFREQ_QSPI0CLK, rcc_clk_div);
	}else if(QSPIptr==QSPI1){
		RCC_SETCLKDivider(RCC_CLOCKFREQ_QSPI1CLK, rcc_clk_div);
	}else{
		RCC_SETCLKDivider(RCC_CLOCKFREQ_QSPI2CLK, rcc_clk_div);
	}

  qspi_initPins(QSPIptr, qspixpos);
  if (initD2D3) QSPI_PinSwitch(QSPIptr, 0);

  QSPI_HwInit(QSPIptr, &QSPI_InitStructure);

	return 0;
}

#define	_NOP_()		{__asm("nop");}
/**
  * @brief  qspi no dma send data.
  * @param  QSPIptr: the QUAD spi controller base address. 
  * @param  data: pointer to the buffer containing the data to be written. 
  * @param  num: write data length.
  * @retval 0: ok, other: fail. 
  */
int SpiTX(QSPI_TypeDef *QSPIptr, uint8_t *data, uint32_t num)
{
	//num<=16
	//FIFO is 4*32bit
	uint16_t i, j;
	volatile int waitnum = MAX_BUSY_WAIT;
	QSPI_DataInitTypeDef QSPI_DataInitStruct;

	for (i = 0; i < num / 4; i++)
	{
		QSPI_DataInitStruct.QSPI_DataLength = 4;
		QSPI_DataInitStruct.QSPI_DUPLEX = QSPI_HalfDuplex;
		QSPI_DataInitStruct.QSPI_TransferDir = QSPI_Transfer_Write;
		QSPI_DataConfig(QSPIptr, &QSPI_DataInitStruct);
		QSPIptr->DATA = *((uint32_t *)data + i);

		waitnum = MAX_BUSY_WAIT;
		while ((QSPI_GetFlagStatus(QSPIptr, QSPI_STATUS_XMIT_EMPTY) == RESET) && (waitnum--))
			_NOP_();
		if (waitnum < 0)
		{
			printf("spi tx is not empty timeout\r\n");
			return -1;
		}

		waitnum = MAX_BUSY_WAIT;
		while ((QSPI_GetFlagStatus(QSPIptr, QSPI_STATUS_SPI_BUSY) == SET) && (waitnum--))
			_NOP_();
		if (waitnum < 0)
		{
			printf("spi tx is busy timeout\r\n");
			return -1;
		}
	}

	num %= 4;
	if(num){
		uint8_t* pData = (uint8_t*)((uint32_t)QSPIptr+0x40);

		QSPI_DataInitStruct.QSPI_DataLength = num;
		QSPI_DataInitStruct.QSPI_DUPLEX = QSPI_HalfDuplex;
		QSPI_DataInitStruct.QSPI_TransferDir = QSPI_Transfer_Write;
		QSPI_DataConfig(QSPIptr, &QSPI_DataInitStruct);
		for(j=0;j<num;j++){
			*pData = data[i*4+j];
		}
		waitnum = MAX_BUSY_WAIT;
		while ((QSPI_GetFlagStatus(QSPIptr, QSPI_STATUS_XMIT_EMPTY) == RESET) && (waitnum--))
			_NOP_();
		if (waitnum < 0)
		{
			printf("spi tx is not empty timeout\r\n");
			return -1;
		}

		waitnum = MAX_BUSY_WAIT;
		while ((QSPI_GetFlagStatus(QSPIptr, QSPI_STATUS_SPI_BUSY) == SET) && (waitnum--))
			_NOP_();
		if (waitnum < 0)
		{
			printf("spi tx is busy timeout\r\n");
			return -1;
		}
	}
	return 0;
}

/**
  * @brief  qspi no dma receive data
  * @param  QSPIptr: the QUAD spi controller base address. 
  * @param  data: pointer to the buffer containing the data to be read. 
  * @param  num: read data length. 
  * @retval 0: ok, other: fail. 
  */
int SpiRX(QSPI_TypeDef* QSPIptr,uint8_t *data,uint32_t num)
{
	uint16_t i;
	uint32_t temp32;
	int waitnum = MAX_BUSY_WAIT;
	QSPI_DataInitTypeDef QSPI_DataInitStruct;

	for(i=0; i<num/4; i++) {
		QSPI_DataInitStruct.QSPI_DataLength = 4;
		QSPI_DataInitStruct.QSPI_DUPLEX = QSPI_HalfDuplex;
		QSPI_DataInitStruct.QSPI_TransferDir = QSPI_Transfer_Read;
		QSPI_DataConfig(QSPIptr, &QSPI_DataInitStruct);

		waitnum = MAX_BUSY_WAIT;
		while ((QSPI_GetFlagStatus(QSPIptr, QSPI_STATUS_SPI_BUSY) == SET) && (waitnum--))
			_NOP_();
		if (waitnum < 0)
		{
			printf("spi rx is busy timeout\r\n");
			return -1;
		}

		*((uint32_t *)data + i) = QSPIptr->DATA;
	}

	num %= 4;
	if(num) {
		QSPI_DataInitStruct.QSPI_DataLength = num;
		QSPI_DataInitStruct.QSPI_DUPLEX = QSPI_HalfDuplex;
		QSPI_DataInitStruct.QSPI_TransferDir = QSPI_Transfer_Read;
		QSPI_DataConfig(QSPIptr, &QSPI_DataInitStruct);

		waitnum = MAX_BUSY_WAIT;
		while ((QSPI_GetFlagStatus(QSPIptr, QSPI_STATUS_SPI_BUSY) == SET) && (waitnum--))
			_NOP_();
		if (waitnum < 0)
		{
			printf("spi rx is busy timeout\r\n");
			return -1;
		}

		temp32 = QSPIptr->DATA;
		data[i*4] = temp32 & 0xff;
		if(num>1){
			data[i*4 + 1] = (temp32 >> 8) & 0xff;
		}
		if(num>2){
			data[i*4 + 2] = (temp32 >> 16) & 0xff;
		}
	}
    return 0;
}

/**
  * @brief  start data write.  
  * @param  QSPIptr: the QUAD spi controller base address.
  * @param  buf : the source address. 
  * @param  n : the bytes number.
  *  
  * @retval 0,okay; else, error.
  */
int qspi_flash_write_buf(QSPI_TypeDef* QSPIptr, uint8_t * buf, int n)
{
  #if 1
	int waitnum = MAX_BUSY_WAIT;
	QSPI_DataInitTypeDef QSPI_DataInitStruct;

	QSPI_DataInitStruct.QSPI_DataLength = n;
	QSPI_DataInitStruct.QSPI_DUPLEX = QSPI_HalfDuplex;
	QSPI_DataInitStruct.QSPI_TransferDir = QSPI_Transfer_Write;

	QSPI_DMACmd(QSPIptr, ENABLE);	
	QSPI_DataConfig(QSPIptr, &QSPI_DataInitStruct);
	QSPI_LowLevel_DMA_TxConfig( QSPIptr, buf, n );
	QSPI_DMACmd(QSPIptr, DISABLE);	
   
   /*wait until finish*/
	waitnum = MAX_BUSY_WAIT;
	while( (QSPI_GetFlagStatus(QSPIptr, QSPI_STATUS_SPI_BUSY)==SET)&&(waitnum--) )
		;
	
	if(waitnum <= 0)
	{
	  printf("QSPI tx timeout!\n");
	  return -3;
	}  
	else
		return 0;		
  #else
  //used by special case : src is on ROM in XIP mode, DMA is too fast
  return SpiTX(QSPIptr, buf, (uint32_t)n);
  #endif
}


/**
  * @brief  start data read.  
  * @param QSPIptr: the QUAD spi controller base address.
  * @param  buf : the target store address. 
  * @param  n : the bytes number.
  *  
  * @retval 0,okay; else, error.
  */

int qspi_flash_read_buf(QSPI_TypeDef* QSPIptr, uint8_t * buf, int n)
{
	//DMA implementation
	int waitnum = MAX_BUSY_WAIT;
	QSPI_DataInitTypeDef QSPI_DataInitStruct;


	QSPI_DataInitStruct.QSPI_DataLength = n;
	QSPI_DataInitStruct.QSPI_DUPLEX = QSPI_HalfDuplex;
	QSPI_DataInitStruct.QSPI_TransferDir = QSPI_Transfer_Read;

	
	QSPI_DMACmd(QSPIptr, ENABLE);	
	QSPI_DataConfig(QSPIptr, &QSPI_DataInitStruct);
    QSPI_LowLevel_DMA_RxConfig( QSPIptr, buf, n );
	QSPI_DMACmd(QSPIptr, DISABLE);

	/*wait until finish*/
	waitnum = MAX_BUSY_WAIT;
	while ((QSPI_GetFlagStatus(QSPIptr, QSPI_STATUS_SPI_BUSY) == SET) && (waitnum--))
		;
   
	if(waitnum <= 0)
	{		 
		printf("QSPI Rx timeout!\n");
		return -3;
	}
	else
		return 0;	
}


__ALIGN4 static uint8_t qspi_cmd[4];
static int qspi_flash_send_cmd(QSPI_TypeDef *QSPIptr, uint8_t cmd)
{

  qspi_cmd[0] = cmd;
  return qspi_flash_write_buf(QSPIptr, qspi_cmd, 1);
}

__ALIGN4 static uint8_t qspi_addr[4];
static int qspi_flash_send_addr(QSPI_TypeDef* QSPIptr,uint32_t faddr)
{
  int ret;
  if (_4ByteAddrMode)
  {
    qspi_addr[0] = faddr >> 24;
    qspi_addr[1] = ((faddr >> 16) & 0xff);
    qspi_addr[2] = ((faddr >> 8) & 0xff);
    qspi_addr[3] = (faddr & 0xff);
    ret = qspi_flash_write_buf(QSPIptr, qspi_addr, 4);
  }
  else
  {
    qspi_addr[0] = faddr >> 16;
    qspi_addr[1] = ((faddr >> 8) & 0xff);
    qspi_addr[2] = (faddr & 0xff);
    ret = qspi_flash_write_buf(QSPIptr, qspi_addr, 3);
  }
  return ret;
}

static int qspi_flash_send_dummy(QSPI_TypeDef* QSPIptr,uint32_t count)
{
  qspi_addr[0] = (uint8_t)0;
  qspi_addr[1] = (uint8_t)0;
  qspi_addr[2] = (uint8_t)0;
  qspi_addr[3] = (uint8_t)0;

  return qspi_flash_write_buf(QSPIptr, qspi_addr, count);
}

__ALIGN4 static uint8_t qspi_flash_status[4];
/**
  * @brief  Read QSPI flash Status Register-1 
  * @param  QSPIptr: the QUAD spi controller base address. 
  * @param  status: get the qspi flash status1 value. 
  * @retval None 
  */
void qspi_flash_read_status1(QSPI_TypeDef* QSPIptr, char * status)
{
  QSPI_CS_Low(QSPIptr);

#ifdef TEST_SPI_MSB_TX_LSB_RX
  QSPI_FirstBitSet(QSPIptr, FLASH_FIRST_BIT_MODE);
  qspi_flash_send_cmd(QSPIptr, QUAD_SPI_FLASH_CMD_READ_STATUS1);
  qspi_flash_read_buf(QSPIptr, qspi_flash_status, 4);
#else
  qspi_flash_send_cmd(QSPIptr, QUAD_SPI_FLASH_CMD_READ_STATUS1);
  qspi_flash_read_buf(QSPIptr, qspi_flash_status, 4);
#endif
  *status = qspi_flash_status[0];
  QSPI_CS_High(QSPIptr);

  return;
}

/**
  * @brief  Read QSPI flash Status Register-2 
  * @param  QSPIptr: the QUAD spi controller base address. 
  * @param  status: get the qspi flash status2 value. 
  * @retval None 
  */
void qspi_flash_read_status2(QSPI_TypeDef* QSPIptr, char * status)
{
  QSPI_CS_Low(QSPIptr);
#ifdef TEST_SPI_MSB_TX_LSB_RX
  QSPI_FirstBitSet(QSPIptr, FLASH_FIRST_BIT_MODE);
  qspi_flash_send_cmd(QSPIptr, QUAD_SPI_FLASH_CMD_READ_STATUS2);
  qspi_flash_read_buf(QSPIptr, qspi_flash_status, 4);
#else
  qspi_flash_send_cmd(QSPIptr, QUAD_SPI_FLASH_CMD_READ_STATUS2);
  qspi_flash_read_buf(QSPIptr, qspi_flash_status, 4);
#endif
  *status = qspi_flash_status[0];
  QSPI_CS_High(QSPIptr);

  return;
}

/**
  * @brief 	Read QSPI flash JEDEC ID 
  * @param  QSPIptr: the QUAD spi controller base address. 
  * @param  buf: read buffer to get the JEDEC ID. 
  * @retval 0: ok, others: fail
  */
int qspi_flash_read_id(QSPI_TypeDef* QSPIptr,uint8_t* buf)
{
	int ret;

	QSPI_CS_Low(QSPIptr);
#ifdef TEST_SPI_MSB_TX_LSB_RX
  QSPI_FirstBitSet(QSPIptr, FLASH_FIRST_BIT_MODE);
  qspi_flash_send_cmd(QSPIptr, QUAD_SPI_FLASH_CMD_READ_ID);	

  if (TEST_SPI_MSB_TX_LSB_RX == 1) //tx msb, rx lsb
    QSPI_FirstBitSet(QSPIptr, QSPI_FirstBit_LSB);
  else
    QSPI_FirstBitSet(QSPIptr, QSPI_FirstBit_MSB);

	ret = qspi_flash_read_buf(QSPIptr, buf,4);
#else
  qspi_flash_send_cmd(QSPIptr, QUAD_SPI_FLASH_CMD_READ_ID);	
	ret = qspi_flash_read_buf(QSPIptr, buf,4);
#endif
	QSPI_CS_High(QSPIptr);

	return ret;
}

#ifdef QUAD_SPI_CMD_HIGH_PERFOR_MODE
static void qspi_flash_high_perf_mode(QSPI_TypeDef* QSPIptr)
{
	QSPI_CS_Low(QSPIptr);	
	qspi_flash_send_cmd(QSPIptr, QUAD_SPI_CMD_HIGH_PERFOR_MODE);
	qspi_flash_send_addr(QSPIptr, 0);
	QSPI_CS_High(QSPIptr);

	return; 
}

static void qspi_flash_disable_high_perf_mode(QSPI_TypeDef* QSPIptr)
{
  int i;

  QSPI_CS_Low(QSPIptr);
  qspi_flash_send_cmd(QSPIptr, QUAD_SPI_CMD_DISABLE_HIGH_PERFOR_MODE);
  QSPI_CS_High(QSPIptr);

  /*wait for 3 us for winbond W25Q64BV, the following not presice*/
  for (i = 0; i < 30; i++)
    ;
  return;
}
#endif

//DMA limit 256 Bytes
int qspi_flash_read_page(QSPI_TypeDef *QSPIptr, uint8_t *buf, uint32_t faddr, int count)
{
  int cmd = _4ByteAddrMode ? QUAD_SPI_FLASH_CMD_READ_WITH_4BYTE_ADDR : QUAD_SPI_FLASH_CMD_READ;
  QSPI_CS_Low(QSPIptr);
#ifdef TEST_SPI_MSB_TX_LSB_RX
  QSPI_FirstBitSet(QSPIptr, FLASH_FIRST_BIT_MODE);
  qspi_flash_send_cmd(QSPIptr, cmd);
  qspi_flash_send_addr(QSPIptr, faddr);

  if (TEST_SPI_MSB_TX_LSB_RX == 1) //tx msb, rx lsb
    QSPI_FirstBitSet(QSPIptr, QSPI_FirstBit_LSB);
  else
    QSPI_FirstBitSet(QSPIptr, QSPI_FirstBit_MSB);

  qspi_flash_read_buf(QSPIptr, buf, count);
#else
  qspi_flash_send_cmd(QSPIptr, cmd);
  qspi_flash_send_addr(QSPIptr, faddr);
  qspi_flash_read_buf(QSPIptr, buf, count);
#endif
  QSPI_CS_High(QSPIptr);
  return 0;
}

/**
  * @brief  Read Data instruction read data. 
  * @param  QSPIptr: the QUAD spi controller base address. 
  * @param  buf: read buffer to  contain read data.
  * @param  faddr: qspi flash internal address. 
  * @param  count: read data length. 
  * @retval 0: ok,other: fail  
  */
int qspi_flash_read(QSPI_TypeDef *QSPIptr, uint8_t *buf, uint32_t faddr, int count)
{
  int i;
  int pages;
  int remains; // first page remains
  int w_bytes;

  remains = (1 << QSPI_FLASH_PAGESHIFT) - (faddr & ((1 << QSPI_FLASH_PAGESHIFT) - 1));
  if (count <= remains)
  {
    remains = count;
  }

  // count left
  count -= remains;
  // total pages going to write
  pages = ((count - 1) >> QSPI_FLASH_PAGESHIFT) + 1;
  // send first page remains
  qspi_flash_read_page(QSPIptr, buf, faddr, remains);

  faddr += remains;
  buf += remains;
  for (i = 0; i < pages; i++)
  {
    if (count > (1 << QSPI_FLASH_PAGESHIFT))
    {
      w_bytes = (1 << QSPI_FLASH_PAGESHIFT);
    }
    else
    {
      w_bytes = count;
    }
    // send remain pages
    qspi_flash_read_page(QSPIptr, buf, faddr, w_bytes);

    faddr += w_bytes;
    buf += w_bytes;
    count -= w_bytes;
  }
  return 0;
}

//DMA limit 256 Bytes
int qspi_flash_dual_read_page(QSPI_TypeDef *QSPIptr, uint8_t *buf, uint32_t faddr, int count)
{
  int cmd = _4ByteAddrMode ? QUAD_SPI_CMD_FAST_READ_DUAL_OUTPUT_WITH_4BYTE_ADDR : QUAD_SPI_CMD_FAST_READ_DUAL_OUTPUT;
  QSPI_CS_Low(QSPIptr);
  qspi_flash_send_cmd(QSPIptr, cmd);
  qspi_flash_send_addr(QSPIptr, faddr);
  qspi_flash_send_dummy(QSPIptr, 1);
  QSPI_ModeSet(QSPIptr, QSPI_DUAL); /*change the controller to DUAL*/
  qspi_flash_read_buf(QSPIptr, buf, count);
  QSPI_CS_High(QSPIptr);

  QSPI_ModeSet(QSPIptr, QSPI_STD); /*change the controller to STD*/
  return 0;
}

/**
  * @brief  Fast Read Dual Output instruction read data.
  * @param  QSPIptr: the QUAD spi controller base address.  
  * @param  buf: read buffer to  contain read data. 
  * @param  faddr: qspi flash internal address.  
  * @param  count: read data length.  
  * @retval 0: ok,other: fail  
  */
int qspi_flash_dual_read(QSPI_TypeDef *QSPIptr, uint8_t *buf, uint32_t faddr, int count)
{
  int i;
  int pages;
  int remains; // first page remains
  int w_bytes;

  remains = (1 << QSPI_FLASH_PAGESHIFT) - (faddr & ((1 << QSPI_FLASH_PAGESHIFT) - 1));
  if (count <= remains)
  {
    remains = count;
  }

  // count left
  count -= remains;
  // total pages going to write
  pages = ((count - 1) >> QSPI_FLASH_PAGESHIFT) + 1;
  // send first page remains
  qspi_flash_dual_read_page(QSPIptr, buf, faddr, remains);

  faddr += remains;
  buf += remains;
  for (i = 0; i < pages; i++)
  {
    if (count > (1 << QSPI_FLASH_PAGESHIFT))
    {
      w_bytes = (1 << QSPI_FLASH_PAGESHIFT);
    }
    else
    {
      w_bytes = count;
    }
    // send remain pages
    qspi_flash_dual_read_page(QSPIptr, buf, faddr, w_bytes);

    faddr += w_bytes;
    buf += w_bytes;
    count -= w_bytes;
  }
  return 0;
}

//DMA limit 256 Bytes
int qspi_flash_dual_io_read_page(QSPI_TypeDef* QSPIptr, uint8_t * buf, uint32_t faddr, int count)
{
  int cmd = _4ByteAddrMode ? QUAD_SPI_CMD_FAST_READ_DUAL_IO_WITH_4BYTE_ADDR : QUAD_SPI_CMD_FAST_READ_DUAL_IO;
#ifdef QUAD_SPI_CMD_HIGH_PERFOR_MODE
  qspi_flash_high_perf_mode(QSPIptr);
#endif

  QSPI_CS_Low(QSPIptr);

  QSPI_ModeSet(QSPIptr, QSPI_DUAL); /*change the controller to DUAL*/
  QSPI_WriteCMD(QSPIptr, cmd);
  qspi_flash_send_addr(QSPIptr, faddr);
  qspi_flash_send_dummy(QSPIptr, 1);
  qspi_flash_read_buf(QSPIptr, buf, count);
  QSPI_CS_High(QSPIptr);

  QSPI_ModeSet(QSPIptr, QSPI_STD); /*change the controller to STD*/

#ifdef QUAD_SPI_CMD_HIGH_PERFOR_MODE
  /*disable high performance mode*/
  qspi_flash_disable_high_perf_mode(QSPIptr);
#endif

  return 0;
}

/**
  * @brief  Fast Read Dual I/O instruction read data.
  * @param  QSPIptr: the QUAD spi controller base address.  
  * @param  buf: read buffer to  contain read data. 
  * @param  faddr: qspi flash internal address.  
  * @param  count: read data length.  
  * @retval 0: ok,other: fail 
  */
int qspi_flash_dual_io_read(QSPI_TypeDef* QSPIptr, uint8_t * buf, uint32_t faddr, int count)
{
  int i;
  int pages;
  int remains; // first page remains
  int w_bytes;

  remains = (1 << QSPI_FLASH_PAGESHIFT) - (faddr & ((1 << QSPI_FLASH_PAGESHIFT) - 1));
  if (count <= remains)
  {
    remains = count;
  }

  // count left
  count -= remains;
  // total pages going to write
  pages = ((count - 1) >> QSPI_FLASH_PAGESHIFT) + 1;
  // send first page remains
  qspi_flash_dual_io_read_page(QSPIptr, buf, faddr, remains);

  faddr += remains;
  buf += remains;
  for (i = 0; i < pages; i++)
  {
    if (count > (1 << QSPI_FLASH_PAGESHIFT))
    {
      w_bytes = (1 << QSPI_FLASH_PAGESHIFT);
    }
    else
    {
      w_bytes = count;
    }
    // send remain pages
    qspi_flash_dual_io_read_page(QSPIptr, buf, faddr, w_bytes);

    faddr += w_bytes;
    buf += w_bytes;
    count -= w_bytes;
  }
  return 0;
}

//DMA limit 256 Bytes
int qspi_flash_quad_io_read_page(QSPI_TypeDef* QSPIptr, uint8_t * buf, uint32_t faddr, int count)
{

  int cmd = _4ByteAddrMode ? QUAD_SPI_CMD_FAST_READ_QUAD_IO_WITH_4BYTE_ADDR : QUAD_SPI_CMD_FAST_READ_QUAD_IO;
#ifdef QUAD_SPI_CMD_HIGH_PERFOR_MODE
	qspi_flash_high_perf_mode(QSPIptr);
#endif
	
	QSPI_PinSwitch(QSPIptr, 1); //only after QUAD_ENABLE

	QSPI_CS_Low(QSPIptr);
	QSPI_WriteCMD(QSPIptr, cmd);/*switch the controller to quad function*/
	QSPI_ModeSet(QSPIptr, QSPI_QUAD);
	qspi_flash_send_addr(QSPIptr, faddr);
	qspi_flash_send_dummy(QSPIptr, 3);
	qspi_flash_read_buf(QSPIptr, buf,count);
	QSPI_CS_High(QSPIptr);

	QSPI_ModeSet(QSPIptr, QSPI_STD); /*change the controller to STD*/
	QSPI_PinSwitch(QSPIptr, 0); //just before QUAD_DISABLE

#ifdef QUAD_SPI_CMD_HIGH_PERFOR_MODE
	/*disable high performance mode*/ 
	qspi_flash_disable_high_perf_mode(QSPIptr);
#endif

	return 0;	
}


/**
  * @brief  Fast Read Quad I/O instruction read data.
  * @param  QSPIptr: the QUAD spi controller base address.  
  * @param  buf: read buffer to  contain read data. 
  * @param  faddr: qspi flash internal address.  
  * @param  count: read data length.  
  * @retval 0: ok,other: fail
  */
int qspi_flash_quad_io_read(QSPI_TypeDef* QSPIptr, uint8_t * buf, uint32_t faddr, int count)
{
  int i;
  int pages;
  int remains; // first page remains
  int w_bytes;

  remains = (1 << QSPI_FLASH_PAGESHIFT) - (faddr & ((1 << QSPI_FLASH_PAGESHIFT) - 1));
  if (count <= remains)
  {
    remains = count;
  }

  // count left
  count -= remains;
  // total pages going to write
  pages = ((count - 1) >> QSPI_FLASH_PAGESHIFT) + 1;
  // send first page remains
  qspi_flash_quad_io_read_page(QSPIptr, buf, faddr, remains);

  faddr += remains;
  buf += remains;
  for (i = 0; i < pages; i++)
  {
    if (count > (1 << QSPI_FLASH_PAGESHIFT))
    {
      w_bytes = (1 << QSPI_FLASH_PAGESHIFT);
    }
    else
    {
      w_bytes = count;
    }
    // send remain pages
    qspi_flash_quad_io_read_page(QSPIptr, buf, faddr, w_bytes);

    faddr += w_bytes;
    buf += w_bytes;
    count -= w_bytes;
  }
  return 0;
}

/**
  * @brief  Enable/Reset the flash device for Quad spi bus operation.
  * @param  QSPIptr: the QUAD spi controller base address.   
  * @param  enable: 0/1 to disable/enable quad spi
  * @retval None 
  */
void qspi_flash_switch_qpi(QSPI_TypeDef* QSPIptr, uint8_t enable)
{
	QSPI_CS_Low(QSPIptr);
	if(enable){
		qspi_flash_send_cmd(QSPIptr, QUAD_SPI_FLASH_CMD_EQPI);	
	}else{
		qspi_flash_send_cmd(QSPIptr, QUAD_SPI_FLASH_CMD_RST_QIO);	
	}
	QSPI_CS_High(QSPIptr); 
}

//DMA limit 256 Bytes
int qspi_flash_qpi_read_page(QSPI_TypeDef* QSPIptr, uint8_t * buf, uint32_t faddr, int count)
{
  int cmd = _4ByteAddrMode ? QUAD_SPI_FLASH_CMD_FAST_READ_WITH_4BYTE_ADDR : QUAD_SPI_FLASH_CMD_FAST_READ;
  qspi_flash_switch_qpi(QSPIptr, 1);

  QSPI_PinSwitch(QSPIptr, 1); //only after QUAD_ENABLE
  QSPI_ModeSet(QSPIptr, QSPI_QUAD);
  QSPI_QpiSet(QSPIptr, 1);

  QSPI_CS_Low(QSPIptr);
	QSPI_WriteCMD(QSPIptr, cmd);
	qspi_flash_send_addr(QSPIptr, faddr);
	qspi_flash_send_dummy(QSPIptr, 1);
	qspi_flash_read_buf(QSPIptr, buf,count);
	QSPI_CS_High(QSPIptr);

	qspi_flash_switch_qpi(QSPIptr, 0);
	
	 /*change the controller to STD*/
	QSPI_QpiSet(QSPIptr, 0);
	QSPI_ModeSet(QSPIptr, QSPI_STD);
	QSPI_PinSwitch(QSPIptr, 0); //just before QUAD_DISABLE
	return 0;	
}

/**
  * @brief  Enable Quad Peripheral Interface mode (EQPI) instruction 
  * 		and use support FAST_READ(0Bh) to read data.	
  * @param  QSPIptr: the QUAD spi controller base address.  
  * @param  buf: read buffer to  contain read data. 
  * @param  faddr: qspi flash internal address.  
  * @param  count: read data length.  
  * @retval 0: ok,other: fail  
  */
int qspi_flash_qpi_read(QSPI_TypeDef* QSPIptr, uint8_t * buf, uint32_t faddr, int count)
{
  int i;
  int pages;
  int remains; // first page remains
  int w_bytes;

  remains = (1 << QSPI_FLASH_PAGESHIFT) - (faddr & ((1 << QSPI_FLASH_PAGESHIFT) - 1));
  if (count <= remains)
  {
    remains = count;
  }

  // count left
  count -= remains;
  // total pages going to write
  pages = ((count - 1) >> QSPI_FLASH_PAGESHIFT) + 1;
  // send first page remains
  qspi_flash_qpi_read_page(QSPIptr, buf, faddr, remains);

  faddr += remains;
  buf += remains;
  for (i = 0; i < pages; i++)
  {
    if (count > (1 << QSPI_FLASH_PAGESHIFT))
    {
      w_bytes = (1 << QSPI_FLASH_PAGESHIFT);
    }
    else
    {
      w_bytes = count;
    }
    // send remain pages
    qspi_flash_qpi_read_page(QSPIptr, buf, faddr, w_bytes);

    faddr += w_bytes;
    buf += w_bytes;
    count -= w_bytes;
  }
  return 0;
}

/**
  * @brief  QSPI flash write Enable/Disable
  * @param  QSPIptr: the QUAD spi controller base address.   
  * @param  enable: set 0/1 to disable/enable qspi flash write. 
  * @retval None 
  */
void qspi_flash_write_enable(QSPI_TypeDef* QSPIptr, int enable)
{
  int cmd = enable ? QUAD_SPI_FLASH_CMD_WRITE_ENABLE : QUAD_SPI_FLASH_CMD_WRITE_DISABLE;

  QSPI_CS_Low(QSPIptr);
#ifdef TEST_SPI_MSB_TX_LSB_RX
  QSPI_FirstBitSet(QSPIptr, FLASH_FIRST_BIT_MODE);
  qspi_flash_send_cmd(QSPIptr, cmd);
#else
  qspi_flash_send_cmd(QSPIptr, cmd);
#endif
  QSPI_CS_High(QSPIptr);

  return;
}

/**
  * @brief  Write QSPI flash status 
  * @param  QSPIptr: the QUAD spi controller base address.  
  * @param  buf: write buffer.
  * @param  n: write buffer length. 
  * @retval None 
  */
void qspi_flash_write_status(QSPI_TypeDef* QSPIptr, uint8_t* buf, int n)
{
  volatile uint8_t status = 1;

  //qspi_flash_write_enable will use qspi_cmd, so buf cannot be qspi_cmd
  qspi_flash_write_enable(QSPIptr, 1);

  QSPI_CS_Low(QSPIptr);
#ifdef TEST_SPI_MSB_TX_LSB_RX
  QSPI_FirstBitSet(QSPIptr, FLASH_FIRST_BIT_MODE);
  qspi_flash_send_cmd(QSPIptr, QUAD_SPI_FLASH_CMD_WRITE_STATUS);
  qspi_flash_write_buf(QSPIptr, buf, n);
#else
  qspi_flash_send_cmd(QSPIptr, QUAD_SPI_FLASH_CMD_WRITE_STATUS);
  qspi_flash_write_buf(QSPIptr, buf, n);
#endif
  QSPI_CS_High(QSPIptr);

  status = 1;
  while (status & 1) //is qspi flash busy
  {
    qspi_flash_read_status1(QSPIptr, (char *)&status);
    commonDelay(1000);
  }
}

/**
  * @brief  QSPI flash quad mode disable/enable
  * @param  QSPIptr: the QUAD spi controller base address.  
  * @param  sel: quad spi enable or disable. 
  * 		@arg QUAD_ENABLE:enable quad
  * 		@arg QUAD_DISABLE:disable quad
  * @param  qeMask:quad enable bit; 
  * @retval  
  */
void qspi_flash_quad_function_select(QSPI_TypeDef* QSPIptr, QUADfuncSelect sel, uint8_t qeMask)
{
  volatile uint8_t status = 1;
  volatile uint8_t status2 = 1;

  qspi_flash_read_status1(QSPIptr, (char *)&status);
  qspi_flash_read_status2(QSPIptr, (char *)&status2);
  qspi_flash_status[0] = status;
  if (sel == QUAD_ENABLE)
    qspi_flash_status[1] = status2 | qeMask;
  else
    qspi_flash_status[1] = status2 & (~qeMask);
  qspi_flash_write_status(QSPIptr, qspi_flash_status, 2);
}

/**
  * @brief  Erase QSPI flash block protect
  * @param  QSPIptr: the QUAD spi controller base address.   
  * @note   status&0xC3 is fully compatible:
  * 		(1)ISSI		BP3~BP0
  * 		(2)GD		BP4~BP0		All can be written when BP2~BP0 is 0
  * 		(3)ESMT		BP3~BP0
  * 		(4)WB		TB,BP2~BP0  All can be written when BP2~BP0 is 0
  *			(5)MXIC		BP3~BP0
  * @retval None 
  */
void qspi_flash_global_unprotect(QSPI_TypeDef* QSPIptr)
{
  volatile uint8_t status1 = 1;
  volatile uint8_t status2 = 1;
  qspi_flash_read_status1(QSPIptr, (char *)&status1);
  qspi_flash_read_status2(QSPIptr, (char *)&status2);
  qspi_flash_status[0] = status1 & 0xC3;
  qspi_flash_status[1] = status2;
  qspi_flash_write_status(QSPIptr, qspi_flash_status, 2);
  return;
}

/**
  * @brief  64k block erase
  * @param  QSPIptr: the QUAD spi controller base address.   
  * @param  faddr: qspi flash internal address. 
  * @retval 0: ok,other: fail  
  */
int qspi_flash_erase_block_64k(QSPI_TypeDef* QSPIptr, uint32_t faddr)
{
  volatile uint8_t status = 1;

  int cmd = _4ByteAddrMode ? QUAD_SPI_FLASH_CMD_ERASE_64K_WITH_4BYTE_ADDR : QUAD_SPI_FLASH_CMD_ERASE_64K;
  qspi_flash_write_enable(QSPIptr, 1);
  QSPI_CS_Low(QSPIptr);
#ifdef TEST_SPI_MSB_TX_LSB_RX
  QSPI_FirstBitSet(QSPIptr, FLASH_FIRST_BIT_MODE);
  qspi_flash_send_cmd(QSPIptr, cmd);
  qspi_flash_send_addr(QSPIptr, faddr);
#else
  qspi_flash_send_cmd(QSPIptr, cmd);
  qspi_flash_send_addr(QSPIptr, faddr);
#endif
  QSPI_CS_High(QSPIptr);

  while (status & 1)
  {
    qspi_flash_read_status1(QSPIptr, (char *)&status);
    commonDelay(1000);
  }

  return 0;
}

int qspi_flash_erase_block_4k(QSPI_TypeDef *QSPIptr, uint32_t faddr)
{
  volatile uint8_t status = 1;
  int cmd = _4ByteAddrMode ? QUAD_SPI_FLASH_CMD_ERASE_4K_WITH_4BYTE_ADDR : QUAD_SPI_FLASH_CMD_ERASE_4K;

  qspi_flash_write_enable(QSPIptr, 1);
  QSPI_CS_Low(QSPIptr);
#ifdef TEST_SPI_MSB_TX_LSB_RX
  QSPI_FirstBitSet(QSPIptr, FLASH_FIRST_BIT_MODE);
  qspi_flash_send_cmd(QSPIptr, cmd);
  qspi_flash_send_addr(QSPIptr, faddr);
#else
  qspi_flash_send_cmd(QSPIptr, cmd);
  qspi_flash_send_addr(QSPIptr, faddr);
#endif
  QSPI_CS_High(QSPIptr);

  while (status & 1)
  {
    qspi_flash_read_status1(QSPIptr, (char *)&status);
    commonDelay(1000);
  }

  return 0;
}

/**
  * @brief  Write pazesize data to the QSPI flash
  * @param  QSPIptr: the QUAD spi controller base address. 
  * @param  buf: pointer to the buffer containing the data to be written 
  *         to the QSPI flash. 
  * @param  faddr: qspi flash internel address. 
  * @param  count: qspi flash pagesize. 
  * @retval 0: ok, other: fail 
  */
int qspi_flash_write_page(QSPI_TypeDef* QSPIptr, uint8_t * buf, uint32_t faddr, int count)
{
  volatile uint8_t status = 1;
  int cmd = _4ByteAddrMode ? QUAD_SPI_FLASH_CMD_WRITE_WITH_4BYTE_ADDR : QUAD_SPI_FLASH_CMD_WRITE;

  qspi_flash_write_enable(QSPIptr, 1);
  QSPI_CS_Low(QSPIptr);
#ifdef TEST_SPI_MSB_TX_LSB_RX
  QSPI_FirstBitSet(QSPIptr, FLASH_FIRST_BIT_MODE);
  qspi_flash_send_cmd(QSPIptr, cmd);
  qspi_flash_send_addr(QSPIptr, faddr);

  if (TEST_SPI_MSB_TX_LSB_RX == 1) //tx msb, rx lsb
    QSPI_FirstBitSet(QSPIptr, QSPI_FirstBit_MSB);
  else
    QSPI_FirstBitSet(QSPIptr, QSPI_FirstBit_LSB);

  qspi_flash_write_buf(QSPIptr, buf, count);
#else
  qspi_flash_send_cmd(QSPIptr, cmd);
  qspi_flash_send_addr(QSPIptr, faddr);
  qspi_flash_write_buf(QSPIptr, buf, count);
#endif
  QSPI_CS_High(QSPIptr);

  while (status & 1)
  {
    qspi_flash_read_status1(QSPIptr, (char *)&status);
    commonDelay(1000);
  }

  return count;
}

/**
  * @brief  Write data to the QSPI flash
  * @param  QSPIptr: the QUAD spi controller base address. 
  * @param  buf: pointer to the buffer containing the data to be written 
  *         to the QSPI flash. 
  * @param  faddr: qspi flash internel address. 
  * @param  count: write data length. 
  * @retval 0: ok, other: fail 
  */
int qspi_flash_write(QSPI_TypeDef* QSPIptr, uint8_t * buf, uint32_t faddr, int count)
{
  int i;
  int pages;
  int remains; // first page remains
  int w_bytes;

  qspi_flash_global_unprotect(QSPIptr);
  remains = (1 << QSPI_FLASH_PAGESHIFT) - (faddr & ((1 << QSPI_FLASH_PAGESHIFT) - 1)); //faddr最后一个page剩余字节
  if (count <= remains)
  {
    remains = count;
  }

  // count left
  count -= remains;
  // total pages going to write
  pages = ((count - 1) >> QSPI_FLASH_PAGESHIFT) + 1;
  // send first page remains
  qspi_flash_write_page(QSPIptr, buf, faddr, remains);

  faddr += remains;
  buf += remains;
  for (i = 0; i < pages; i++)
  {
    if (count > (1 << QSPI_FLASH_PAGESHIFT))
    {
      w_bytes = (1 << QSPI_FLASH_PAGESHIFT);
    }
    else
    {
      w_bytes = count;
    }
    // send remain pages
    qspi_flash_write_page(QSPIptr, buf, faddr, w_bytes);

    faddr += w_bytes;
    buf += w_bytes;
    count -= w_bytes;
  }
  return 0;
}

/**
  * @brief  Enable/Disable 4 byte address mode.
  * @param  QSPIptr: the QUAD spi controller base address. 
  * @param  enable: set 1/0 to enable/disable 4 byte address mode.
  * @retval none. 
  */
void qspi_flash_4Byte_address_enable(QSPI_TypeDef *QSPIptr, uint8_t enable)
{
  int cmd = enable ? QUAD_SPI_CMD_ENTER_4BYTE_ADDR_MODE : QUAD_SPI_CMD_EXIT_4BYTE_ADDR_MODE;
  _4ByteAddrMode = enable ? ENABLE : DISABLE;
  QSPI_CS_Low(QSPIptr);
#ifdef TEST_SPI_MSB_TX_LSB_RX
  QSPI_FirstBitSet(QSPIptr, FLASH_FIRST_BIT_MODE);
  qspi_flash_send_cmd(QSPIptr, cmd);
#else
  qspi_flash_send_cmd(QSPIptr, cmd);
#endif
  QSPI_CS_High(QSPIptr);
  return;
}

/**
  * @brief  Write pazesize data to the QSPI flash
  * @param  QSPIptr: the QUAD spi controller base address. 
  * @param  buf: pointer to the buffer containing the data to be written 
  *         to the QSPI flash. 
  * @param  faddr: qspi flash internel address. 
  * @param  count: qspi flash pagesize. 
  * @retval 0: ok, other: fail 
  */
int qspi_flash_quad_io_write_page(QSPI_TypeDef* QSPIptr, uint8_t * buf, uint32_t faddr, int count)
{
  volatile uint8_t status = 1;
  int cmd = _4ByteAddrMode ? QUAD_SPI_FLASH_CMD_QUAD_WRITE_WITH_4BYTE_ADDR : QUAD_SPI_FLASH_CMD_QUAD_WRITE;

  QSPI_PinSwitch(QSPIptr, 1); //only after QUAD_ENABLE
  qspi_flash_write_enable(QSPIptr, 1);
  QSPI_CS_Low(QSPIptr);
  qspi_flash_send_cmd(QSPIptr, cmd);
  qspi_flash_send_addr(QSPIptr, faddr);
  QSPI_ModeSet(QSPIptr, QSPI_QUAD);
  qspi_flash_write_buf(QSPIptr, buf, count);
  QSPI_CS_High(QSPIptr);
  QSPI_ModeSet(QSPIptr, QSPI_STD);
  QSPI_PinSwitch(QSPIptr, 0); //only after QUAD_ENABLE

  while (status & 1)
  {
    qspi_flash_read_status1(QSPIptr, (char *)&status);
    commonDelay(1000);
  }

  return count;
}

/**
  * @brief  Fast Write Quad I/O instruction to write QSPI flash
  * @param  QSPIptr: the QUAD spi controller base address. 
  * @param  buf: pointer to the buffer containing the data to be written 
  *         to the QSPI flash. 
  * @param  faddr: qspi flash internel address. 
  * @param  count: write data length. 
  * @retval 0: ok, other: fail 
  */
int qspi_flash_quad_io_write(QSPI_TypeDef* QSPIptr, uint8_t * buf, uint32_t faddr, int count)
{
  int i;
  int pages;
  int remains; // first page remains
  int w_bytes;

  qspi_flash_global_unprotect(QSPIptr);
  remains = (1 << QSPI_FLASH_PAGESHIFT) - (faddr & ((1 << QSPI_FLASH_PAGESHIFT) - 1)); //faddr最后一个page剩余字节
  if (count <= remains)
  {
    remains = count;
  }

  // count left
  count -= remains;
  // total pages going to write
  pages = ((count - 1) >> QSPI_FLASH_PAGESHIFT) + 1;
  // send first page remains
  qspi_flash_quad_io_write_page(QSPIptr, buf, faddr, remains);

  faddr += remains;
  buf += remains;
  for (i = 0; i < pages; i++)
  {
    if (count > (1 << QSPI_FLASH_PAGESHIFT))
    {
      w_bytes = (1 << QSPI_FLASH_PAGESHIFT);
    }
    else
    {
      w_bytes = count;
    }
    // send remain pages
    qspi_flash_quad_io_write_page(QSPIptr, buf, faddr, w_bytes);

    faddr += w_bytes;
    buf += w_bytes;
    count -= w_bytes;
  }
  return 0;
}

/******************* (C) COPYRIGHT 2013 Alpscale *****END OF FILE****/


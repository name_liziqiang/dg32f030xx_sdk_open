/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "m1130_uart.h" 
#include "m1130_gpio.h" 
#include "m1130_rcc.h"	 
#include "misc.h"
#include "m1130_dma.h"
#include "m1130_tim.h"
#include <string.h>
#include "ADC_R.h"



typedef enum
{
  UART0_GP2,
  UART0_GP7,
  UART1_GP0,
  UART1_GP2,
  UART2_GP0,
  UART2_GP7,
} UartPinGroup;

void delay_ms(uint16_t ms)
{
	uint16_t i;
	for(; ms>0; ms--)
		for(i=0; i<4000; i++);
}


uint8_t TIMES=0;
uint8_t rx_buf[256];
uint16_t rxlen;
uint16_t rx_flag;
uint16_t rx_ptr;
#define UART_RXIFLSEL_max  14-1 //这个值为fifo深度减一
void UART1_IRQHandler(void)
{

//  if (UART_GetITStatus(UART1, UART_IT_RXIS))
//  {
    if((UART1->INTR & UART_IT_RXIS) != RESET)
    {
	  for(int i=0;i<UART_RXIFLSEL_max;i++)      //fifo为14个字节进入一次接收中断，每次从fifo读出fifo深度减一个字节，需要留一个字节
	  {
        rx_buf[rx_ptr++] = UART1->DATA;
	  }
      TIMES++;
     UART1->INTR_CLR = UART_INTR_RXIS;
    }

//    if (UART_GetITStatus(UART1, UART_IT_RTIS))
//    {
      if((UART1->INTR & UART_IT_RTIS) != RESET)
    {
    while (!UART_GetFlagStatus(UART1, UART_FLAG_RXFE))
            rx_buf[rx_ptr++] = UART1->DATA;
            rxlen =rx_ptr;
            rx_flag = 1;
            rx_ptr = 0;
        UART1->INTR_CLR = UART_INTR_RTIS;
        }

  
}

/**
  * @brief  configure uart
  * @param  UARTx: Select the UART peripheral.
  *   This parameter can be one of the following values:
  *   UARTx, x:[0:1:2].
  * @param  BaudRate: Set uart baudrate
  * @param  PinGroup: Select uart pin group to set pin mux
  *   This parameter can be one of the following values:
  *   UART0_GP2/7, UART1_GP0/2, UART2_GP0/7
  * @param  RxBuf: Set uart receive buffer
  * @retval:none
  */

void UART_Configuration(UART_TypeDef *UARTx, uint32_t BaudRate, UartPinGroup PinGroup)
{
  UART_InitTypeDef UART_InitStructure;
  NVIC_InitTypeDef NVIC_InitStruct;

  RCC_UARTCLKSel(RCC_UARTCLK_SOURCE_SYSPLL);
  if (UARTx == UART0)
  {
    RCC_ResetAHBCLK(1 << AHBCLK_BIT_UART0);
    RCC_SETCLKDivider(RCC_CLOCKFREQ_UART0CLK, 4);
    NVIC_InitStruct.NVIC_IRQChannel = UART0_IRQn;
  }
  else if (UARTx == UART1)
  {
    RCC_ResetAHBCLK(1 << AHBCLK_BIT_UART1);
    RCC_SETCLKDivider(RCC_CLOCKFREQ_UART1CLK, 4);
    NVIC_InitStruct.NVIC_IRQChannel = UART1_IRQn;
  }
  else if (UARTx == UART2)
  {
    RCC_ResetAHBCLK(1 << AHBCLK_BIT_UART2);
    RCC_SETCLKDivider(RCC_CLOCKFREQ_UART2CLK, 4);
    NVIC_InitStruct.NVIC_IRQChannel = UART2_IRQn;
  }

  switch (PinGroup)
  {
  case UART0_GP2:
    GPIO_SetPinMux(GPIO0, GPIO_Pin_16, GPIO_FUNCTION_3);
    GPIO_SetPinMux(GPIO0, GPIO_Pin_17, GPIO_FUNCTION_3);
    break;
  case UART0_GP7:
    GPIO_SetPinMux(GPIO1, GPIO_Pin_24, GPIO_FUNCTION_2);
    GPIO_SetPinMux(GPIO1, GPIO_Pin_25, GPIO_FUNCTION_2);
    break;
  case UART1_GP0:
    GPIO_SetPinMux(GPIO0, GPIO_Pin_4, GPIO_FUNCTION_2);
    GPIO_SetPinMux(GPIO0, GPIO_Pin_3, GPIO_FUNCTION_2);
    break;
  case UART1_GP2:
    GPIO_SetPinMux(GPIO0, GPIO_Pin_18, GPIO_FUNCTION_2);
    GPIO_SetPinMux(GPIO0, GPIO_Pin_19, GPIO_FUNCTION_2);
    break;
  case UART2_GP0:
    GPIO_SetPinMux(GPIO0, GPIO_Pin_0, GPIO_FUNCTION_3);
    GPIO_SetPinMux(GPIO0, GPIO_Pin_1, GPIO_FUNCTION_3);
    break;
  case UART2_GP7:
    GPIO_SetPinMux(GPIO1, GPIO_Pin_26, GPIO_FUNCTION_2);
    GPIO_SetPinMux(GPIO1, GPIO_Pin_27, GPIO_FUNCTION_2);
    break;
  default:
    break;
  }

  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
  NVIC_InitStruct.NVIC_IRQChannelPriority = 2;
  NVIC_Init(&NVIC_InitStruct);

  UART_Reset(UARTx);
  UART_StructInit(&UART_InitStructure);
  UART_InitStructure.UART_BaudRate = BaudRate;
  UART_InitStructure.UART_RXIFLSEL = UART_RXIFLSEL_14;
  UART_Init(UARTx, &UART_InitStructure);
  /* set timeout to 8 uart clk cycle len */
  UARTx->CTRL0_CLR = UART_CTRL0_RXTIMEOUT;
  UARTx->CTRL0_SET = 8 << 16;

//  UART_ITConfig(UARTx, UART_IT_RXIEN | UART_IT_RTIEN, ENABLE);
  UART_Cmd(UARTx, ENABLE);
}
	#define 	LED_PIN		GPIO_Pin_2
	#define 	LED_PORT	GPIO0
void LED_Init(void)
{
 
    GPIO_SetPinMux(LED_PORT, LED_PIN, GPIO_FUNCTION_0);
	GPIO_SetPinDir(LED_PORT, LED_PIN, GPIO_Mode_OUT);
	GPIO_SetPin(LED_PORT, LED_PIN);
}
 


int main()
{

    LED_Init();
    
    UART_Configuration(UART1, 115200, UART1_GP2);
    printf("\r\nDG30F030CC开发板 ADC实验\r\n");
    myADC_Init(ADC0);
	while(1){
        ADC_Test();
    }
}

/* Private function prototypes -----------------------------------------------*/
#ifdef __GNUC__
  /* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
     set to 'Yes') calls __io_putchar() */
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Retargets the C library printf function to the UART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE
{
	/* Place your implementation of fputc here */
	/* e.g. write a character to the UART */
	UART_SendData(UART1, (uint8_t) ch);
	
	/* Loop until the end of transmission */
	while (UART_GetFlagStatus(UART1, UART_FLAG_TXFE) == RESET)
	{}
	
	return ch;
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  printf("Wrong parameters value: file %s on line %d\r\n", file, line);
  /* Infinite loop */
  while (1)
  {
  }
}
#endif

#include "ADC_R.h"
#include "m1130_adc.h"
#include "m1130_gpio.h"
#include "m1130_rcc.h"
 #include "stdio.h"
#include "m1130_tim.h"
#include "m1130_rcc.h"	 

#define testADCCHANNEL ADC_CHANNEL1


uint16_t adbuf[16];
extern void delay_ms(uint16_t ms);
uint8_t ADFLAG;


void TIM_init(void)
{
	//TIM1  CH4 输出PWM
	RCC->AHBCLKCTRL0_SET|=(1<<AHBCLK_BIT_TIM1);//开启定时器时钟
	RCC->PWMCLKDIV = 0X01;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Prescaler=95;//96Mhz/(95+1)=1Mhz
	TIM_TimeBaseStructure.TIM_Period=10000 ;//计数到10000为10ms
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);
	
	//SYSAHBCLKDIV=4 在SystemInit已经初始化了 AHB时钟96Mhz	
	TIM_OCStructInit(&TIM_OCInitStructure);
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1; //选择定时器模式:TIM脉冲宽度调制模式2
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; //比较输出使能
	TIM_OCInitStructure.TIM_Pulse = 5; //设置待装入捕获比较寄存器的脉冲值
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; //输出极性:TIM输出比较极性高
	TIM_OC4Init(TIM1, &TIM_OCInitStructure);  //根据TIM_OCInitStruct中指定的参数初始化外设TIMx     //TIM1_CH1

	TIM_OC4PreloadConfig(TIM1,TIM_OCPreload_Enable);
	TIM_ARRPreloadConfig(TIM1,ENABLE);
	TIM_CtrlPWMOutputs(TIM1, ENABLE);
	TIM_Cmd(TIM1,ENABLE);
}

 void myADC_Init(ADC_TypeDef *ADCx)
{
    ADC_InitTypeDef ADC_InitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;

	ADC_InitStruct.ADC_Channel = testADCCHANNEL;//使用通道2和通道3
	ADC_InitStruct.ADC_SingleTrigConvMode = ADC_SingleTrigConv_Disable;
	ADC_InitStruct.ADC_TrigConvSrcSel = ADC_Internal_TrigSrc_TIM1_CH4;
	ADC_InitStruct.ADC_TrigConvEdge =  ADC_TrigEdge_Rising;
	ADC_InitStruct.ADC_First_DisSampleNum = 0;//第一次采用丢弃前面三个采样
	ADC_InitStruct.ADC_Exch_DisSampleNum = 0;//切换通道 丢弃前面三个采样
	ADC_InitStruct.ADC_SampFrqSel = ADC_285K_SampFrq;
	ADC_InitStruct.ADC_ClkDiv = 12;

	ADC_RCC_Init(ADC0, ADC_InitStruct.ADC_ClkDiv);
	ADC_Init(ADC0,&ADC_InitStruct);  
    ADC_ITConfig(ADC0, ENABLE);

	
      NVIC_InitStruct.NVIC_IRQChannel = ADC0_IRQn;
      NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
      NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
      NVIC_Init(&NVIC_InitStruct);
	
	TIM_init();
	
	ADC_Run(ADC0, ENABLE);
    TIM_Cmd(TIM1,ENABLE);
}

void ADC0_IRQHandler(void)
{

   if(ADC_GetITStatus(ADC0, testADCCHANNEL))
    {
	     adbuf[0]=ADC_GetConvValue(ADC0, testADCCHANNEL);
         ADC0->CTRL1_CLR =testADCCHANNEL;
         GPIO0->DT_TOG=GPIO_Pin_2;
    }
}
void ADC_Test()
{
 delay_ms(4000);
}

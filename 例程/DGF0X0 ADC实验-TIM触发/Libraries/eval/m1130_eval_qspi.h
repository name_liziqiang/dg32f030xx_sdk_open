/**
  ******************************************************************************
  * @file    m1130_eval_qspi.h
  * @author  Alpscale Application Team
  * @version V1.0.0
  * @date    27-November-2013
  * @brief   This file contains all the functions prototypes for the 
  *              m1130_eval_qspi.c driver.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alpscale</center></h2>
  ******************************************************************************  
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __M1130_EVAL_QSPI_H
#define __M1130_EVAL_QSPI_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "m1130_eval.h"
#include "m1130_gpio.h"

/** @addtogroup Utilities
  * @{
  */ 

/** @addtogroup M1130_EVAL
  * @{
  */
  
/** @addtogroup M1130_EVAL_QSPI
  * @{
  */  


  
//ESMT EN25Q80
#define QUAD_SPI_FLASH_CMD_WRITE_STATUS                  0x01
#define QUAD_SPI_FLASH_CMD_WRITE                         0x02
#define QUAD_SPI_FLASH_CMD_WRITE_WITH_4BYTE_ADDR         0x12
#define QUAD_SPI_FLASH_CMD_READ                          0x03
#define QUAD_SPI_FLASH_CMD_READ_WITH_4BYTE_ADDR          0x13
#define QUAD_SPI_FLASH_CMD_WRITE_DISABLE                 0x04
#define QUAD_SPI_FLASH_CMD_WRITE_ENABLE                  0x06
#define QUAD_SPI_FLASH_CMD_READ_STATUS1                  0x05
#define QUAD_SPI_FLASH_CMD_READ_STATUS2                  0x35

#define QUAD_SPI_FLASH_CMD_ERASE_4K                      0x20
#define QUAD_SPI_FLASH_CMD_ERASE_4K_WITH_4BYTE_ADDR      0x21
#define QUAD_SPI_FLASH_CMD_ERASE_32K                     0x52
#define QUAD_SPI_FLASH_CMD_ERASE_64K                     0xD8
#define QUAD_SPI_FLASH_CMD_ERASE_64K_WITH_4BYTE_ADDR     0xDC
#define QUAD_SPI_FLASH_CMD_ERASE_ALL                     0x60 // 0xC7

#define QUAD_SPI_FLASH_CMD_READ_ID                       0x9F

#define QUAD_SPI_FLASH_CMD_QUAD_WRITE	  			      0x32
#define QUAD_SPI_FLASH_CMD_QUAD_WRITE_WITH_4BYTE_ADDR	  0x34

#define QUAD_SPI_CMD_FAST_READ_DUAL_OUTPUT                      0x3B 
#define QUAD_SPI_CMD_FAST_READ_DUAL_OUTPUT_WITH_4BYTE_ADDR      0x3C 
#define QUAD_SPI_CMD_FAST_READ_DUAL_IO			                    0xBB
#define QUAD_SPI_CMD_FAST_READ_DUAL_IO_WITH_4BYTE_ADDR			    0xBC
#define QUAD_SPI_CMD_FAST_READ_QUAD_IO	                        0xEB
#define QUAD_SPI_CMD_FAST_READ_QUAD_IO_WITH_4BYTE_ADDR	        0xEC


#define QUAD_SPI_CMD_ENTER_4BYTE_ADDR_MODE	      0xB7
#define QUAD_SPI_CMD_EXIT_4BYTE_ADDR_MODE	        0xE9
//#define QUAD_SPI_FLASH_CMD_QUAD_WRITE	  0x32
//#define QUAD_SPI_CMD_FAST_READ_QUAD_OUTPUT	  0x6b

//#define QUAD_SPI_CMD_HIGH_PERFOR_MODE	0xa3
//#define QUAD_SPI_CMD_DISABLE_HIGH_PERFOR_MODE	0xab

#define QUAD_SPI_FLASH_CMD_EQPI      	0x38
#define QUAD_SPI_FLASH_CMD_RST_QIO      0xFF	//exit QPI, exit Fast Read Enhance
#define QUAD_SPI_FLASH_CMD_FAST_READ                    0x0B
#define QUAD_SPI_FLASH_CMD_FAST_READ_WITH_4BYTE_ADDR    0x0C

#define QSPI_FLASH_PAGESHIFT  8 // 256 Byte


#define	__ALIGN4				__align(4)

/**
  * @}
  */ 
  

/** @defgroup M1130_EVAL_QSPI_Flash_Types
  * @{
  */ 
typedef enum {QUAD_ENABLE, QUAD_DISABLE} QUADfuncSelect;
/**
  * @}
  */ 

//QSPI0 => GP4
//QSPI1 => GP3/GP1
//QSPI2 => GP5
typedef enum {QSPI0_GP4, QSPI1_GP1, QSPI1_GP3, QSPI2_GP5} QSPIxPOS;

/** @defgroup M1130_EVAL_QSPI_FLASH_Functions
  * @{
  */

void QSPI_PinSwitch(QSPI_TypeDef* QSPIptr, int isQuad);

int qspi_flash_init(QSPI_TypeDef* QSPIptr,
                    uint8_t rcc_clk_div,
                    uint32_t slaveMode,
                    int internal_clk_div,
                    int internal_clk_rate,
                    QSPIxPOS qspixpos,
                    char initD2D3);
void qspi_flash_deinit(QSPI_TypeDef* QSPIptr);

void qspi_flash_4Byte_address_enable(QSPI_TypeDef *QSPIptr, uint8_t enable);

void qspi_flash_read_status1(QSPI_TypeDef* QSPIptr, char * status);
void qspi_flash_read_status2(QSPI_TypeDef* QSPIptr, char * status);
int qspi_flash_read_id(QSPI_TypeDef* QSPIptr,uint8_t* buf);
int qspi_flash_read(QSPI_TypeDef* QSPIptr, uint8_t * buf, uint32_t faddr, int count);
int qspi_flash_dual_read(QSPI_TypeDef* QSPIptr, uint8_t* buf, uint32_t faddr, int count);
int qspi_flash_dual_io_read(QSPI_TypeDef* QSPIptr, uint8_t * buf, uint32_t faddr, int count);
int qspi_flash_quad_io_read(QSPI_TypeDef* QSPIptr, uint8_t * buf, uint32_t faddr, int count);
int qspi_flash_qpi_read(QSPI_TypeDef* QSPIptr, uint8_t * buf, uint32_t faddr, int count);

void qspi_flash_write_enable(QSPI_TypeDef* QSPIptr, int enable);
void qspi_flash_write_status(QSPI_TypeDef* QSPIptr, uint8_t* buf, int n);
void qspi_flash_quad_function_select(QSPI_TypeDef* QSPIptr, QUADfuncSelect sel, uint8_t qeMask);
void qspi_flash_global_unprotect(QSPI_TypeDef* QSPIptr);
int qspi_flash_erase_block_64k(QSPI_TypeDef* QSPIptr, uint32_t faddr);
int qspi_flash_erase_block_4k(QSPI_TypeDef* QSPIptr, uint32_t faddr);
int qspi_flash_write(QSPI_TypeDef* QSPIptr, uint8_t * buf, uint32_t faddr, int count);
int qspi_flash_quad_io_write(QSPI_TypeDef* QSPIptr, uint8_t * buf, uint32_t faddr, int count);

int qspi_flash_write_buf(QSPI_TypeDef* QSPIptr, uint8_t * buf, int n);
int qspi_flash_read_buf(QSPI_TypeDef* QSPIptr, uint8_t * buf, int n);
int SpiRX(QSPI_TypeDef* QSPIptr,uint8_t *data,uint32_t num);
int SpiTX(QSPI_TypeDef* QSPIptr,uint8_t *data,uint32_t num);

#ifdef __cplusplus
}
#endif

#endif /* __M1130_EVAL_QSPI_H */
/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/******************* (C) COPYRIGHT 2013 Alpscale *****END OF FILE****/


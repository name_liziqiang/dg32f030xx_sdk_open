#include "ADC_R.h"
#include "m1130_adc.h"
#include "m1130_gpio.h"
#include "m1130_rcc.h"
#include "stdio.h"
#include "m1130_tim.h"
#include "m1130_rcc.h"	 

extern void delay_ms(uint16_t ms);
 void myADC_Init(ADC_TypeDef *ADCx)
{
	ADC_InitTypeDef ADC_InitStruct;

	ADC_InitStruct.ADC_Channel =ADC_CHANNEL2|ADC_CHANNEL3;//使用通道2和通道3
	ADC_InitStruct.ADC_SingleTrigConvMode = ADC_SingleTrigConv_Disable;
	ADC_InitStruct.ADC_TrigConvSrcSel = ADC_Internal_TrigSrc_TIM1_CH4;
	ADC_InitStruct.ADC_TrigConvEdge =  ADC_TrigEdge_Software;//配置为软件触发
	ADC_InitStruct.ADC_First_DisSampleNum = 0;//第一次采用丢弃前面三个采样
	ADC_InitStruct.ADC_Exch_DisSampleNum =0;//切换通道 丢弃前面三个采样
	ADC_InitStruct.ADC_SampFrqSel = ADC_285K_SampFrq;//此处设置不是指256K，是指8分频
	ADC_InitStruct.ADC_ClkDiv = 64;
	ADC_Init(ADC0,&ADC_InitStruct);  
	ADC_Run(ADC0, ENABLE);
}
#define TRIMCOUNT             16
uint16_t TrimLowValue[TRIMCOUNT];
uint16_t TrimHighValue[TRIMCOUNT];

void ADC_Test()
{
	for(int i = 0; i < TRIMCOUNT; i++){
		ADC_StartSoftwareTrigConv(ADC0);//软件触发一次转换
		while((ADC0->CTRL1 & (ADC_CHANNEL3 )) != (ADC_CHANNEL3 ));//等待转换结束	
		ADC_ClearITFlag(ADC0, ADC_CHANNEL2);
		ADC_ClearITFlag(ADC0, ADC_CHANNEL3);
		TrimLowValue[i] = ADC_GetConvValue(ADC0, ADC_CHANNEL2);
		TrimHighValue[i] = ADC_GetConvValue(ADC0, ADC_CHANNEL3);
	 }
   	
      for(int i = 0; i < TRIMCOUNT; i++){
		printf("Adcval of TrimHighValue adbuf[%02d] is %04d\r\n",i,TrimHighValue[i]);
	}
	printf("\r\n");
   	for(int i = 0; i < TRIMCOUNT; i++){
		printf("Adcval of TrimLowValue adbuf[%02d] is %04d\r\n",i,TrimLowValue[i]);
	}
	printf("\r\n");
    delay_ms(4000);
    delay_ms(4000);
}

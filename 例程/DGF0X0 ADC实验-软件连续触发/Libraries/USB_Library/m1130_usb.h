/**
  ******************************************************************************
  * @file    m1130_USB.h
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    10/12/2013
  * @brief   This file contains all the functions prototypes for the UART 
  *          firmware library.
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2012 Alphascale</center></h2>
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __M1130_USB_H
#define __M1130_USB_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "m1130.h"
#include "m1130_rcc.h"

/** @addtogroup M1130_USB_HOST_Driver
  * @{
  */

/** @addtogroup MSC
  * @{
  */ 

/** @defgroup USB_Exported_Types
  * @{
  */ 



#ifdef __cplusplus
}
#endif

#endif /* __M1130_USB_H */
/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */ 

/******************* (C) COPYRIGHT 2012 Alphascale *****END OF FILE****/

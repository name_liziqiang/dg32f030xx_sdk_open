/*
 * USB OTG Core host controller driver.
 *
 * Copyright (c) 2012 Alpscale 
 *
 */

#include "musb_hcd.h"

/* MSC control transfers */
#define USB_MSC_BBB_RESET 	0xFF
#define USB_MSC_BBB_GET_MAX_LUN	0xFE

/* Endpoint configuration information */
static const struct musb_epinfo epinfo[3] = {
	{MUSB_BULK_EP, 1, 256}, /* EP1 - Bluk Out - 512 Bytes */
	{MUSB_BULK_EP, 0, 256}, /* EP1 - Bluk In  - 512 Bytes */
	{MUSB_INTR_EP, 0, 64}   /* EP2 - Interrupt IN - 64 Bytes */
};

/* --- Virtual Root Hub ---------------------------------------------------- */
#ifdef MUSB_NO_MULTIPOINT
static int rh_devnum;
static uint32_t port_status;

/* Device descriptor */
static const uint8_t root_hub_dev_des[] = {
	0x12,			/*  __uint8_t  bLength; */
	0x01,			/*  __uint8_t  bDescriptorType; Device */
	0x00,			/*  __uint16_t bcdUSB; v1.1 */
	0x02,
	0x09,			/*  __uint8_t  bDeviceClass; HUB_CLASSCODE */
	0x00,			/*  __uint8_t  bDeviceSubClass; */
	0x00,			/*  __uint8_t  bDeviceProtocol; */
	0x08,			/*  __uint8_t  bMaxPacketSize0; 8 Bytes */
	0x00,			/*  __uint16_t idVendor; */
	0x00,
	0x00,			/*  __uint16_t idProduct; */
	0x00,
	0x00,			/*  __uint16_t bcdDevice; */
	0x00,
	0x00,			/*  __uint8_t  iManufacturer; */
	0x01,			/*  __uint8_t  iProduct; */
	0x00,			/*  __uint8_t  iSerialNumber; */
	0x01			/*  __uint8_t  bNumConfigurations; */
};

/* Configuration descriptor */
static const uint8_t root_hub_config_des[] = {
	0x09,			/*  __uint8_t  bLength; */
	0x02,			/*  __uint8_t  bDescriptorType; Configuration */
	0x19,			/*  __uint16_t wTotalLength; */
	0x00,
	0x01,			/*  __uint8_t  bNumInterfaces; */
	0x01,			/*  __uint8_t  bConfigurationValue; */
	0x00,			/*  __uint8_t  iConfiguration; */
	0x40,			/*  __uint8_t  bmAttributes;
				   Bit 7: Bus-powered, 6: Self-powered, 5 Remote-wakwup, 4..0: resvd */
	0x00,			/*  __uint8_t  MaxPower; */

	/* interface */
	0x09,			/*  __uint8_t  if_bLength; */
	0x04,			/*  __uint8_t  if_bDescriptorType; Interface */
	0x00,			/*  __uint8_t  if_bInterfaceNumber; */
	0x00,			/*  __uint8_t  if_bAlternateSetting; */
	0x01,			/*  __uint8_t  if_bNumEndpoints; */
	0x09,			/*  __uint8_t  if_bInterfaceClass; HUB_CLASSCODE */
	0x00,			/*  __uint8_t  if_bInterfaceSubClass; */
	0x00,			/*  __uint8_t  if_bInterfaceProtocol; */
	0x00,			/*  __uint8_t  if_iInterface; */

	/* endpoint */
	0x07,			/*  __uint8_t  ep_bLength; */
	0x05,			/*  __uint8_t  ep_bDescriptorType; Endpoint */
	0x81,			/*  __uint8_t  ep_bEndpointAddress; IN Endpoint 1 */
	0x03,			/*  __uint8_t  ep_bmAttributes; Interrupt */
	0x00,			/*  __uint16_t ep_wMaxPacketSize; ((MAX_ROOT_PORTS + 1) / 8 */
	0x02,
	0xff			/*  __uint8_t  ep_bInterval; 255 ms */
};

static const unsigned char root_hub_str_index0[] = {
	0x04,			/*  __uint8_t  bLength; */
	0x03,			/*  __uint8_t  bDescriptorType; String-descriptor */
	0x09,			/*  __uint8_t  lang ID */
	0x04,			/*  __uint8_t  lang ID */
};

static const unsigned char root_hub_str_index1[] = {
	0x1c,			/*  __uint8_t  bLength; */
	0x03,			/*  __uint8_t  bDescriptorType; String-descriptor */
	'M',			/*  __uint8_t  Unicode */
	0,			/*  __uint8_t  Unicode */
	'U',			/*  __uint8_t  Unicode */
	0,			/*  __uint8_t  Unicode */
	'S',			/*  __uint8_t  Unicode */
	0,			/*  __uint8_t  Unicode */
	'B',			/*  __uint8_t  Unicode */
	0,			/*  __uint8_t  Unicode */
	' ',			/*  __uint8_t  Unicode */
	0,			/*  __uint8_t  Unicode */
	'R',			/*  __uint8_t  Unicode */
	0,			/*  __uint8_t  Unicode */
	'o',			/*  __uint8_t  Unicode */
	0,			/*  __uint8_t  Unicode */
	'o',			/*  __uint8_t  Unicode */
	0,			/*  __uint8_t  Unicode */
	't',			/*  __uint8_t  Unicode */
	0,			/*  __uint8_t  Unicode */
	' ',			/*  __uint8_t  Unicode */
	0,			/*  __uint8_t  Unicode */
	'H',			/*  __uint8_t  Unicode */
	0,			/*  __uint8_t  Unicode */
	'u',			/*  __uint8_t  Unicode */
	0,			/*  __uint8_t  Unicode */
	'b',			/*  __uint8_t  Unicode */
	0,			/*  __uint8_t  Unicode */
};
#endif

/*
 * This function writes the data toggle value.
 */
 #if 0
static void write_toggle(struct usb_device *dev, uint8_t ep, uint8_t dir_out)
{
	uint16_t toggle = usb_gettoggle(dev, ep, dir_out);
	uint8_t csr;
	uint8_t csr2;


	if (dir_out) {
			//musbr->txcsr1=MUSB_TXCSR_CLRDATATOG;
			//musbr->txcsr2=0;
	} else {
			//musbr->rxcsr1=MUSB_RXCSR_CLRDATATOG;
			//musbr->rxcsr2=0;
	}
}
#endif

/*
 * This function checks if RxStall has occured on the endpoint. If a RxStall
 * has occured, the RxStall is cleared and 1 is returned. If RxStall has
 * not occured, 0 is returned.
 */
static uint8_t check_stall(uint8_t ep, uint8_t dir_out)
{
	uint8_t csr;

	/* For endpoint 0 */
	if (!ep) {
		csr = musbr->txcsr1;
		if (csr & MUSB_CSR0_H_RXSTALL) {
			csr &= ~MUSB_CSR0_H_RXSTALL;
			musbr->txcsr1=csr;
			return 1;
		}
	} else { /* For non-ep0 */
		if (dir_out) { /* is it tx ep */
			csr = musbr->txcsr1;
			if (csr & MUSB_TXCSR_H_RXSTALL) {
				csr &= ~MUSB_TXCSR_H_RXSTALL;
				musbr->txcsr1=csr;
				return 1;
			}
		} else { /* is it rx ep */
			csr = musbr->rxcsr1;
			if (csr & MUSB_RXCSR_H_RXSTALL) {
				csr &= ~MUSB_RXCSR_H_RXSTALL;
				musbr->rxcsr1=csr;
				return 1;
			}
		}
	}
	return 0;
}

extern uint32_t g_forceReset;

/*
 * waits until ep0 is ready. Returns 0 if ep is ready, -1 for timeout
 * error and -2 for stall.
 */
static int wait_until_ep0_ready(struct usb_device *dev, uint32_t bit_mask)
{
	uint8_t csr;
	int result = 1;
	int timeout = CONFIG_MUSB_TIMEOUT;
	
	if(g_forceReset)
		return -1;

	while (result > 0) {
		csr = musbr->txcsr1;
		if (csr & MUSB_CSR0_H_ERROR) {
			csr &= ~MUSB_CSR0_H_ERROR;
			musbr->txcsr1=csr;
			dev->status = USB_ST_CRC_ERR;
			result = -1;
			break;
		}

		switch (bit_mask) {
		case MUSB_CSR0_TXPKTRDY:
			if (!(csr & MUSB_CSR0_TXPKTRDY)) {
				if (check_stall(MUSB_CONTROL_EP, 0)) {
					dev->status = USB_ST_STALLED;
					result = -2;
				} else
					result = 0;
			}
			break;

		case MUSB_CSR0_RXPKTRDY:
			if (check_stall(MUSB_CONTROL_EP, 0)) {
				dev->status = USB_ST_STALLED;
				result = -2;
			} else
				if (csr & MUSB_CSR0_RXPKTRDY)
					result = 0;
			break;

		case MUSB_CSR0_H_REQPKT:
			if (!(csr & MUSB_CSR0_H_REQPKT)) {
				if (check_stall(MUSB_CONTROL_EP, 0)) {
					dev->status = USB_ST_STALLED;
					result = -2;
				} else
					result = 0;
			}
			break;
		}

		/* Check the timeout */
		if (--timeout)
			{
			wait_ms(6);
			}
		else {
			dev->status = USB_ST_CRC_ERR;
			result = -1;
			g_forceReset=1;
			break;
		}
	}
	return result;
}

/*
 * waits until tx ep is ready. Returns 1 when ep is ready and 0 on error.
 */
static int wait_until_txep_ready(struct usb_device *dev, uint8_t ep)
{
	uint8_t csr;
	int timeout = CONFIG_MUSB_TIMEOUT;

	if(g_forceReset)
		return 0;

	do {
		if (check_stall(ep, 1)) {
			dev->status = USB_ST_STALLED;
			return 0;
		}

		csr = musbr->txcsr1;
		if (csr & MUSB_TXCSR_H_ERROR) {
			dev->status = USB_ST_CRC_ERR;
			return 0;
		}

		/* Check the timeout */
		if (--timeout)
			{
			wait_ms(6);
			}
		else {
			dev->status = USB_ST_CRC_ERR;
			g_forceReset=1;
			return 0;
		}

	} while (csr & MUSB_TXCSR_TXPKTRDY);
	return 1;
}

/*
 * waits until rx ep is ready. Returns 1 when ep is ready and 0 on error.
 */
static uint8_t wait_until_rxep_ready(struct usb_device *dev, uint8_t ep)
{
	uint8_t csr;
	int timeout = CONFIG_MUSB_TIMEOUT;

	if(g_forceReset)
		return 0;

	do {
		if (check_stall(ep, 0)) {
			dev->status = USB_ST_STALLED;
			return 0;
		}

		csr = musbr->rxcsr1;
		if (csr & MUSB_RXCSR_H_ERROR) {
			dev->status = USB_ST_CRC_ERR;
			return 0;
		}

		/* Check the timeout */
		if (--timeout)
			{
				wait_ms(6);
			}
		else {
			dev->status = USB_ST_CRC_ERR;
			g_forceReset=1;
			return 0;
		}

	} while (!(csr & MUSB_RXCSR_RXPKTRDY));

	return 1;
}

/*
 * This function performs the setup phase of the control transfer
 */
static int ctrlreq_setup_phase(struct usb_device *dev, struct devrequest *setup)
{
	int result;
	uint8_t csr;

	/* write the control request to ep0 fifo */
	write_fifo(MUSB_CONTROL_EP, sizeof(struct devrequest), (void *)setup);

	/* enable transfer of setup packet */
	csr = musbr->txcsr1;
	csr |= (MUSB_CSR0_TXPKTRDY|MUSB_CSR0_H_SETUPPKT);
	musbr->txcsr1=csr;
	/* wait until the setup packet is transmitted */
	result = wait_until_ep0_ready(dev, MUSB_CSR0_TXPKTRDY);
	dev->act_len = 0;

	csr = musbr->txcsr1;
	csr &= ~MUSB_CSR0_H_SETUPPKT;
	musbr->txcsr1=csr;
	return result;
}

/*
 * This function handles the control transfer in data phase
 */
static int ctrlreq_in_data_phase(struct usb_device *dev, uint32_t len, void *buffer)
{
	uint8_t csr;
	uint32_t rxlen = 0;
	uint32_t nextlen = 0;
	uint8_t  maxpktsize = (1 << dev->maxpacketsize) * 8;
	uint8_t  *rxbuff = (uint8_t *)buffer;
	uint16_t  rxedlength;
	int result;

	while (rxlen < len) {
		/* Determine the next read length */
		nextlen = ((len-rxlen) > maxpktsize) ? maxpktsize : (len-rxlen);

		/* Set the ReqPkt bit */
		csr = musbr->txcsr1;
		csr |= MUSB_CSR0_H_REQPKT;
		musbr->txcsr1=csr;
		result = wait_until_ep0_ready(dev, MUSB_CSR0_RXPKTRDY);
		//print("ctrlreq_in_data_phase\r\n");
		if (result < 0)
			return result;

		/* Actual number of bytes received by usb */
		rxedlength = (musbr->rxcount2<<8)+musbr->rxcount1;

		/* Read the data from the RxFIFO */
		read_fifo(MUSB_CONTROL_EP, rxedlength, &rxbuff[rxlen]);

		/* Clear the RxPktRdy Bit */
		csr = musbr->txcsr1;
		csr &= ~MUSB_CSR0_RXPKTRDY;
		musbr->txcsr1=csr;

		/* short packet? */
		if (rxedlength != nextlen) {
			dev->act_len += rxedlength;
			break;
		}
		rxlen += nextlen;
		dev->act_len = rxlen;
	}
	return 0;
}

/*
 * This function handles the control transfer out data phase
 */
static int ctrlreq_out_data_phase(struct usb_device *dev, uint32_t len, void *buffer)
{
	uint8_t csr;
	uint32_t txlen = 0;
	uint32_t nextlen = 0;
	uint8_t  maxpktsize = (1 << dev->maxpacketsize) * 8;
	uint8_t  *txbuff = (uint8_t *)buffer;
	int result = 0;

	while (txlen < len) {
		/* Determine the next write length */
		nextlen = ((len-txlen) > maxpktsize) ? maxpktsize : (len-txlen);

		/* Load the data to send in FIFO */
		write_fifo(MUSB_CONTROL_EP, txlen, &txbuff[txlen]);

		/* Set TXPKTRDY bit */
		csr = musbr->txcsr1;
		musbr->txcsr1=csr | MUSB_CSR0_TXPKTRDY;
		result = wait_until_ep0_ready(dev, MUSB_CSR0_TXPKTRDY);
		if (result < 0)
			break;

		txlen += nextlen;
		dev->act_len = txlen;
	}
	return result;
}

/*
 * This function handles the control transfer out status phase
 */
static int ctrlreq_out_status_phase(struct usb_device *dev)
{
	uint8_t csr;
	int result;

	/* Set the StatusPkt bit */
	csr = musbr->txcsr1;
	csr |= (MUSB_CSR0_TXPKTRDY | MUSB_CSR0_H_STATUSPKT);
	musbr->txcsr1=csr;

	/* Wait until TXPKTRDY bit is cleared */
	result = wait_until_ep0_ready(dev, MUSB_CSR0_TXPKTRDY);

	csr = musbr->txcsr1;
	csr &= ~MUSB_CSR0_H_STATUSPKT;
	musbr->txcsr1=csr;
	return result;
}

/*
 * This function handles the control transfer in status phase
 */
static int ctrlreq_in_status_phase(struct usb_device *dev)
{
	uint8_t csr;
	int result;

	/* Set the StatusPkt bit and ReqPkt bit */
	csr = musbr->txcsr1;
	csr |= MUSB_CSR0_H_REQPKT | MUSB_CSR0_H_STATUSPKT;
	musbr->txcsr1=csr;
	result = wait_until_ep0_ready(dev, MUSB_CSR0_H_REQPKT);

	/* clear StatusPkt bit and RxPktRdy bit */
	csr = musbr->txcsr1;
	csr &= ~(MUSB_CSR0_RXPKTRDY | MUSB_CSR0_H_STATUSPKT);
	musbr->txcsr1=csr;
	return result;
}


unsigned int min_ui(unsigned int __x, unsigned int __y)
{
	return __x < __y ? __x : __y;
}

int min_i(int __x, int __y)
{
	return __x < __y ? __x : __y;
}

#ifdef MUSB_NO_MULTIPOINT

static void musb_port_reset(int do_reset)
{
	uint8_t power = readb(&musbr->power);

	if (do_reset) {
		power &= 0xf0;
		writeb(power | MUSB_POWER_RESET, &musbr->power);
		port_status |= USB_PORT_STAT_RESET;
		port_status &= ~USB_PORT_STAT_ENABLE;
		wait_ms(600);	//udelay(30000);
	} else {
		writeb(power & ~MUSB_POWER_RESET, &musbr->power);

		power = readb(&musbr->power);
		//if (power & MUSB_POWER_HSMODE)
		//	port_status |= USB_PORT_STAT_HIGH_SPEED;

		port_status &= ~(USB_PORT_STAT_RESET | (USB_PORT_STAT_C_CONNECTION << 16));
		port_status |= USB_PORT_STAT_ENABLE
			| (USB_PORT_STAT_C_RESET << 16)
			| (USB_PORT_STAT_C_ENABLE << 16);
	}
}

/*
 * root hub control
 */
static int musb_submit_rh_msg(struct usb_device *dev, unsigned long pipe,
			      void *buffer, int transfer_len,
			      struct devrequest *cmd)
{
	int leni = transfer_len;
	int len = 0;
	int stat = 0;
	uint32_t datab[4];
	const uint8_t *data_buf = (uint8_t *) datab;
	uint16_t bmRType_bReq;
	uint16_t wValue;
	//uint16_t wIndex;
	uint16_t wLength;
	uint16_t int_usb;
//	uint32_t ssz;

	if ((pipe & PIPE_INTERRUPT) == PIPE_INTERRUPT) {
		return 0;
	}

	bmRType_bReq = cmd->requesttype | (cmd->request << 8);
	wValue = swap_16(cmd->value);
	//wIndex = swap_16(cmd->index);
	wLength = swap_16(cmd->length);

	switch (bmRType_bReq) {
	case RH_GET_STATUS:

		*(uint16_t *) data_buf = swap_16(1);
		len = 2;
		break;

	case RH_GET_STATUS | RH_INTERFACE:

		*(uint16_t *) data_buf = swap_16(0);
		len = 2;
		break;

	case RH_GET_STATUS | RH_ENDPOINT:

		*(uint16_t *) data_buf = swap_16(0);
		len = 2;
		break;

	case RH_GET_STATUS | RH_CLASS:

		*(uint32_t *) data_buf = swap_32(0);
		len = 4;
		break;

	case RH_GET_STATUS | RH_OTHER | RH_CLASS:

		int_usb = musbr->intrusb;
		if (int_usb & MUSB_INTR_CONNECT) {
			port_status |= USB_PORT_STAT_CONNECTION
				| (USB_PORT_STAT_C_CONNECTION << 16);
			port_status |= USB_PORT_STAT_HIGH_SPEED
				| USB_PORT_STAT_ENABLE;
		}

		if (port_status & USB_PORT_STAT_RESET)
			musb_port_reset(0);

		*(uint32_t *) data_buf = swap_32(port_status);
		len = 4;
		break;

	case RH_CLEAR_FEATURE | RH_ENDPOINT:

		switch (wValue) {
		case RH_ENDPOINT_STALL:
			len = 0;
			break;
		}
		port_status &= ~(1 << wValue);
		break;

	case RH_CLEAR_FEATURE | RH_CLASS:

		switch (wValue) {
		case RH_C_HUB_LOCAL_POWER:
			len = 0;
			break;

		case RH_C_HUB_OVER_CURRENT:
			len = 0;
			break;
		}
		port_status &= ~(1 << wValue);
		break;

	case RH_CLEAR_FEATURE | RH_OTHER | RH_CLASS:

		switch (wValue) {
		case RH_PORT_ENABLE:
			len = 0;
			break;

		case RH_PORT_SUSPEND:
			len = 0;
			break;

		case RH_PORT_POWER:
			len = 0;
			break;

		case RH_C_PORT_CONNECTION:
			len = 0;
			break;

		case RH_C_PORT_ENABLE:
			len = 0;
			break;

		case RH_C_PORT_SUSPEND:
			len = 0;
			break;

		case RH_C_PORT_OVER_CURRENT:
			len = 0;
			break;

		case RH_C_PORT_RESET:
			len = 0;
			break;

		default:
			stat = USB_ST_STALLED;
		}

		port_status &= ~(1 << wValue);
		break;

	case RH_SET_FEATURE | RH_OTHER | RH_CLASS:

		switch (wValue) {
		case RH_PORT_SUSPEND:
			len = 0;
			break;

		case RH_PORT_RESET:
			musb_port_reset(1);
			len = 0;
			break;

		case RH_PORT_POWER:
			len = 0;
			break;

		case RH_PORT_ENABLE:
			len = 0;
			break;

		default:
			stat = USB_ST_STALLED;
		}

		port_status |= 1 << wValue;
		break;

	case RH_SET_ADDRESS:

		rh_devnum = wValue;
		len = 0;
		break;

	case RH_GET_DESCRIPTOR:

		switch (wValue) {
		case (USB_DT_DEVICE << 8):	/* device descriptor */
			len = min_ui(leni, min_ui(sizeof(root_hub_dev_des),wLength));
			data_buf = root_hub_dev_des;
			break;

		case (USB_DT_CONFIG << 8):	/* configuration descriptor */
			len = min_ui(leni, min_ui(sizeof(root_hub_config_des),wLength));
			data_buf = root_hub_config_des;
			break;

		case ((USB_DT_STRING << 8) | 0x00):	/* string 0 descriptors */
			len = min_ui(leni, min_ui(sizeof(root_hub_str_index0),wLength));
			data_buf = root_hub_str_index0;
			break;

		case ((USB_DT_STRING << 8) | 0x01):	/* string 1 descriptors */
			len = min_ui(leni, min_ui(sizeof(root_hub_str_index1),wLength));
			data_buf = root_hub_str_index1;
			break;

		default:
			stat = USB_ST_STALLED;
		}

		break;

	case RH_GET_DESCRIPTOR | RH_CLASS: {
		uint8_t *_data_buf = (uint8_t *) datab;

		_data_buf[0] = 0x09;	/* min length; */
		_data_buf[1] = 0x29;
		_data_buf[2] = 0x1;	/* 1 port */
		_data_buf[3] = 0x01;	/* per-port power switching */
		_data_buf[3] |= 0x10;	/* no overcurrent reporting */

		/* Corresponds to data_buf[4-7] */
		_data_buf[4] = 0;
		_data_buf[5] = 5;
		_data_buf[6] = 0;
		_data_buf[7] = 0x02;
		_data_buf[8] = 0xff;

		len = min_ui(leni, min_ui(data_buf[0], wLength));
		break;
	}

	case RH_GET_CONFIGURATION:

		*(uint8_t *) data_buf = 0x01;
		len = 1;
		break;

	case RH_SET_CONFIGURATION:

		len = 0;
		break;

	default:

		stat = USB_ST_STALLED;
	}

	len = min_i(len, leni);
	if (buffer != data_buf){
		//MemCopy(buffer, len, data_buf, &ssz);
		memcpy(buffer,data_buf,len);
	}
	dev->act_len = len;
	dev->status = stat;

	return stat;
}

static void musb_rh_init(void)
{
	rh_devnum = 0;
	port_status = 0;
}

#else

static void musb_rh_init(void) {}

#endif

/*
 * do a control transfer
 */
int submit_control_msg(struct usb_device *dev, unsigned long pipe, void *buffer,
			int len, struct devrequest *setup)
{
	int devnum = usb_pipedevice(pipe);
//	uint8_t  devspeed;

	if(g_forceReset)
		return -1;

#ifdef MUSB_NO_MULTIPOINT
	/* Control message is for the HUB? */
	if (devnum == rh_devnum) {
		int stat = musb_submit_rh_msg(dev, pipe, buffer, len, setup);
		if (stat)
			return stat;
	}
#endif

	/* select control endpoint */
	writeb(MUSB_CONTROL_EP, &musbr->index);
	//csr = musbr->txcsr;

#ifndef MUSB_NO_MULTIPOINT
	/* target addr and (for multipoint) hub addr/port */
	writeb(devnum, &musbr->tar[MUSB_CONTROL_EP].txfuncaddr);
	writeb(devnum, &musbr->tar[MUSB_CONTROL_EP].rxfuncaddr);
#endif

	/* configure the hub address and the port number as required */
		//writeb(musb_cfg.musb_speed << 6, &musbr->txtype);
#ifndef MUSB_NO_MULTIPOINT
		writeb(0, &musbr->tar[MUSB_CONTROL_EP].txhubaddr);
		writeb(0, &musbr->tar[MUSB_CONTROL_EP].txhubport);
		writeb(0, &musbr->tar[MUSB_CONTROL_EP].rxhubaddr);
		writeb(0, &musbr->tar[MUSB_CONTROL_EP].rxhubport);
#endif
	
	/* Control transfer setup phase */
	if (ctrlreq_setup_phase(dev, setup) < 0)
		return 0;

	switch (setup->request) {
	case USB_REQ_GET_DESCRIPTOR:
	case USB_REQ_GET_CONFIGURATION:
	case USB_REQ_GET_INTERFACE:
	case USB_REQ_GET_STATUS:
	case USB_MSC_BBB_GET_MAX_LUN:
		/* control transfer in-data-phase */
		if (ctrlreq_in_data_phase(dev, len, buffer) < 0)
			return 0;
		/* control transfer out-status-phase */
		if (ctrlreq_out_status_phase(dev) < 0)
			return 0;
		break;

	case USB_REQ_SET_ADDRESS:
	case USB_REQ_SET_CONFIGURATION:
	case USB_REQ_SET_FEATURE:
	case USB_REQ_SET_INTERFACE:
	case USB_REQ_CLEAR_FEATURE:
	case USB_MSC_BBB_RESET:
		/* control transfer in status phase */
		if (ctrlreq_in_status_phase(dev) < 0)
			return 0;
		break;

	case USB_REQ_SET_DESCRIPTOR:
		/* control transfer out data phase */
		if (ctrlreq_out_data_phase(dev, len, buffer) < 0)
			return 0;
		/* control transfer in status phase */
		if (ctrlreq_in_status_phase(dev) < 0)
			return 0;
		break;

	default:
		/* unhandled control transfer */
		return -1;
	}

	dev->status = 0;
	dev->act_len = len;

#ifdef MUSB_NO_MULTIPOINT
	/* Set device address to USB_FADDR register */
	if (setup->request == USB_REQ_SET_ADDRESS)
		writeb(dev->devnum, &musbr->faddr);
#endif

	return len;
}

/*
 * do a bulk transfer
 */
int submit_bulk_msg(struct usb_device *dev, unsigned long pipe,
					void *buffer, int len)
{
	int dir_out = usb_pipeout(pipe);
	int ep = usb_pipeendpoint(pipe);
#ifndef MUSB_NO_MULTIPOINT
	int devnum = usb_pipedevice(pipe);
#endif
	uint8_t  type;
	uint8_t csr;
	uint32_t txlen = 0;
	uint32_t nextlen = 0;
	//uint8_t  devspeed;
	
	if(g_forceReset)
		return -1;
	
	/* select bulk endpoint */
	writeb(MUSB_BULK_EP, &musbr->index);

#ifndef MUSB_NO_MULTIPOINT
	/* write the address of the device */
	if (dir_out)
		writeb(devnum, &musbr->tar[MUSB_BULK_EP].txfuncaddr);
	else
		writeb(devnum, &musbr->tar[MUSB_BULK_EP].rxfuncaddr);
#endif

	/* configure the hub address and the port number as required */
#ifndef MUSB_NO_MULTIPOINT
		if (dir_out) {
			writeb(0, &musbr->tar[MUSB_BULK_EP].txhubaddr);
			writeb(0, &musbr->tar[MUSB_BULK_EP].txhubport);
		} else {
			writeb(0, &musbr->tar[MUSB_BULK_EP].rxhubaddr);
			writeb(0, &musbr->tar[MUSB_BULK_EP].rxhubport);
		}
#endif
		//devspeed = musb_cfg.musb_speed;

	/* Write the saved toggle bit value */
	//write_toggle(dev, ep, dir_out);

	if (dir_out) { /* bulk-out transfer */
		/* Program the TxType register */
		type = ((MUSB_TYPE_PROTO_BULK << MUSB_TYPE_PROTO_SHIFT) |(ep & MUSB_TYPE_REMOTE_END));
		writeb(type, &musbr->txtype);

		/* Write maximum packet size to the TxMaxp register */
		writeb(dev->epmaxpacketout[ep], &musbr->txmaxp);
		while (txlen < len) {
			nextlen = ((len-txlen) < dev->epmaxpacketout[ep]) ?
					(len-txlen) : dev->epmaxpacketout[ep];

#ifdef CONFIG_USB_BLACKFIN
			/* Set the transfer data size */
			musbr->txcount=nextlen;
#endif

			/* Write the data to the FIFO */
			write_fifo(MUSB_BULK_EP, nextlen,
					(void *)(((uint8_t *)buffer) + txlen));

			/* Set the TxPktRdy bit */
			csr = musbr->txcsr1;
			musbr->txcsr1=csr | MUSB_TXCSR_TXPKTRDY;

			/* Wait until the TxPktRdy bit is cleared */
			if (1 != wait_until_txep_ready(dev, MUSB_BULK_EP)) {
				//musbr->txcsr;
				//usb_settoggle(dev, ep, dir_out, (csr >> MUSB_TXCSR_H_DATATOGGLE_SHIFT) & 1);
				dev->act_len = txlen;
				return -1;
			}
			txlen += nextlen;
		}

		/* Keep a copy of the data toggle bit */
		//csr = musbr->txcsr1;
		//usb_settoggle(dev, ep, dir_out, (csr >> MUSB_TXCSR_H_DATATOGGLE_SHIFT) & 1);
	} else { /* bulk-in transfer */
		/* Write the saved toggle bit value */
		//write_toggle(dev, ep, dir_out);

		/* Program the RxType register */
		type = ((MUSB_TYPE_PROTO_BULK << MUSB_TYPE_PROTO_SHIFT) |(ep & MUSB_TYPE_REMOTE_END));
		writeb(type, &musbr->rxtype);

		/* Write the maximum packet size to the RxMaxp register */
		writeb(dev->epmaxpacketin[ep], &musbr->rxmaxp);
		while (txlen < len) {
			nextlen = ((len-txlen) < dev->epmaxpacketin[ep]) ?
					(len-txlen) : dev->epmaxpacketin[ep];

			/* Set the ReqPkt bit */
			csr = musbr->rxcsr1;
			musbr->rxcsr1=csr | MUSB_RXCSR_H_REQPKT;

			/* Wait until the RxPktRdy bit is set */
			if (!wait_until_rxep_ready(dev, MUSB_BULK_EP)) {
				csr = musbr->rxcsr1;
				//usb_settoggle(dev, ep, dir_out,(csr >> MUSB_S_RXCSR_H_DATATOGGLE) & 1);
				csr &= ~MUSB_RXCSR_RXPKTRDY;
				musbr->rxcsr1=csr;
				dev->act_len = txlen;
				return -1;
			}

			
			/* Read the data from the FIFO */
			read_fifo(MUSB_BULK_EP, nextlen,
					(void *)(((uint8_t *)buffer) + txlen));

			/* Clear the RxPktRdy bit */
			csr =  musbr->rxcsr1;
			csr &= ~MUSB_RXCSR_RXPKTRDY;
			musbr->rxcsr1=csr;
			txlen += nextlen;
		}

		/* Keep a copy of the data toggle bit */
		//csr = musbr->rxcsr1;
		//usb_settoggle(dev, ep, dir_out,(csr >> MUSB_S_RXCSR_H_DATATOGGLE) & 1);
	}

	/* bulk transfer is complete */
	dev->status = 0;
	dev->act_len = len;
	return 0;
}

/*
 * This function initializes the usb controller module.
 */
int usb_lowlevel_init(void)
{
	uint8_t  power;
	uint32_t timeout;

	musbr = musb_cfg.regs;
	musb_rh_init();

	if (musb_platform_init() == -1)
		return -1;

	/*
	 * Wait until musb is enabled in host mode with a timeout. There
	 * should be a usb device connected.
	 */
	timeout = musb_cfg.timeout;
	while (timeout--)
		if (readb(&musbr->devctl) & MUSB_DEVCTL_HM)
			{
				break;
			}
	/* if musb core is not in host mode, then return */
	if (!timeout)
		return -1;

	/* start usb bus reset */
	power = readb(&musbr->power);
	writeb(power | MUSB_POWER_RESET, &musbr->power);

	/* After initiating a usb reset, wait for about 20ms to 30ms */
	wait_ms(1800);

	/* stop usb bus reset */
	power = readb(&musbr->power);
	power &= ~MUSB_POWER_RESET;
	writeb(power, &musbr->power);

	/* Determine if the connected device is a high/full/low speed device */
	musb_cfg.musb_speed = ((readb(&musbr->devctl) & MUSB_DEVCTL_FSDEV) ? MUSB_TYPE_SPEED_FULL : MUSB_TYPE_SPEED_LOW);

	/* Configure all the endpoint FIFO's and start usb controller */
	musb_configure_ep(&epinfo[0],sizeof(epinfo) / sizeof(struct musb_epinfo));
	musb_start();
	wait_ms(1800);


	//����NAK limit
	//musbr->index = 0;
	//musbr->txinterval = 255;
	
	return 0;
}

/*
 * This function stops the operation of the davinci usb module.
 */
int usb_lowlevel_stop(void)
{
	/* Reset the USB module */
	musb_platform_deinit();
	writeb(0, &musbr->devctl);
	return 0;
}

/*
 * This function supports usb interrupt transfers. Currently, usb interrupt
 * transfers are not supported.
 */
int submit_int_msg(struct usb_device *dev, unsigned long pipe,
				void *buffer, int len, int interval)
{
	int dir_out = usb_pipeout(pipe);
	int ep = usb_pipeendpoint(pipe);
#ifndef MUSB_NO_MULTIPOINT
	int devnum = usb_pipedevice(pipe);
#endif
	uint8_t  type;
	uint8_t csr;
	uint32_t txlen = 0;
	uint32_t nextlen = 0;

	/* select interrupt endpoint */
	writeb(MUSB_INTR_EP, &musbr->index);

#ifndef MUSB_NO_MULTIPOINT
	/* write the address of the device */
	if (dir_out)
		writeb(devnum, &musbr->tar[MUSB_INTR_EP].txfuncaddr);
	else
		writeb(devnum, &musbr->tar[MUSB_INTR_EP].rxfuncaddr);
#endif

#ifndef MUSB_NO_MULTIPOINT
		if (dir_out) {
			writeb(0, &musbr->tar[MUSB_INTR_EP].txhubaddr);
			writeb(0, &musbr->tar[MUSB_INTR_EP].txhubport);
		} else {
			writeb(0, &musbr->tar[MUSB_INTR_EP].rxhubaddr);
			writeb(0, &musbr->tar[MUSB_INTR_EP].rxhubport);
		}
#endif

	/* Write the saved toggle bit value */
	//write_toggle(dev, ep, dir_out);

	if (!dir_out) { /* intrrupt-in transfer */
		/* Write the saved toggle bit value */
		//write_toggle(dev, ep, dir_out);
		writeb(interval, &musbr->rxinterval);

		/* Program the RxType register */
		type = ((MUSB_TYPE_PROTO_INTR << MUSB_TYPE_PROTO_SHIFT) | (ep & MUSB_TYPE_REMOTE_END));
		writeb(type, &musbr->rxtype);

		/* Write the maximum packet size to the RxMaxp register */
		writeb(dev->epmaxpacketin[ep], &musbr->rxmaxp);

		while (txlen < len) {
			nextlen = ((len-txlen) < dev->epmaxpacketin[ep]) ?
					(len-txlen) : dev->epmaxpacketin[ep];

			/* Set the ReqPkt bit */
			csr = musbr->rxcsr1;
			musbr->rxcsr1=csr | MUSB_RXCSR_H_REQPKT;

			/* Wait until the RxPktRdy bit is set */
			if (!wait_until_rxep_ready(dev, MUSB_INTR_EP)) {
				csr = musbr->rxcsr1;
				//usb_settoggle(dev, ep, dir_out,(csr >> MUSB_S_RXCSR_H_DATATOGGLE) & 1);
				csr &= ~MUSB_RXCSR_RXPKTRDY;
				musbr->rxcsr1=csr;
				dev->act_len = txlen;
				return -1;
			}

			/* Read the data from the FIFO */
			read_fifo(MUSB_INTR_EP, nextlen,
					(void *)(((uint8_t *)buffer) + txlen));

			/* Clear the RxPktRdy bit */
			csr =  musbr->rxcsr1;
			csr &= ~MUSB_RXCSR_RXPKTRDY;
			musbr->rxcsr1=csr;
			txlen += nextlen;
		}

		/* Keep a copy of the data toggle bit */
		csr = musbr->rxcsr1;
		//usb_settoggle(dev, ep, dir_out,(csr >> MUSB_S_RXCSR_H_DATATOGGLE) & 1);
	}

	/* interrupt transfer is complete */
	dev->irq_status = 0;
	dev->irq_act_len = len;
	dev->irq_handle(dev);
	dev->status = 0;
	dev->act_len = len;
	return 0;
}


#ifdef CONFIG_SYS_USB_EVENT_POLL
/*
 * This function polls for USB keyboard data.
 */
void usb_event_poll()
{
	struct stdio_dev *dev;
	struct usb_device *usb_kbd_dev;
	struct usb_interface *iface;
	struct usb_endpoint_descriptor *ep;
	int pipe;
	int maxp;

	/* Get the pointer to USB Keyboard device pointer */
	dev = stdio_get_by_name("usbkbd");
	usb_kbd_dev = (struct usb_device *)dev->priv;
	iface = &usb_kbd_dev->config.if_desc[0];
	ep = &iface->ep_desc[0];
	pipe = usb_rcvintpipe(usb_kbd_dev, ep->bEndpointAddress);

	/* Submit a interrupt transfer request */
	maxp = usb_maxpacket(usb_kbd_dev, pipe);
	usb_submit_int_msg(usb_kbd_dev, pipe, &new[0],
			maxp > 8 ? 8 : maxp, ep->bInterval);
}
#endif /* CONFIG_SYS_USB_EVENT_POLL */

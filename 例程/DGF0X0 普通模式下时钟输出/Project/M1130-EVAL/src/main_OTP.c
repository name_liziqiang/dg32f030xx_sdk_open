/*
	OTP test
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "m1130_uart.h" 
#include "m1130_rcc.h"	 
#include "misc.h"
#include "m1130_gpio.h" 
#include "m1130.h"
#include "m1130_utility_usart.h"
#include "m1130_otp.h"

int main(void)
{
	uint32_t snValue;
	
  	UART_Configuration(UART1, 115200, UART1_GP2);
	OTP_Reset();
	
	snValue = OTP_ReadSN();

	printf("SN=0x%x\r\n",snValue);
	
	while(1);
}




/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "m1130_uart.h"
#include "m1130_gpio.h"
#include "m1130_rcc.h"
#include "misc.h"
#include "m1130_dma.h"
#include "m1130_wdg.h"
#include "m1130_utility_dma.h"
#include "m1130_utility_usart.h"


#define DMA_CHANNEL_X     DMA_CHANNEL_3
#define DATA_Type         uint8_t
#define WORDS_SIZE        511


/**
  * @brief  Main program.asd
  * @param  None
  * @retval None
  */
#define SRC_ADDR   0x20001048
#define DST_ADDR   0x20006048 
DATA_Type sData[WORDS_SIZE]   __attribute__((at(SRC_ADDR)));
DATA_Type dData[WORDS_SIZE]   __attribute__((at(DST_ADDR)));

int main()
{
  int i;
  uint32_t comm;
	UART_Configuration(UART1, 115200, UART1_GP2);
  printf("\r\n-----------------|- m1130 Standard Peripheral Library Driver -|-----------------\r\n");
  DmaInit(DMA_CHANNEL_X);
  for(i=0; i<WORDS_SIZE; i++)
  {
    sData[i] = i;
  }
  //memset(sata, 0xAA, sizeof(sData));
  memset(dData, 0x00, sizeof(dData));
  printf("psdata=%p,pdData=%p\r\n",sData,dData);
  DMA_MemCopy(dData, sData, sizeof(sData));
  commonDelay(400000);
  for(i=0; i<WORDS_SIZE; i++)
  {
    if (sData[i] != dData[i])
    {
      printf("%d dma copy failed\r\n",i);
      goto err;
    }
  }
  printf("dma copy ok\r\n", i);

  comm = 0x12345678;
  DMA_MemSet(dData, comm, sizeof(DATA_Type), WORDS_SIZE);
  for(i=0; i<WORDS_SIZE; i++)
  {
    if (dData[i] != (comm & (0xffffffff >> ((4-sizeof(DATA_Type)) * 8))))
    {
      printf("dma memset failed\r\n");
      goto err;
    }
  }
  printf("dma memset ok\r\n");

  err:
  while(1)
  {
  //commonDelay(4000000);
  }
}


/**
  ******************************************************************************
  * @file    Project/M1800-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "main.h"
#include "m1130_uart.h" 
#include "m1130_gpio.h" 
#include "m1130_rcc.h"	 
#include "misc.h"
#include "m1130_qspi.h"	 
#include "m1130_eval_qspi.h"
#include "led_common.h"
#include "m1130_otp.h"


/* Private function prototypes -----------------------------------------------*/
#ifdef __GNUC__
  /* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
     set to 'Yes') calls __io_putchar() */
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
/* Private functions ---------------------------------------------------------*/

void serial_init(void);

void delay(unsigned long time);
void delay_ms(uint16_t ms)
{
	uint16_t i;
	for(; ms>0; ms--)
		for(i=0; i<4000; i++);
}

//#define FLASH_4BYTES_ADDR

int switchQuad(uint32_t ctrl2Val)
{	
	GPIO_SetPinMux(GPIO1, GPIO_Pin_0, GPIO_FUNCTION_1);
	GPIO_SetPinMux(GPIO1, GPIO_Pin_3, GPIO_FUNCTION_1);
	//QSPI0->CMD = QUAD_SPI_CMD_FAST_READ_OCTAL_QUAD_IO;	//0xEB
	QSPI_ModeSet(QSPI0, QSPI_STD); //XIP自动切换到4线

#ifdef FLASH_4BYTES_ADDR
	QSPI0->XIP_READ_CMD = 0xEC;
	QSPI0->ADD_BYTE_LEN = 1;
#else
	QSPI0->XIP_READ_CMD = 0xEB;
	QSPI0->ADD_BYTE_LEN = 0;
#endif

	QSPI0->DUMMY = 2;					//
	QSPI0->START_ADDR = 0x8000;			//32KB
	QSPI0->CTRL2 = ctrl2Val;	
	//QSPI0->BUSY_DLY = 0;
	//bit13 DMA_ENABLE=0
	QSPI_DMACmd(QSPI0, DISABLE);
	//bit26 WRITE
	QSPI0->CTRL0_CLR = 1<<26;
	//bit28 HALF_DUPLEX=1
	QSPI0->CTRL0_SET = 1<<28;
	//bit25 MEMAP=1
	QSPI0->CTRL0_SET = 0x0a000000;

	return 0;
}

void startXIP(void)
{
/*
	(1)ISSI		M7-0=0xA0
	(2)GD		M7-0=0xA0
	(3)ESMT		M7-0=0xA5
	(4)WB		M7-0=0x20
	(5)MXIC		M7-0=0xA5
*/
	//RCC->XMAP_ADDR = 1; //flash 32KB = XIP 0  等价于  flash 40KB => XIP 8KB
	switchQuad(0x0000);
//	printf("start XIP\r\n");
	RCC->ADDRESS_REMAP = 1;	//REBOOT to XIP		
}

int ASM1130_UsbHostIrqInit(void);
int ASM1130_UsbHostprocess(void);

int checkCapDly(void)
{
	uint32_t spAddr,resetAddr,i;

	QSPI0->BUSY_DLY &= 0xFFFFFF00;		//make it faster
	for(i=0;i<16;i++){
		QSPI0->BUSY_DLY &= 0xFFFFF0FF;	//clean [11:8]
		QSPI0->BUSY_DLY |= (i<<8);
		
		qspi_flash_read(QSPI0, (uint8_t *)0x20008000, 0x8000, 16);	//read 16 Bytes
		
		spAddr = inl(0x20008000)&0xFFFF0000;			//spAddr=0x2000XXXX
		resetAddr = inl(0x20008004);					//resetAddr=0x000000B1
		if((spAddr==0x20000000)&&(resetAddr==0x000000B1)){
			return 0;	//success
		}
	}

	return -1;		//fail
}

/*
  加载OTP中的校准值
*/
void SystemADJInit (void)
{    
       uint32_t ADJ,temp;
       OTP_Reset();
       //加载内部12MIRC校准值
       ADJ=OTP_ReadADJ_12M();
       temp=RCC->OSC12_CTRL;
       temp &=0xffffff00;
       temp |=ADJ;
       RCC->OSC12_CTRL=temp;

       //加载内部10KIRC校准值
       ADJ=OTP_ReadADJ_10K();
       temp=RCC->OSC10_CTRL;
       temp &=0xfffffff8;
       temp |=ADJ;
       RCC->OSC10_CTRL=temp;

       //加载内部LDO校准值
       ADJ=OTP_ReadADJ_LDO();
       temp=RCC->LDO_CTRL;
       temp &=0xfffffff8;
       temp |=ADJ;
       RCC->LDO_CTRL=temp;

       //加载内部RTC负载电容校准值
       ADJ=OTP_ReadADJ_RTC_CAP();
       temp=RCC->RTC_ANA_CTRL;
       temp &=0xffffe0ff;
       temp |=(ADJ<<8);
       RCC->RTC_ANA_CTRL =temp;
    
}
extern uint32_t g_usbconnect;
int main()
{
   SystemADJInit();//开机前读取校准值
//	serial_init();   
//	printf("Start Loader\r\n");
	quad_spi_flash_init(QSPI0, 2, 2, 0);

	if(checkCapDly()<0){
//		printf("cap dly error\r\n");
		while(1);
	}
	
	qspi_flash_enableQE(QSPI0, 0x02); //除了ESMT , 都需要这个  bit9 = QE
	//read 8KB from Flash to RAM1
	qspi_flash_read(QSPI0, (uint8_t *)0x20008000, 0x8000, 0x2000);

	//XIP
	startXIP();
	while(1);
}

//默认UART1:GP2_2&GP2_3
void serial_init(void)
{
	UART_InitTypeDef UART_InitStructure;

	RCC_ResetAHBCLK(1<<AHBCLK_BIT_IOCON);
	RCC_ResetAHBCLK(1<<AHBCLK_BIT_UART1);
	RCC_UARTCLKSel(RCC_UARTCLK_SOURCE_SYSPLL);
	RCC_SETCLKDivider(RCC_CLOCKFREQ_UART1CLK, 12);

	GPIO_SetPinMux(GPIO0, GPIO_Pin_18, GPIO_FUNCTION_2);
	GPIO_SetPinMux(GPIO0, GPIO_Pin_19, GPIO_FUNCTION_2);

	UART_Reset(UART1);
	UART_StructInit(&UART_InitStructure);
	UART_Init(UART1, &UART_InitStructure);
	UART_Cmd(UART1, ENABLE);
}


/**
  * @brief  Retargets the C library printf function to the UART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE
{
	/* Place your implementation of fputc here */
	/* e.g. write a character to the UART */
	UART_SendData(UART1, (uint8_t) ch);
	
	/* Loop until the end of transmission */
	while (UART_GetFlagStatus(UART1, UART_FLAG_TXFE) == RESET)
	{}
	
	return ch;
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  printf("Wrong parameters value: file %s on line %d\r\n", file, line);
  /* Infinite loop */
  while (1)
  {
  }
}
#endif


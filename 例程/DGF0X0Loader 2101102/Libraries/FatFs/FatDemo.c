/*----------------------------------------------------------------------*/
/* FatFs sample														    */
/*----------------------------------------------------------------------*/

#include <stdio.h>
#include "ff.h"
#include "m1130_eval_qspi.h"
#include "m1130_qspi.h"
#include "part.h"
#include "stdlib.h"

// #define USE_QSPI0_FLASH
#define LOADER_APP_FILENAME "Loader_App.bin"
#define LED_CONFIG_FILENAME "config.bin" //led ID+宽高+位置
#define LED_DATA_FILENAME   "data.bin"   //led 在什么位置显示什么图片，图片怎样运动;图片数据
#define BUFF_SIZE           512

#define LOADER_BURN_ADDR 0
#define APP_BURN_ADDR    0x8000 //32k

#define LOADER_SIZE     0x8000 //32K
#define LED_CONFIG_SIZE 0x1000
#define LED_DATA_SIZE   0x1000

#ifdef USE_QSPI0_FLASH        //QSPI0 flash
#define LED_CONFIG_BURN_ADDR 0x20000 //128k
#define LED_DATA_BURN_ADDR   0x21000 //132k
#else                                //QSPI1 flash
#define LED_CONFIG_BURN_ADDR 0x0
#define LED_DATA_BURN_ADDR   0x1000
#endif
unsigned char *Buff; /* 512B for all temp buffer */
FATFS *pFatfs;       /* File system object */
FIL *pFil;           /* File object */
void die(            /* Stop with dying message */
         FRESULT rc  /* FatFs return value */
)
{
  printf("Failed with rc=%u.\r\n", rc);
}

FRESULT upgrade(QSPI_TypeDef *QSPIptr, const char *filename, uint32_t faddr)
{
  FRESULT rc; /* Result code */
  DIR dir;    /* Directory object */
  UINT br, i, shift;
  UINT readSize;
  DWORD size;

  printf("start to burn %s into flash\r\n", filename);
  rc = f_open(pFil, filename, FA_READ);
  if (rc) {
    printf("file:%s open fail\r\n", filename);
    return rc;
  }

  if (faddr == LOADER_BURN_ADDR) {
    size = LOADER_SIZE;
  } else if (faddr == APP_BURN_ADDR) {
    size = pFil->fsize - LOADER_SIZE;
  } else {
    size = pFil->fsize;
  }

  shift = 0;
  while (shift < size) {
    qspi_flash_write_enable(QSPIptr, 1);
    qspi_flash_erase_block_4k(QSPIptr, faddr + shift); //Must be done before write
    shift += 0x1000;
  }
  shift = 0;
  for (;;) {
    if (shift + BUFF_SIZE <= size) {
      readSize = BUFF_SIZE;
    } else {
      readSize = size - shift;
    }
    rc = f_read(pFil, Buff, readSize, &br); /* Read a chunk of file */
    if (rc || !br)
			break; /* Error or end of file */
		qspi_flash_write(QSPIptr, Buff, faddr + shift, readSize);
		shift += readSize;
		if (shift >= size)
      break;
  }
  f_close(pFil);
  if (rc) {
    return rc;
  }

  return FR_OK;
}

int isUpgrade(QSPI_TypeDef *QSPIptr, const char *filename, int faddr, int size)
{
  int oldVersion = 0;
  int RoldVersion = 0;
  int newVersion = 0;
  FRESULT rc; /* Result code */
  UINT br;
  //get old version
  if (faddr != APP_BURN_ADDR) {
    qspi_flash_read(QSPIptr, (uint8_t *)(&oldVersion), faddr + size - 4, 4);
  } else {
    while (1) {
      size += 0x8000; //32KB
      qspi_flash_read(QSPIptr, (uint8_t *)(&oldVersion), faddr + size - 8, 4);
      qspi_flash_read(QSPIptr, (uint8_t *)(&RoldVersion), faddr + size - 4, 4);
      if (size > 0x80000) return -1;
      if (oldVersion == ~RoldVersion) break;
    }
  }
  //get new version
  if (f_open(pFil, filename, FA_READ)) {
    printf("file:%s open fail\r\n", filename);
    return -1;
  }

  if (faddr != APP_BURN_ADDR) {
    f_lseek(pFil, size - 4);
  } else {
    f_lseek(pFil, pFil->fsize - 8);
  }
  rc = f_read(pFil, &newVersion, 4, &br); /* Read a chunk of file */
  if (rc || !br) {
    return -1;
  }
  f_close(pFil);
  printf("oldversion=%d,newversion=%d\r\n", oldVersion & 0xffffffff, newVersion);
  if (newVersion > oldVersion) {
    return 0;
  }
  return -1;
}

void flashUpgrade(QSPI_TypeDef *QSPIptr)
{
  //Loader.bin
  if (!isUpgrade(QSPIptr, LOADER_APP_FILENAME, LOADER_BURN_ADDR, LOADER_SIZE)) {
    upgrade(QSPIptr, LOADER_APP_FILENAME, LOADER_BURN_ADDR);
  }
  //App.bin
  if (!isUpgrade(QSPIptr, LOADER_APP_FILENAME, APP_BURN_ADDR, 0)) {
    upgrade(QSPIptr, LOADER_APP_FILENAME, APP_BURN_ADDR);
  }
  //config.bin
  // if (!isUpgrade(QSPIptr, LED_CONFIG_FILENAME, LED_CONFIG_BURN_ADDR, LED_CONFIG_SIZE)) {
    upgrade(QSPIptr, LED_CONFIG_FILENAME, LED_CONFIG_BURN_ADDR);
  // }
  //Infor.bin and pic.bin
  // if (!isUpgrade(QSPIptr, INFO_FILENAME, LED_DATA_BURN_ADDR, LED_DATA_SIZE)){
  upgrade(QSPIptr, LED_DATA_FILENAME, LED_DATA_BURN_ADDR);
  // }
  return;
}
/*-----------------------------------------------------------------------*/
/* Program Main                                                          */
/*-----------------------------------------------------------------------*/

int FatDemo(void)
{
  FRESULT rc;  /* Result code */
  DIR dir;     /* Directory object */
  FILINFO fno; /* File information object */
  UINT br, i, shift;
  UINT readSize;
  //跳转到XIP之前,可以利用RAM1的空间用作FAT数据
  Buff = (unsigned char *)MEM_Buff; //512B
  pFatfs = (FATFS *)(MEM_Buff + BUFF_SIZE);
  pFil = (FIL *)(MEM_Buff + BUFF_SIZE + sizeof(FATFS));

  /*注册磁盘卷 0表示 U盘*/
  f_mount(0, pFatfs); /* Register volume work area (never fails) */

#if 1
#ifdef USE_QSPI0_FLASH
  flashUpgrade(QSPI0);
#else
  quad_spi_flash_init(QSPI1, 2, 4, 0);
  flashUpgrade(QSPI1);
#endif
#endif
#if 0
	/**********************************************************/
	//读文件示例
	printf("Open an existing file (data.txt).\r\n");
	rc = f_open(pFil, "data.txt", FA_READ);
	if (rc) die(rc);

	printf("read the file content.\r\n");
	for (;;) {
		rc = f_read(pFil, Buff, 512, &br);	/* Read a chunk of file */
		if (rc || !br) break;			/* Error or end of file */
		// for (i = 0; i < br; i++)		/* Type the data */
		// 	putchar(Buff[i]);
	}
	if (rc) die(rc);

	printf("Close the file.\r\n");
	rc = f_close(pFil);
	if (rc) die(rc);
#endif

#if 0
	/**********************************************************/
	//读文件示例
	printf("\r\nOpen an existing file (message.txt).\r\n");
	rc = f_open(pFil, "MESSAGE.TXT", FA_READ);
	if (rc) die(rc);

	printf("\r\nType the file content.\r\n");
	for (;;) {
		rc = f_read(pFil, Buff, BUFF_SIZE, &br);	/* Read a chunk of file */
		if (rc || !br) break;			/* Error or end of file */
		for (i = 0; i < br; i++)		/* Type the data */
			putchar(Buff[i]);
	}
	if (rc) die(rc);

	printf("\r\nClose the file.\r\n");
	rc = f_close(pFil);
	if (rc) die(rc);
#endif
  /**********************************************************/

#if 0
	/**********************************************************/
	//创建和写文件示例
	printf("\r\nCreate a new file (hello.txt).\r\n");
	rc = f_open(&Fil, "HELLO.TXT", FA_WRITE | FA_CREATE_ALWAYS);
	if (rc) die(rc);

	printf("\r\nWrite a text data. (Hello world!)\r\n");
	rc = f_write(&Fil, "Hello world!\r\n", 14, &bw);
	if (rc) die(rc);
	printf("%u bytes written.\r\n", bw);

	printf("\r\nClose the file.\r\n");
	rc = f_close(&Fil);
	if (rc) die(rc);
	/**********************************************************/
#endif
#if 0
	/**********************************************************/
	//列目录示例
	printf("\r\nOpen root directory.\r\n");
	rc = f_opendir(&dir, "");
	if (rc) die(rc);

	printf("\r\nDirectory listing...\r\n");
	for (;;) {
		rc = f_readdir(&dir, &fno);		/* Read a directory item */
		if (rc || !fno.fname[0]) break;	/* Error or end of dir */
		if (fno.fattrib & AM_DIR)
			printf("   <dir>  %s\r\n", fno.fname);
		else
			printf("%8lu  %s\r\n", fno.fsize, fno.fname);
	}
	if (rc) die(rc);
	/**********************************************************/
#endif
#if 0 //read test.bin and write into flash
	rc = f_open(pFil, "test.bin", FA_READ);
	if (rc) die(rc);
		
	printf("\r\nStart save USB data\r\n");
	shift = 0;
	while(shift<pFil->fsize){
		qspi_flash_write_enable(1);
		qspi_flash_erase_block_4k(0xE0000+shift);//Must be done before write
		shift += 0x1000;
	}

	shift = 0;
	for (;;) {
		if(shift+BUFF_SIZE<=pFil->fsize){
			readSize = BUFF_SIZE;
		}else{
			readSize = pFil->fsize-shift;
		}
		rc = f_read(pFil, Buff, readSize, &br);	/* Read a chunk of file */
		if (rc || !br) break;				/* Error or end of file */
		qspi_flash_write(Buff, 0xE0000+shift, readSize);
		shift += readSize;
		if(shift>=pFil->fsize)break;
	}
	if (rc) die(rc);
	
	printf("\r\nEnd save USB data\r\n");
#endif

  printf("FatDemo completed.\r\n");
  return 0;
}

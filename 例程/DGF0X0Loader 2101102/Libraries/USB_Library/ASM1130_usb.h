/*
 * Blackfin MUSB HCD  Host Controller Driver) for u-boot
 *
 * Copyright (c) 2012 Alpscale.
 *
 * Licensed under the GPL-2 or later.
 */

#ifndef __M1130_USB_H__
#define __M1130_USB_H__



struct bfin_musb_dma_regs {
	uint16_t interrupt;
	uint16_t control;
	uint16_t addr_low;
	uint16_t addr_high;
	uint16_t count_low;
	uint16_t count_high;
	uint32_t reserved0[2];
};



/* EP5-EP7 are the only ones with 1024 byte FIFOs which BULK really needs */
//#define MUSB_BULK_EP 1


#endif

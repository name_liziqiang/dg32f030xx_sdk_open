#ifndef __LED_COMMON_H
#define __LED_COMMON_H

#ifdef __cplusplus
extern "C"
{
#endif

void LED_Init(void);

//这个配置支持64*32*RGB*8灰度(前景)	,	假如要前后景叠加,支持4灰度
#define PIC0_FADDR 0x0007800
#define PIC_DATA_SIZE	0x40




#ifdef __cplusplus
}
#endif

#endif /*__LED_COMMON_H */


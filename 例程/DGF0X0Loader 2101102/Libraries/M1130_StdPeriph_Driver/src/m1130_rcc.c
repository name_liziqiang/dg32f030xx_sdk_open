/**
  ******************************************************************************
  * @file    m1800_rcc.c
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    10/12/2013
  * @brief   This file provides all the RCC firmware functions.
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  */ 

/* Includes ------------------------------------------------------------------*/
#include "m1130_rcc.h"
#include "main.h"

/** @addtogroup M1800_StdPeriph_Driver
  * @{
  */

/** @defgroup RCC 
  * @brief RCC driver modules
  * @{
  */ 

/** @defgroup RCC_Private_TypesDefinitions
  * @{
  */

/**
  * @}
  */

/** @defgroup RCC_Private_Defines
  * @{
  */

#define RCC_SYSPLLSTATE_TIMEOUT     		0x10000
#define RCC_SYSPLLUEN_DISABLE				0
#define RCC_SYSPLLUEN_ENABLE				1
#define RCC_SYSPLLCTRL_CLEAR_Mask			0xFFE0
#define RCC_SYSPLLSTATE_LOCK				1

#define RCC_MAINCLKUEN_DISABLE				0
#define RCC_MAINCLKUEN_ENABLE				1

#define RCC_UARTCLKUEN_DISABLE				0
#define RCC_UARTCLKUEN_ENABLE				1

#define RCC_WDTCLKUEN_DISABLE				0
#define RCC_WDTCLKUEN_ENABLE				1

#define RCC_OUTCLKUEN_DISABLE				0
#define RCC_OUTCLKUEN_ENABLE				1

#define RCC_DEEPSLEEPEN_SET					0x0002
#define RCC_DEEPSLEEPEN_RESET				0xFFFD

/**
  * @}
  */

/** @defgroup RCC_Private_FunctionPrototypes
  * @{
  */

/**
  * @}
  */

/** @defgroup RCC_Private_Functions
  * @{
  */

void RCC_SetAHBCLK(uint32_t AHBCLK, FunctionalState NewState)
{
	if (NewState != DISABLE)
	{
		RCC->AHBCLKCTRL0_SET = AHBCLK;
	}
	else
	{
		RCC->AHBCLKCTRL0_CLR = AHBCLK;
	}
}

void RCC_SetPRESETCTRL(uint32_t PRESETCTRL, FunctionalState NewState)
{
	if (NewState != DISABLE)
	{
		RCC->PRESETCTRL0_SET = PRESETCTRL;
	}
	else
	{
		RCC->PRESETCTRL0_CLR = PRESETCTRL;
	}
}


void RCC_ResetAHBCLK(uint32_t AHBCLK)
{
  RCC_SetAHBCLK(AHBCLK, ENABLE);
  RCC_SetPRESETCTRL(AHBCLK, DISABLE);
  RCC_SetPRESETCTRL(AHBCLK, ENABLE);
}
/**
  * @brief  Select MAIN Clock Source 
  * @param  RCC_MAINCLKSource: specifies MAIN Clock Source .
  *   This parameter can be the following values:
  *     @arg RCC_MAINCLK_SOURCE_IRC
  *     @arg RCC_MAINCLK_SOURCE_SYSPLL
  *
  * @retval None
  */
void RCC_MAINCLKSel(uint8_t RCC_MAINCLKSource)
{
	assert_param(IS_RCC_MAINCLK_SOURCE(RCC_MAINCLKSource));
	RCC->MAINCLKSEL = RCC_MAINCLKSource;
	RCC->MAINCLKUEN = RCC_MAINCLKUEN_DISABLE;
	RCC->MAINCLKUEN = RCC_MAINCLKUEN_ENABLE;	
}

/**
  * @brief  Select UART Clock Source 
  * @param  RCC_UARTCLKSource: specifies UART Clock Source .
  *   This parameter can be the following values:
  *     @arg RCC_UARTCLK_SOURCE_IRC
  *     @arg RCC_UARTCLK_SOURCE_SYSPLL
  * @retval None
  */
void RCC_UARTCLKSel(uint8_t RCC_UARTCLKSource)
{
	assert_param(IS_RCC_UARTCLK_SOURCE(RCC_UARTCLKSource));
	RCC->UARTCLKSEL = RCC_UARTCLKSource;
	RCC->UARTCLKUEN = RCC_UARTCLKUEN_DISABLE;
	RCC->UARTCLKUEN = RCC_UARTCLKUEN_ENABLE;
}

/**
  * @brief  Select OUT Clock Source 
  * @param  RCC_OUTCLKSource: specifies OUT Clock Source .
  *   This parameter can be the following values:
  *     @arg RCC_OUTCLK_SOURCE_IRC
  *     @arg RCC_OUTCLK_SOURCE_MAIN
  *     @arg RCC_OUTCLK_SOURCE_WDT
  *     @arg RCC_OUTCLK_SOURCE_RTC
  * @retval None
  */
void RCC_OUTCLKSel(uint8_t RCC_OUTCLKSource)
{
	assert_param(IS_RCC_OUTCLK_SOURCE(RCC_OUTCLKSource));
	RCC->OUTCLKSEL = RCC_OUTCLKSource;
	RCC->OUTCLKUEN = RCC_OUTCLKUEN_DISABLE;
	RCC->OUTCLKUEN = RCC_OUTCLKUEN_ENABLE;	
}

/**
  * @brief  Select WDT Clock Source 
  * @param  RCC_WDTCLKSource: specifies WDT Clock Source .
  *   This parameter can be the following values:
  *     @arg RCC_WDTCLK_SOURCE_IRC
  *     @arg RCC_WDTCLK_SOURCE_SYSPLL
  * @retval None
  */
void RCC_WDTCLKSel(uint8_t RCC_WDTCLKSource)
{
	assert_param(IS_RCC_WDTCLK_SOURCE(RCC_WDTCLKSource));
	RCC->WDTCLKSEL = RCC_WDTCLKSource;
	RCC->WDTCLKUEN = RCC_WDTCLKUEN_DISABLE;
	RCC->WDTCLKUEN = RCC_WDTCLKUEN_ENABLE;
}


/**
  * @brief  Set Clock divider factor .
  * @param  RCC_Peripheral: specifies the Peripheral.
  *   This parameter can be one of the following values:
  *     @arg RCC_CLOCKFREQ_SYSAHBCLK	
  *     @arg RCC_CLOCKFREQ_UART0CLK			
  *     @arg RCC_CLOCKFREQ_UART1CLK			
  *     @arg RCC_CLOCKFREQ_UART2CLK			
  *     @arg RCC_CLOCKFREQ_UART3CLK			
  *     @arg RCC_CLOCKFREQ_SYSTICKCLK			
  *     @arg RCC_CLOCKFREQ_SPI0CLK				
  *     @arg RCC_CLOCKFREQ_WDTCLK			
  *     @arg RCC_CLOCKFREQ_CLKOUTCLK		
  *     @arg RCC_CLOCKFREQ_SSP0CLKDIV	
  * @param  RCC_CLKDIV: specifies the Clock divider factor.
  *   This parameter can be RCC_CLKDIV_x where x:[0,255].
  * @retval None
  */
void RCC_SETCLKDivider(uint32_t RCC_Peripheral_Clock, uint8_t RCC_CLKDIV)
{
	assert_param(IS_RCC_CLKDIV_PER(RCC_Peripheral_Clock));
	(*((__IO uint8_t *)(RCC_BASE + RCC_Peripheral_Clock))) = RCC_CLKDIV;		
}


/**
  * @brief  Configures the SYSPLL multiplication factor .
  * @param  RCC_SYSPLL: specifies the SYSPLL multiplication factor.
  *     @arg RCC_SYSPLL_VAL_120
  *     @arg RCC_SYSPLL_VAL_144
  *     @arg RCC_SYSPLL_VAL_156
  *     @arg RCC_SYSPLL_VAL_168
  *     @arg RCC_SYSPLL_VAL_180
  *     @arg RCC_SYSPLL_VAL_192
  *     @arg RCC_SYSPLL_VAL_204
  *     @arg RCC_SYSPLL_VAL_216
  *     @arg RCC_SYSPLL_VAL_228
  *     @arg RCC_SYSPLL_VAL_240
  * @retval None
  */
void RCC_SYSPLLConfig( uint32_t RCC_SYSPLL)
{
	uint32_t tmpreg = 0;
	uint32_t timeout = 0;
	assert_param(IS_RCC_SYSPLL_VAL(RCC_SYSPLL));

	tmpreg = RCC->SYSPLLCTRL;
	tmpreg &= RCC_SYSPLLCTRL_CLEAR_Mask;
	tmpreg |= RCC_SYSPLL | RCC_SYSPLLCTRL_FORCELOCK;
	RCC->SYSPLLCTRL = tmpreg;

	while((RCC->SYSPLLSTAT) != RCC_SYSPLLSTATE_LOCK)
	{
		if((timeout++) >= RCC_SYSPLLSTATE_TIMEOUT)
			break;
	}
}

/**
  * @brief  Capture PIO value whether it is 1 or 0.
  * @param  RCC_PortNum: specifies the number of Port.
  *   This parameter can be RCC_PortNum_x where x:[0,10].
  * @param  RCC_PortNum: specifies the number of Pin.
  *   This parameter can be RCC_PinNum_x where x:[0,7].
  * @retval The new state of PIO (SET(1) or RESET(0)).
  */
FlagStatus RCC_CAPPIO(uint32_t RCC_PortNum, uint32_t RCC_PinNum)
{
	FlagStatus bitstatus = RESET;
	assert_param(IS_RCC_CAPPIO_PinNum(RCC_PinNum));

	if(((RCC->PIOPORCAP0) & (1<<(RCC_PinNum + (RCC_PortNum * 8)))) == 0)
		bitstatus = RESET;
	else
		bitstatus = SET;

	return bitstatus; 	
}

/**
  * @brief  Controls the start logic inputs of GPIO0,1,2,3,4.
  * @param  RCC_GPIOxPINy: specifies the number of pin.
  *   This parameter can be RCC_GPIOx_PINy where x:[0,4] y[0,7]	.
  * @param  RCC_APRP_MODE: specifies a falling or rising edge.
  *   This parameter can be one of the following values:
  *     @arg RCC_APRP_MODE_RisingEdge
  *     @arg RCC_APRP_MODE_FallingEdge
  * @retval None
  */
void RCC_APRPConfig(uint32_t RCC_GPIOxPINy, uint32_t RCC_APRP_MODE)
{
	assert_param(IS_RCC_GPIOX_PINY(RCC_GPIOxPINy));
	assert_param(IS_RCC_APRP_MODE(RCC_APRP_MODE));
	if(RCC_APRP_MODE == RCC_APRP_MODE_RisingEdge)
	{
		RCC->STARTAPRP0 |= (1 << RCC_GPIOxPINy);
	}
	else
	{
		RCC->STARTAPRP0 &= ~(1 << RCC_GPIOxPINy);
	}
}

/**
  * @brief  Enable start signal for start logic input GPIO0,1,2,3,4.
  * @param  RCC_GPIOxPINy: specifies the number of pin.
  *   This parameter can be RCC_GPIOx_PINy where x:[0,4] y[0,7]	.
  * @param  NewState: new state of the logic input. This parameter can be: ENABLE or DISABLE.
  * @retval None
  */
void RCC_ERPCmd(uint32_t RCC_GPIOxPINy, FunctionalState NewState)
{
	assert_param(IS_RCC_GPIOX_PINY(RCC_GPIOxPINy));
	assert_param(IS_FUNCTIONAL_STATE(NewState));
	if(NewState != DISABLE)
	{
		RCC->STARTERP0 |= (1 << RCC_GPIOxPINy);

	}
	else
	{
		RCC->STARTERP0 &= ~(1 << RCC_GPIOxPINy);

	}
}

/**
  * @brief  Start signal reset for start logic input GPIO0,1,2,3,4.
  * @param  RCC_GPIOxPINy: specifies the number of pin.
  *   This parameter can be RCC_GPIOx_PINy where x:[0,4] y[0,7]	.
  * @retval None
  */
void RCC_RSRPCmd(uint32_t RCC_GPIOxPINy)
{
	assert_param(IS_RCC_GPIOX_PINY(RCC_GPIOxPINy));
	RCC->STARTRSRP0 |= (1 << RCC_GPIOxPINy);

}


/**
  * @brief  Checks whether the logic input flag is set or not.
  * @param  RCC_GPIOxPINy: specifies the number of pin.
  *   This parameter can be RCC_GPIOx_PINy where x:[0,4] y[0,7]	.
  * @retval The new state of GPIO_PIN logic input (SET or RESET).
  */
FlagStatus RCC_GETSRP(uint32_t RCC_GPIOxPINy)
{
	FlagStatus bitstatus = RESET;
	assert_param(IS_RCC_GPIOX_PINY(RCC_GPIOxPINy));
	
	if((RCC->STARTSRP0 & (1 << RCC_GPIOxPINy)) != 0)
		bitstatus = SET;
	else
		bitstatus = RESET;

	return bitstatus;	
}

/**
  * @brief  Configures the state the chip must enter when the Deep-sleep mode is asserted by the ARM.
  * @param  RCC_Analog_Block: specifies Module which will be power on or down.
  *   This parameter can be any combination of the following values:
  *     @arg RCC_PDCFG_EXT12M				
  *     @arg RCC_PDCFG_RTC					
  *     @arg RCC_PDCFG_ADC0			
  *     @arg RCC_PDCFG_SYSPLL			
  *     @arg RCC_PDCFG_BOD				
  *     @arg RCC_PDCFG_BOR				
  *     @arg RCC_PDCFG_IRC10K		
  *     @arg RCC_PDCFG_IRC12M			
  *
  * @param  PD_MODE: new mode of the Oscillator.
  *   This parameter can be one of the following values:
  *     @arg RCC_PDCFG_POWER_ON
  *     @arg RCC_PDCFG_POWER_DOWN
  * @retval None
  */
void RCC_PDSLEEPConfig(uint16_t RCC_Analog_Block, uint8_t PD_MODE)
{
	assert_param(IS_RCC_PDCFG_ANALOG_BLOCK(RCC_Analog_Block));
	assert_param(IS_RCC_PDCFG_MODE(PD_MODE));
	if(PD_MODE != RCC_PDCFG_POWER_DOWN)
	{
		RCC->PDSLEEPCFG &= ~RCC_Analog_Block;
	}
	else
	{
		RCC->PDSLEEPCFG |= RCC_Analog_Block;
	}
}

/**
  * @brief  Configures the state the chip must enter when it is waking up from Deep-sleep mode.
  * @param  RCC_Analog_Block: specifies Module which will be power on or down.
  *   This parameter can be any combination of the following values:
  *     @arg RCC_PDCFG_EXT12M				
  *     @arg RCC_PDCFG_RTC					
  *     @arg RCC_PDCFG_ADC0			
  *     @arg RCC_PDCFG_SYSPLL			
  *     @arg RCC_PDCFG_BOD				
  *     @arg RCC_PDCFG_BOR				
  *     @arg RCC_PDCFG_IRC10K		
  *     @arg RCC_PDCFG_IRC12M			
  *
  * @param  PD_MODE: new mode of the Oscillator.
  *   This parameter can be one of the following values:
  *     @arg RCC_PDCFG_POWER_ON
  *     @arg RCC_PDCFG_POWER_DOWN
  * @retval None
  */
void RCC_PDAWAKEConfig(uint16_t RCC_Analog_Block, uint8_t PD_MODE)
{
	assert_param(IS_RCC_PDCFG_ANALOG_BLOCK(RCC_Analog_Block));
	assert_param(IS_RCC_PDCFG_MODE(PD_MODE));
	if(PD_MODE != RCC_PDCFG_POWER_DOWN)
	{
		RCC->PDAWAKECFG &= ~RCC_Analog_Block;
	}
	else
	{
		RCC->PDAWAKECFG |= RCC_Analog_Block;
	}
}

/**
  * @brief  Configures the power to the various analog blocks.
  * @param  RCC_Analog_Block: specifies Module which will be power on or down.
  *   This parameter can be any combination of the following values:
  *     @arg RCC_PDCFG_EXT12M				
  *     @arg RCC_PDCFG_RTC					
  *     @arg RCC_PDCFG_ADC0			
  *     @arg RCC_PDCFG_SYSPLL			
  *     @arg RCC_PDCFG_BOD				
  *     @arg RCC_PDCFG_BOR				
  *     @arg RCC_PDCFG_IRC10K		
  *     @arg RCC_PDCFG_IRC12M			
  *
  * @param  PD_MODE: new mode of the Oscillator.
  *   This parameter can be one of the following values:
  *     @arg RCC_PDCFG_POWER_ON
  *     @arg RCC_PDCFG_POWER_DOWN
  * @retval None
  */
void RCC_PDRUNConfig(uint16_t RCC_Analog_Block, uint8_t PD_MODE)
{
	assert_param(IS_RCC_PDCFG_ANALOG_BLOCK(RCC_Analog_Block));
	assert_param(IS_RCC_PDCFG_MODE(PD_MODE));
	if(PD_MODE != RCC_PDCFG_POWER_DOWN)
	{
		RCC->PDRUNCFG &= ~RCC_Analog_Block;
	}
	else
	{
		RCC->PDRUNCFG |= RCC_Analog_Block;
	}
}

/**
  * @brief  Enables or disables the Deep-Sleep mode.
  * @param  NewState: new state of the Deep-Sleep mode.
  *   This parameter can be: ENABLE or DISABLE.
  * @retval None
  */
void RCC_DeepsleepCmd(FunctionalState NewState)
{
	assert_param(IS_FUNCTIONAL_STATE(NewState));
	if (NewState != DISABLE)
	{
		RCC->PCON |= RCC_DEEPSLEEPEN_SET;
	}
	else
	{
		RCC->PCON &= RCC_DEEPSLEEPEN_RESET;
	}
}

/**
  * @brief  Checks whether the specified RCC flag is set or not.
  * @param  RCC_FLAG: specifies the flag to check.
  *   This parameter can be one of the following values:
  *     @arg RCC_SYSRSTSTATE_POR
  *     @arg RCC_SYSRSTSTATE_EXERSTN
  *     @arg RCC_SYSRSTSTATE_WDT
  *     @arg RCC_SYSRSTSTATE_BOD     
  *     @arg RCC_SYSRSTSTATE_SYSRST                        
  *     @arg RCC_SLEEPFLAG 
  *     @arg RCC_DPDFLAG 
  * @retval The new state of RCC_FLAG (SET or RESET).
  */
FlagStatus RCC_GetSYSRSTFlagStatus(uint16_t RCC_FLAG)
{
	FlagStatus bitstatus = RESET;
	assert_param(IS_RCC_SYSRST_FLAG(RCC_FLAG));
		if ((RCC->SYSRSTSTAT & RCC_FLAG)!= (uint8_t)RESET)
		{
			bitstatus = SET;
		}
		else
		{
			bitstatus = RESET;
		}
	/* Return the flag status */
	return bitstatus;
}

FlagStatus RCC_GetPCONFlagStatus(uint16_t RCC_FLAG)
{
	FlagStatus bitstatus = RESET;
	assert_param(IS_RCC_PCON_FLAG(RCC_FLAG));
		if ((RCC->PCON & RCC_FLAG)!= (uint16_t)RESET)
		{
			bitstatus = SET;
		}
		else
		{
			bitstatus = RESET;
		}
	/* Return the flag status */
	return bitstatus;
}

/**
  * @brief  Clears the RCC reset flags.
  * @param  RCC_FLAG: specifies the flag to clear.
  *   This parameter can be one of the following values:
  *     @arg RCC_SYSRSTSTATE_POR
  *     @arg RCC_SYSRSTSTATE_EXERSTN
  *     @arg RCC_SYSRSTSTATE_WDT
  *     @arg RCC_SYSRSTSTATE_BOD     
  *     @arg RCC_SYSRSTSTATE_SYSRST                        
  *     @arg RCC_SLEEPFLAG 
  *     @arg RCC_DPDFLAG 
  * @retval None
  */
void RCC_ClearSYSRSTFlag(uint16_t RCC_FLAG)
{
	assert_param(IS_RCC_SYSRST_FLAG(RCC_FLAG));
	RCC->SYSRSTSTAT |= RCC_FLAG;
}

void RCC_ClearPCONFlag(uint16_t RCC_FLAG)
{
	assert_param(IS_RCC_PCON_FLAG(RCC_FLAG));
	RCC->PCON |= RCC_FLAG;
}

/**
  * @brief  Set the systick Calibration.
  * @param  SystickCalibration: specifies the Calibration.
  *   This parameter can be any value of 26 bits data.
  * @retval None
  */
void RCC_SetSystickCal(uint32_t SystickCalibration)
{
	assert_param(IS_RCC_SYSTICK_CAL(SystickCalibration));
	RCC->SYSTICKCAL = SystickCalibration;
}

/**
  * @brief  Configures the BOD interrupt electrical level.
  * @param  BODINTVal: specifies BOD interrupt electrical level.
  *   This parameter can be one of the following values:
  *     @arg RCC_BODINT_VAL_1_69
  *     @arg RCC_BODINT_VAL_2_29
  *     @arg RCC_BODINT_VAL_2_59
  *     @arg RCC_BODINT_VAL_2_87
  * @param  NewState: new mode of the BOD reset.
  *   This parameter can be: ENABLE or DISABLE.
  * @retval None
  */
void RCC_BODConfig(uint8_t BODINTVal, FunctionalState NewState)
{  	
	uint8_t tmpreg;
	assert_param(IS_FUNCTIONAL_STATE(NewState));
	assert_param(IS_RCC_BODINT_VAL(BODINTVal));

	tmpreg = RCC->BODCTRL;
	tmpreg &= RCC_BODCTRL_CLEAR_Mask;
	tmpreg |= BODINTVal;
	
	if(NewState != DISABLE)
	{
		RCC->BODCTRL |= RCC_BODRSTENA_SET_Mask;	
	}
	else
	{
		RCC->BODCTRL &= RCC_BODRSTENA_RESET_Mask;	
	}
}

uint32_t RCC_GetClocksFreq(uint32_t RCC_Clocks)
{
 	uint32_t tmpclk, syspll, mainclk, hclk, uartclk, wdtclk, outclk;
	uint8_t mainclk_sel, uartclk_sel, wdtclk_sel, outclk_sel;
	
	mainclk_sel = RCC->MAINCLKSEL;
	uartclk_sel = RCC->UARTCLKSEL;
	wdtclk_sel = RCC->WDTCLKSEL;

#ifdef EXECUTE_ON_FPGA
	syspll = 96000000;
#else
	syspll = ((RCC->SYSPLLCTRL & RCC_SYSPLLCTRL_FREQ) + 10) * 12000000;
#endif
	if (mainclk_sel == RCC_MAINCLK_SOURCE_12MIRC)
		mainclk = 12000000;	
	else if (mainclk_sel == RCC_MAINCLK_SOURCE_SYSPLL)
		mainclk	= syspll;
	else if (mainclk_sel == RCC_MAINCLK_SOURCE_OSC)
		mainclk	= 12000000;
	else if (mainclk_sel == RCC_MAINCLK_SOURCE_10KIRC)
		mainclk	= 10000;

	hclk = mainclk / RCC->SYSAHBCLKDIV;

	if (RCC_Clocks == RCC_CLOCKFREQ_SYSAHBCLK)
		tmpclk = hclk;

	else if ((RCC_Clocks == RCC_CLOCKFREQ_UART0CLK) || (RCC_Clocks == RCC_CLOCKFREQ_UART1CLK))
	{
		if ((*((__IO uint8_t *)(RCC_BASE + RCC_Clocks))) == 0)
			return 0;

		if (uartclk_sel == RCC_UARTCLK_SOURCE_12MIRC)
		{
			uartclk = 12000000 / (*((__IO uint8_t *)(RCC_BASE + RCC_Clocks)));
		}
		else if (uartclk_sel == RCC_UARTCLK_SOURCE_SYSPLL)
		{
			uartclk = syspll / (*((__IO uint8_t *)(RCC_BASE + RCC_Clocks)));
		}
		else if (uartclk_sel == RCC_UARTCLK_SOURCE_OSC)
		{
			uartclk = 12000000 / (*((__IO uint8_t *)(RCC_BASE + RCC_Clocks)));
		}
		else if (uartclk_sel == RCC_UARTCLK_SOURCE_10KIRC)
		{
			uartclk = 10000 / (*((__IO uint8_t *)(RCC_BASE + RCC_Clocks)));
		}

		tmpclk = uartclk;
	}
	else if (RCC_Clocks == RCC_CLOCKFREQ_WDTCLK)
	{
		if (RCC->WDTCLKDIV == 0)
			return 0;

		if (wdtclk_sel == RCC_WDTCLK_SOURCE_12MIRC)
			wdtclk = 12000000 / RCC->WDTCLKDIV;
		else if (wdtclk_sel == RCC_WDTCLK_SOURCE_SYSPLL)
			wdtclk = syspll / RCC->WDTCLKDIV;
		else if (wdtclk_sel == RCC_WDTCLK_SOURCE_OSC)
			wdtclk = 12000000 / RCC->WDTCLKDIV;

		tmpclk = wdtclk;	
	}
	else if (RCC_Clocks == RCC_CLOCKFREQ_CLKOUTCLK)
	{
		if (RCC->OUTCLKDIV == 0)
			return 0;

		if (outclk_sel == RCC_OUTCLK_SOURCE_12MIRC)
			outclk = 12000000 / RCC->OUTCLKDIV;
		else if (outclk_sel == RCC_OUTCLK_SOURCE_MAIN)
			outclk = mainclk / RCC->OUTCLKDIV;
		else if (outclk_sel == RCC_OUTCLK_SOURCE_10KIRC)
			outclk = 10000 / RCC->OUTCLKDIV;
		else if (outclk_sel == RCC_OUTCLK_SOURCE_12MOSC)
			outclk = 12000000 / RCC->OUTCLKDIV;
		else if (outclk_sel == RCC_OUTCLK_SOURCE_32KOSC)
			outclk = 32000 / RCC->OUTCLKDIV;
		else if (outclk_sel == RCC_OUTCLK_SOURCE_USBPLL)
			outclk = 48000000 / RCC->OUTCLKDIV;

		tmpclk = outclk;	
	}
	else if (outclk_sel == RCC_CLOCKFREQ_SYSTICKCLK)
	{
		if ((*((__IO uint8_t *)(RCC_BASE + RCC_Clocks))) == 0)
			return 0;

		tmpclk = mainclk / (*((__IO uint8_t *)(RCC_BASE + RCC_Clocks)));
	}
	else
	{ 
		/*can't get clock*/
		while(1);
	}

	return tmpclk;
}

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/******************* (C) COPYRIGHT 2010 ALPHASCALE *****END OF FILE****/


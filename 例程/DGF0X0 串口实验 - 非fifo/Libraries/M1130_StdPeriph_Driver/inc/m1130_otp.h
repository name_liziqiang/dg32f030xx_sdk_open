 /**
   ******************************************************************************
   * @file    m1130_cmp.h
   * @author  Alpscale Software Team
   * @version V1.0.0
   * @date    19-December-2013
   * @brief   This file contains all the functions prototypes for the CMP firmware 
   *          library
   ******************************************************************************
   * @attention
   *
   * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
   * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
   * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
   * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
   * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
   * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
   *
   * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
   ******************************************************************************
   */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __M1130_OTP_H
#define __M1130_OTP_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "m1130.h"

void OTP_Reset(void);
uint32_t OTP_ReadSN(void);
uint32_t OTP_ReadADJ_12M(void);
uint32_t OTP_ReadADJ_10K(void);
uint32_t OTP_ReadADJ_LDO(void);
uint32_t OTP_ReadADJ_RTC_CAP(void);

#ifdef __cplusplus
}
#endif

#endif /*__M1130_OTP_H */

/**
  * @}
  */ 

/**
  * @}
  */ 

/************************ (C) COPYRIGHT Alpscale *****END OF FILE****/

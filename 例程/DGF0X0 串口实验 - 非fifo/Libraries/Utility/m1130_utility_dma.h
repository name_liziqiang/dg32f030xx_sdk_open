#ifndef __M1130_UTILITY_DMA_H
#define __M1130_UTILITY_DMA_H

#include "m1130_dma.h"

int DMA_MemCopy(void *dst, void *src, int size);
int DMA_MemSet(void *dst, uint32_t comm, int width, int size);


#endif //__M1130_UTILITY_DMA_H

/**
  ******************************************************************************
  * @file    m1130_rtc.c
  * @author  Alpscale Software Team
  * @version V1.0.0
  * @date    12/20/2013
  * @brief   This file provides all the IWDG firmware functions.
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alpscale</center></h2>
  */ 

/* Includes ------------------------------------------------------------------*/

#include "m1130.h"
#include "m1130_rcc.h"
#include "m1130_rtc.h" 
/** @addtogroup m1130_StdPeriph_Driver
  * @{
  */

/** @defgroup RTC
  * @brief RTC driver modules
  * @{
  */


/**
  * @brief  Open or close the RTC Clock.
  * @param  NewState: new state of RTC Clock. 
  *   This parameter can be: ENABLE or DISABLE. 
  * @retval None
  */
void RTC_CLKSET(FunctionalState NewState)
{
  /* Check the parameters */
  assert_param(IS_FUNCTIONAL_STATE(NewState));
  if (NewState != DISABLE)
  {
    /* Open RTC Clock */
    RCC_SetAHBCLK(1<<AHBCLK_BIT_RTC,ENABLE);
  }
  else
  {
    /* Close RTC Clock */
    RCC_SetAHBCLK(1<<AHBCLK_BIT_RTC,DISABLE);
  }
}

/**
  * @brief  Deinitializes the RTC peripheral registers to their default reset values.
  * @param  None. 
  * @retval None
  */
void RTC_Reset(void)
{
  RCC_ResetAHBCLK(1<<PRESET_BIT_RTC);
}

/**
  * @brief  Enable or disable the RTC interrupts
  * @param  RTC_IT: interrupt will be enabled or disabled
  *   This parameter can be:
  *      @arg RTC_IT_SEC: 1Hz interrupt
  *      @arg RTC_IT_ALARM: alarm interrupt
  * @param  NewState: new state of the specified RTC interrupts.
  *   This parameter can be: ENABLE or DISABLE.
  * @retval None
  */
void RTC_ITConfig(uint32_t RTC_IT, FunctionalState NewState)
{
  assert_param(IS_RTC_IT(RTC_IT));
  assert_param(IS_FUNCTIONAL_STATE(NewState));
  RTC_EnterConfigMode();
  if(NewState != DISABLE)
  {
    RTC->CTRL |= RTC_IT;
  }
  else
  {
    RTC->CTRL &= (uint32_t)~RTC_IT;
  }
  RTC_ExitConfigMode();
}

/**
  * @brief  select the RTC clock source
  * @param  RTC_CLKSOURCE: rtc clk source selections
  *   This parameter can be:
  *      @arg RTC_CLKSOURCE_LSE: low speed external
  *      @arg RTC_CLKSOURCE_IRC: internal rc clock
  *      @arg RTC_CLKSOURCE_OSC: external high speed clock
  * @retval None
  */
void RTC_ClockSelect(uint32_t RTC_CLKSOURCE)
{
  uint32_t temreg;
  assert_param(IS_RTC_CLKSOURCE(RTC_CLKSOURCE));

  temreg = RTC->CTRL;
  temreg &= ~RTC_CTRL_CLKSEL;
  temreg |= RTC_CLKSOURCE;

  RTC_EnterConfigMode();
  RTC->CTRL = temreg;
  RTC_ExitConfigMode();
}

/**
  * @brief  Enable or disable the RTC
  * @param  NewState: new state of RTC Clock. 
  *   This parameter can be: ENABLE or DISABLE. 
  * @retval None
  */
void RTC_Cmd(FunctionalState NewState)
{
  /* Check the parameters */
  assert_param(IS_FUNCTIONAL_STATE(NewState));
  RTC_EnterConfigMode();
  if (NewState != DISABLE)
  {
    RTC->CTRL |= RTC_CTRL_EN;
  }
  else
  {
    RTC->CTRL &= ~RTC_CTRL_EN;
  }
  RTC_ExitConfigMode();
}

/**
  * @brief  Enters the RTC configuration mode.
  * @param  None
  * @retval None
  */
void RTC_EnterConfigMode(void)
{
  RTC->LOAD |= RTC_LOAD_EN;
}

/**
  * @brief  Exits from the RTC configuration mode.
  * @param  None
  * @retval None
  */
void RTC_ExitConfigMode(void)
{
  RTC->LOAD &= ~RTC_LOAD_EN;
}

/**
  * @brief  Gets the RTC counter value.
  * @param  None
  * @retval RTC counter value.
  */
uint32_t RTC_GetCounter(void)
{
  return RTC->CNT;
}

/**
  * @brief  Sets the RTC counter value.
  * @param  CounterValue: RTC counter new value.
  * @retval None
  */
void RTC_SetCounter(uint32_t CounterValue)
{
  RTC_EnterConfigMode();
  /* Set RTC COUNTER word */
  RTC->CNT = CounterValue;
  RTC_ExitConfigMode();
}

/**
  * @brief  Gets the RTC alarm value.
  * @param  None
  * @retval alarm value
  */
uint32_t RTC_GetAlarm(void)
{
  /* Set the ALARM word */
  return (uint32_t) RTC->ALARM;
}

/**
  * @brief  Sets the RTC alarm value.
  * @param  AlarmValue: RTC alarm new value.
  * @retval None
  */
void RTC_SetAlarm(uint32_t AlarmValue)
{
  RTC_EnterConfigMode();
  /* Set the ALARM word */
  RTC->ALARM = AlarmValue;
  RTC_ExitConfigMode();
}

/**
  * @brief  Checks whether the specified RTC flag is set or not.
  * @param  RTC_FLAG: specifies the flag to check.
  *   This parameter can be one the following values:
  *     @arg RTC_FLAG_ALARM: Alarm flag
  *     @arg RTC_FLAG_SEC: Second flag
  * @retval The new state of RTC_FLAG (SET or RESET).
  */
FlagStatus RTC_GetFlagStatus(uint16_t RTC_FLAG)
{
  FlagStatus bitstatus = RESET;
  
  /* Check the parameters */
  assert_param(IS_RTC_GET_FLAG(RTC_FLAG));
  
  if ((RTC->STAT & RTC_FLAG) != (uint16_t)RESET)
  {
    bitstatus = SET;
  }
  else
  {
    bitstatus = RESET;
  }
  return bitstatus;
}

/**
  * @brief  Clears the RTC's pending flags.
  * @param  RTC_FLAG: specifies the flag to clear.
  *   This parameter can be any combination of the following values:
  *     @arg RTC_FLAG_RSF: Registers Synchronized flag. This flag is cleared only after
  *                        an APB reset or an APB Clock stop.
  *     @arg RTC_FLAG_OW: Overflow flag
  *     @arg RTC_FLAG_ALR: Alarm flag
  *     @arg RTC_FLAG_SEC: Second flag
  * @retval None
  */
void RTC_ClearFlag(uint16_t RTC_FLAG)
{
  /* Check the parameters */
  assert_param(IS_RTC_CLEAR_FLAG(RTC_FLAG)); 
    
  /* Clear the corresponding RTC flag */
  RTC->STAT = RTC_FLAG;
}

/**
  * @brief  Checks whether the specified RTC interrupt has occurred or not.
  * @param  RTC_IT: specifies the RTC interrupts sources to check.
  *   This parameter can be one of the following values:
  *     @arg RTC_IT_OW: Overflow interrupt
  *     @arg RTC_IT_ALR: Alarm interrupt
  *     @arg RTC_IT_SEC: Second interrupt
  * @retval The new state of the RTC_IT (SET or RESET).
  */
ITStatus RTC_GetITStatus(uint16_t RTC_IT)
{
  ITStatus bitstatus = RESET;
  /* Check the parameters */
  assert_param(IS_RTC_GET_IT(RTC_IT)); 
  
  bitstatus = (ITStatus)(RTC->STAT & (RTC_IT >> 1));
  if (((RTC->CTRL & RTC_IT) != (uint16_t)RESET) && (bitstatus != (uint16_t)RESET))
  {
    bitstatus = SET;
  }
  else
  {
    bitstatus = RESET;
  }
  return bitstatus;
}

/**
  * @brief  Clears the RTC's interrupt pending bits.
  * @param  RTC_IT: specifies the interrupt pending bit to clear.
  *   This parameter can be any combination of the following values:
  *     @arg RTC_IT_OW: Overflow interrupt
  *     @arg RTC_IT_ALR: Alarm interrupt
  *     @arg RTC_IT_SEC: Second interrupt
  * @retval None
  */
void RTC_ClearITPendingBit(uint16_t RTC_IT)
{
  /* Check the parameters */
  assert_param(IS_RTC_IT(RTC_IT));
  
  /* Clear the corresponding RTC pending bit */
  RTC->STAT = (uint32_t)(RTC_IT >> 1);
}


/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/******************* (C) COPYRIGHT 2013 Alpscale *****END OF FILE****/

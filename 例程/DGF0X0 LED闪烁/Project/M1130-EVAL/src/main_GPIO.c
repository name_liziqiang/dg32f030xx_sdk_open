/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "m1130_uart.h" 
#include "m1130_gpio.h" 
#include "m1130_rcc.h"	 
#include "misc.h"
#include "m1130_dma.h"
#include "m1130_utility_usart.h"


/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */

void GPIO_IRQInit(void)
{
	NVIC_InitTypeDef NVIC_InitStruct;

	NVIC_InitStruct.NVIC_IRQChannel = GPIO0_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
	NVIC_Init(&NVIC_InitStruct);
	
	NVIC_InitStruct.NVIC_IRQChannel = GPIO1_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
	NVIC_Init(&NVIC_InitStruct);
}

uint32_t data;
uint32_t irs;
void GPIO0_IRQHandler(void)
{
	data = GPIO0_IT->MIS;
	irs  = GPIO0_IT->IRS;
	GPIO0_IT->IC = data;
	__asm("NOP");
	__asm("NOP");
}

void GPIO1_IRQHandler(void)
{
	data = GPIO1_IT->MIS;
	irs  = GPIO1_IT->IRS;
	GPIO1_IT->IC = data;
	__asm("NOP");
	__asm("NOP");
}

void testPinMux(uint32_t value)
{
	uint32_t i;
	GPIO_InitTypeDef  GPIO_InitStructure;

	for(i=0;i<32;i++){
		if(i==14 || i==15)//SWD
			continue;
		
		GPIO_InitStructure.GPIO_Pin = 1<<i;
		GPIO_InitStructure.GPIO_Function = GPIO_FUNCTION_0;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_Init(GPIO0, &GPIO_InitStructure);
	}
	for(i=0;i<32;i++){
		GPIO_InitStructure.GPIO_Pin = 1<<i;
		GPIO_InitStructure.GPIO_Function = GPIO_FUNCTION_0;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_Init(GPIO1, &GPIO_InitStructure);
	}
		
	GPIO_WritePort(GPIO0, value);
	GPIO_WritePort(GPIO1, value);
	while(1);
}

void testOut(GPIO_TypeDef *GPIOx, uint32_t GPIO_Pin)
{
	GPIO_SetPinMux(GPIOx, GPIO_Pin, GPIO_FUNCTION_0);
	GPIO_SetPinDir(GPIOx, GPIO_Pin, GPIO_Mode_OUT);
	//屏蔽数据
	//GPIO_PinMask(GPIOx, GPIO_Pin);
	
	GPIO_SetPin(GPIOx, GPIO_Pin);			//out 1
	GPIO_ClearPin(GPIOx, GPIO_Pin);			//out 0
}

void testIn(GPIO_TypeDef *GPIOx, GPIO_IT_TypeDef *GPIOx_IT, uint32_t GPIO_Pin)
{
	GPIO_SetPinMux(GPIOx, GPIO_Pin, GPIO_FUNCTION_0);
	GPIO_SetPinDir(GPIOx, GPIO_Pin, GPIO_Mode_IN);

	//5种触发方式
	GPIO_LevelITEnable(GPIOx_IT, GPIO_Pin, GPIO_IRQ_LEVEL_LOW);
	//GPIO_LevelITEnable(GPIOx_IT, GPIO_Pin, GPIO_IRQ_LEVEL_HIGH);
	//GPIO_EdgeITEnable(GPIOx_IT, GPIO_Pin, GPIO_IRQ_EDGE_FALLING);
	//GPIO_EdgeITEnable(GPIOx_IT, GPIO_Pin, GPIO_IRQ_EDGE_RISING);
	//GPIO_EdgeITEnable(GPIOx_IT, GPIO_Pin, GPIO_IRQ_EDGE_DOUBLE);

	//屏蔽中断
	//GPIO_MaskIT(GPIOx_IT, GPIO_Pin);
}

int main()
{
	uint32_t i;
	
  UART_Configuration(UART1, 115200, UART1_GP2);
	GPIO_IRQInit();
	
	for(i=0;i<32;i++){
		if(i==14 || i==15)//SWD
			continue;
		
		testOut(GPIO0, 1<<i);
		testIn(GPIO0, GPIO0_IT, 1<<i);
	}
	for(i=0;i<32;i++){
		testOut(GPIO1, 1<<i);
		testIn(GPIO1, GPIO1_IT, 1<<i);
	}
	

	#if 0
	GPIO_SetPinMux(GPIO1, GPIO_Pin_18, GPIO_FUNCTION_0);	//GP6_2
	GPIO_SetPinDir(GPIO1, GPIO_Pin_18, GPIO_Mode_OUT);
	GPIO_SetPin(GPIO1, GPIO_Pin_18);
	GPIO_ClearPin(GPIO1, GPIO_Pin_18);
//	GPIO_SetPinMux(GPIO1, GPIO_Pin_0, GPIO_FUNCTION_0);
//	GPIO_SetPinDir(GPIO1, GPIO_Pin_0, GPIO_Mode_IN);
  UART_Configuration(UART1, 115200, UART1_GP2);
//	printf("\r\n-----------------|- m1130 Standard Peripheral Library Driver -|-----------------\r\n");
	#endif
	
	#if 0
	GPIO_SetPinMux(GPIO1, GPIO_Pin_8, GPIO_FUNCTION_0);	//GP5_0 = 0
	GPIO_SetPinDir(GPIO1, GPIO_Pin_8, GPIO_Mode_OUT);
	GPIO_ClearPin(GPIO1, GPIO_Pin_8);
	
	GPIO_SetPinMux(GPIO1, GPIO_Pin_5, GPIO_FUNCTION_0);	//GP4_5 = IN
	GPIO_SetPinDir(GPIO1, GPIO_Pin_5, GPIO_Mode_IN);
	
	//GPIO1_IT->IBE |= GPIO_Pin_18;		//双边沿触发 设为0时IEV有效
	GPIO1_IT->IS  |= GPIO_Pin_5;		//电平触发
	GPIO1_IT->IEV |= GPIO_Pin_5;		//上升沿或高电平触发
	GPIO1_IT->IC |= GPIO_Pin_5;		//清除状态位
	GPIO1_IT->IE  |= GPIO_Pin_5;		//不屏蔽中断
	#endif
	
//	PIO1->PIN18 &= ~(3 << 3);			//下拉电阻
//	PIO1->PIN18 |= 1 << 3;

//	GPIO0_IT->DATAMASK_SET = 0x0000000f;
//	GPIO0_IT->DATAMASK_CLR = 0x0000000f;
//	GPIO1_IT->DATAMASK_SET = 0x0000000f;

	//testPinMux(0x55555555);
	//testPinMux(0xFFFFFFFF);

/*
	while(1)
	{
		GPIO_WritePort(GPIO0, 0xffffffff);
		GPIO_WritePort(GPIO1, 0x00000000);
		commonDelay(1000);
		GPIO_WritePort(GPIO0, 0x00000000);
		GPIO_WritePort(GPIO1, 0xffffffff);
		commonDelay(1000);
	}
	*/
}


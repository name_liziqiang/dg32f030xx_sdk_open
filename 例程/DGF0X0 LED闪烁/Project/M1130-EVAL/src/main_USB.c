/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "m1130_uart.h" 
#include "m1130_gpio.h" 
#include "m1130_rcc.h"	 
#include "misc.h"
#include "m1130_dma.h"
#include "m1130_wdg.h"
#include "m1130_utility_usart.h"


extern int ASM1130_UsbHostIrqInit(void);
extern int ASM1130_UsbHostprocess(void);

int main()
{
  UART_Configuration(UART1, 115200, UART1_GP2);
	printf("\r\n-----------------M1130 Main-----------------\r\n");

#if 0	//GP1_5
	GPIO_SetPinMux(GPIO0, GPIO_Pin_13, GPIO_FUNCTION_0);
	GPIO_SetPinDir(GPIO0, GPIO_Pin_13, GPIO_Mode_OUT);
	GPIO_SetPin(GPIO0, GPIO_Pin_13);	
#else	//GP6_0
	GPIO_SetPinMux(GPIO1, GPIO_Pin_16, GPIO_FUNCTION_0);
	GPIO_SetPinDir(GPIO1, GPIO_Pin_16, GPIO_Mode_OUT);
	GPIO_SetPin(GPIO1, GPIO_Pin_16);	
#endif

	commonDelay(1000);
	ASM1130_UsbHostIrqInit();
	while(1)
	{
		ASM1130_UsbHostprocess();
	}
}


/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "m1130_uart.h" 
#include "m1130_gpio.h" 
#include "m1130_rcc.h"	 
#include "misc.h"
#include "m1130_dma.h"
#include "m1130_utility_usart.h"

#if 0
//64pin
	#define 	KEY_PIN		GPIO_Pin_17
	#define 	LED_PIN		GPIO_Pin_2
	#define 	LED_PORT	GPIO0
#else
//48pin
	#define 	KEY_PIN		GPIO_Pin_23
	#define 	LED_PIN		GPIO_Pin_20
	#define 	LED_PORT	GPIO0
#endif

void delay_ms(uint16_t ms)
{
	uint16_t i;
	for(; ms>0; ms--)
		for(i=0; i<4000; i++);
}
int main()
{
	UART_Configuration(UART1, 115200, UART1_GP2);
    printf("\r\n-----------------|- LED test -|-----------------\r\n");
	GPIO_SetPinMux(LED_PORT, LED_PIN, GPIO_FUNCTION_0);
	GPIO_SetPinDir(LED_PORT, LED_PIN, GPIO_Mode_OUT);
	GPIO_ClearPin(LED_PORT, LED_PIN);
	while(1)
    {
        GPIO_ClearPin(LED_PORT, LED_PIN);
        delay_ms(3000);
         GPIO_SetPin(LED_PORT, LED_PIN);
        delay_ms(3000);
    }
}


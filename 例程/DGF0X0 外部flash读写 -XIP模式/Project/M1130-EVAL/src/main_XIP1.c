/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "m1130_uart.h" 
#include "m1130_gpio.h" 
#include "m1130_rcc.h"	 
#include "misc.h"
#include "m1130_dma.h"
#include "m1130_wdg.h"
#include "m1130_rtc.h"
#include "m1130_utility_rtc.h"
//#include "m1130_bitcopy.h"
#include "m1130_qspi.h"	 
#include "m1130_eval_qspi.h"
#include "m1130_utility_usart.h"

//#define FLASH_4BYTES_ADDR

int randomTest(void);

#define XIP1_MAPPING_ADDR	0x38000000
#define TEST_START_ADDR		0x8000			//512K
#define TEST_START_ADDR2	0x1080000		//16M+512K
#define TEST_DATA_SIZE		0x100
#define XIP_READ_ADDR      XIP1_MAPPING_ADDR+TEST_START_ADDR

__align(4) uint8_t writeBuf[TEST_DATA_SIZE]={0};
void testExit(void)
{
	int i;

	//ADDRESS_REMAP1
	RCC->ADDRESS_REMAP1 = 0;
	//clear MEMP:bit 25
	QSPI1->CTRL0_CLR = 0x22000000;

	//发送8个0x00, 退出连续读模式
	for(i=0;i<8;i++){
		writeBuf[i] = 0x00;
	}
    QSPI_CS_Low(QSPI1);
    qspi_flash_write_buf(QSPI1, writeBuf, 8);
    QSPI_CS_High(QSPI1);

	//CTRL1切换为单线，D2D3 pinmux切换到GPIO
	QSPI_ModeSet(QSPI1, QSPI_STD);
	QSPI_PinSwitch(QSPI1, 0);

	//write Flash
	for(i=0; i<TEST_DATA_SIZE; i++){
		writeBuf[i] = i+0x30;
	}
	qspi_flash_erase_block_64k(QSPI1, TEST_START_ADDR);
	qspi_flash_write(QSPI1, writeBuf, TEST_START_ADDR, TEST_DATA_SIZE);

#ifdef FLASH_4BYTES_ADDR
	for(i=0; i<TEST_DATA_SIZE; i++){
		writeBuf[i] = i+0x60;
	}
	qspi_flash_erase_block_64k(QSPI1, TEST_START_ADDR2);
	qspi_flash_write(QSPI1, writeBuf, TEST_START_ADDR2, TEST_DATA_SIZE);
#endif

	//D2D3 pinmux切换到QSPI,CTRL1保持单线状态，XIP自动切换到4线
	QSPI_PinSwitch(QSPI1, 1);

	//set MEMP:bit 25
	QSPI1->CTRL0_SET = 0x02000000;
	//ADDRESS_REMAP1
	RCC->ADDRESS_REMAP1 = 1;
}
void start_xip1(void)
{
   qspi_flash_quad_function_select(QSPI1, QUAD_ENABLE, 0x02);	//ESMT needn't it
#ifdef FLASH_4BYTES_ADDR
	qspi_flash_4Byte_address_enable(QSPI1, ENABLE);
#endif
	
	qspi_flash_write_enable(QSPI1, 1);	
	qspi_flash_global_unprotect(QSPI1);
	
#ifdef FLASH_4BYTES_ADDR
	qspi_flash_erase_block_64k(QSPI1, TEST_START_ADDR2);
	for(i=0;i<TEST_DATA_SIZE;i++){
		tempBuf[i] = (i+0x10)&0xFF;
	}
	qspi_flash_write(QSPI1, tempBuf, TEST_START_ADDR2, TEST_DATA_SIZE);
#endif

	//init XIP1
	RCC_SETCLKDivider(RCC_CLOCKFREQ_QSPI1CLK, 2);
	QSPI_PinSwitch(QSPI1, 1);
	QSPI_ModeSet(QSPI1, QSPI_STD);

#ifdef FLASH_4BYTES_ADDR
	QSPI1->XIP_READ_CMD = 0xEC;
	QSPI1->ADD_BYTE_LEN = 1;
#else
	QSPI1->XIP_READ_CMD = 0xEB;
	QSPI1->ADD_BYTE_LEN = 0;
#endif

	QSPI1->DUMMY = 2;		
	QSPI1->START_ADDR = 0;		//QSPI1 = 0;
	QSPI1->CTRL2 = 0x2001;			
	QSPI1->BUSY_DLY = 0x101;
	//bit13 DMA_ENABLE=0
	QSPI_DMACmd(QSPI1, DISABLE);
	//bit26 WRITE
	QSPI1->CTRL0_CLR = 1<<26;
	//bit28 HALF_DUPLEX=1
	QSPI1->CTRL0_SET = 1<<28;
	//bit25 MEMAP=1
	QSPI1->CTRL0_SET = 0x0a000000;
	//ADDRESS_REMAP1
	RCC->ADDRESS_REMAP1 = 1;
}

__align(4) uint8_t tempBuf[TEST_DATA_SIZE]={0};
int main()
{
	int i;
	
   UART_Configuration(UART1, 115200, UART1_GP2);
	printf("Start Test YYY\r\n");

	qspi_flash_init(QSPI1, 4, QSPI_MASTER_MODE, 2, 0, QSPI1_GP3, 0);
   	//write Flash
	for(i=0; i<TEST_DATA_SIZE; i++){
		writeBuf[i] = i+0x30;
	}
	qspi_flash_erase_block_64k(QSPI1, TEST_START_ADDR);
	qspi_flash_write(QSPI1, writeBuf, TEST_START_ADDR, TEST_DATA_SIZE);

   start_xip1();

   int printIndex;
	uint8_t* pRead;
	pRead = (uint8_t*)XIP_READ_ADDR;
	for(printIndex=0; printIndex<TEST_DATA_SIZE; printIndex++){
		printf("%x:0x%x\r\n",printIndex,pRead[printIndex]);

	}
 	testExit();
  
	for(i=0;i<TEST_DATA_SIZE;i++){
		uint8_t data8=inb(XIP1_MAPPING_ADDR+TEST_START_ADDR+i);
		printf("0x:%x\r\n",data8);
		if(data8 != i){
      printf("test error\r\n");
			while(1);
		}
	}
	while(1);
}



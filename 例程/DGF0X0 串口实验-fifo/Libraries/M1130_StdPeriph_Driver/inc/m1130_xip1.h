/**
  ******************************************************************************
  * @file    m1130_adc.h
  * @author  Alpscale Application Team
  * @version V1.0.1
  * @date    27-12-2018
  * @brief   This file contains all the functions prototypes for the XIP firmware 
  *          library
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __M1130_XIP_WRITE_H
#define __M1130_XIP_WRITE_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "m1130.h"
  
void initXIP1(uint8_t addr4Bytes);

#ifdef __cplusplus
}
#endif

#endif /*__M1130_XIP_WRITE_H */

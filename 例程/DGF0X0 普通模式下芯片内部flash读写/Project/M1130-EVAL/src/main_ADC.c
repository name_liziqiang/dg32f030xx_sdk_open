/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "m1130_uart.h" 
#include "m1130_gpio.h" 
#include "m1130_rcc.h"	 
#include "misc.h"
#include "m1130_dma.h"
#include "m1130_adc.h"
#include "m1130_wdg.h"
#include "m1130_utility_usart.h"


#define ADCx                ADC0
#define ADCx_IRQn           ADC0_IRQn
#define ADCx_IRQHandler     ADC0_IRQHandler
#define AHBCLK_BIT_ADCx     AHBCLK_BIT_ADC0
#define ADC_Channelx        ADC_CHANNEL_ALL

#define ADC_TRAG_MODE       ADC_TrigEdge_Software//ADC_TrigEdge_Rising //ADC_TrigEdge_Falling//ADC_TrigEdge_RisingFalling //ADC_TrigEdge_Software 
#define ADC_TRAG_SRC        ADC_External_TrigSrc_GPIO5_2
//ADC_Internal_TrigSrc_TIM4_CH1//ADC_Internal_TrigSrc_TIM1_CH4//ADC_Internal_TrigSrc_TIM15_CH1//ADC_Internal_TrigSrc_TIM16_CH1

#define TIMx                TIM16
#define AHBCLK_BIT_TIMx     AHBCLK_BIT_TIM16


void TIMx_Init(void)
{
  RCC_SetAHBCLK(1 << AHBCLK_BIT_TIMx, ENABLE);
  TIMx->ARR       = 10000;
  TIMx->CCR1      = 5000;
  TIMx->CCR4      = 5000;
  TIMx->CNT       = 0;
  TIMx->PSC       = 599;
  TIMx->CCMR1    |= 6 << 4;  //PWM1 mode
  TIMx->CCMR1    |= 6 << 12;
  TIMx->CCMR2    |= 6 << 4;  //PWM1 mode
  TIMx->CCMR2    |= 6 << 12;
  TIMx->CCER     |= TIM_CCER_CC1E;
  // TIMx->CCER     |= TIM_CCER_CC2E;
  // TIMx->CCER     |= TIM_CCER_CC3E;
  TIMx->CCER     |= TIM_CCER_CC4E;
  TIMx->BDTR      = (3 << 0) | TIM_BDTR_MOE;
  TIMx->CR1      |= TIM_CR1_CEN;
}

void Adc_Config(void)
{
  ADC_InitTypeDef ADC_InitStruct;
  NVIC_InitTypeDef NVIC_InitStruct;


  ADC_InitStruct.ADC_Channel = ADC_Channelx;
  ADC_InitStruct.ADC_ClkDiv = 4;
  ADC_InitStruct.ADC_Exch_DisSampleNum = 0;
  ADC_InitStruct.ADC_First_DisSampleNum = 0;
  ADC_InitStruct.ADC_SampFrqSel = ADC_571K_SampFrq;//采样率
  ADC_InitStruct.ADC_SingleTrigConvMode = ADC_SingleTrigConv_Disable;//连续触发,一个触发信号转换所有调度通道
  ADC_InitStruct.ADC_TrigConvEdge = ADC_TRAG_MODE;//触发模式
  ADC_InitStruct.ADC_TrigConvSrcSel = ADC_TRAG_SRC;//触发源
  ADC_Init(ADCx, &ADC_InitStruct);
  ADC_ITConfig(ADCx, ENABLE);
  //ADC_SetAccumulate(ADCx, ADC_Channelx, 3);
  
  NVIC_InitStruct.NVIC_IRQChannel = ADCx_IRQn;
  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
  NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
  NVIC_Init(&NVIC_InitStruct);
  
  ADC_Run(ADCx, ENABLE);
}

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main()
{
  UART_Configuration(UART1, 115200, UART1_GP2);
  printf("\r\n-----------------|- m1130 Standard Peripheral Library Driver -|-----------------\r\n");

  // GPIO_SetPinMux(GPIO0, GPIO_Pin_18, GPIO_FUNCTION_0);//gp2_2
  // GPIO_SetPinDir(GPIO0, GPIO_Pin_18, GPIO_Mode_IN);
  
  // GPIO_SetPinMux(GPIO0, GPIO_Pin_19, GPIO_FUNCTION_0);//gp2_3
  // GPIO_SetPinDir(GPIO0, GPIO_Pin_19, GPIO_Mode_IN);

  GPIO_SetPinMux(GPIO1, GPIO_Pin_10, GPIO_FUNCTION_0);//gp5_2
  GPIO_SetPinDir(GPIO1, GPIO_Pin_10, GPIO_Mode_IN);
  
  GPIO_SetPinMux(GPIO1, GPIO_Pin_11, GPIO_FUNCTION_0);//gp5_3
  GPIO_SetPinDir(GPIO1, GPIO_Pin_11, GPIO_Mode_OUT);
  
//  Adc_Config();
  TIMx_Init();
  while(1)
  {
//    if(ADC_TRAG_MODE == ADC_TrigEdge_Software){
//      ADC_StartSoftwareTrigConv(ADCx); /*software trigger test*/
//    }
//    else {
//      if(ADC_TRAG_SRC == ADC_External_TrigSrc_GPIO5_2){
//        GPIO_SetPin(GPIO1, GPIO_Pin_11);
//        commonDelay(1000);
//        GPIO_ClearPin(GPIO1, GPIO_Pin_11);
//        commonDelay(1000);
//        continue;
//      }
//    }
    commonDelay(400000);
  }
}


/**
  * @brief  This function handles ADC Handler.
  * @param  None
  * @retval None
  */
uint32_t val = 0;
uint32_t flag_val = 0;
void ADCx_IRQHandler(void)
{
  #if 1
  int i = 0;
  uint32_t adcon;
  /*get irq flag*/
  for(i=0; i<10; i++)
  {
    if(ADC_GetITStatus(ADCx, 1 << i))
    {
      /*get channel4 converted value*/
      adcon = ADC_GetConvValue(ADCx, 1 << i);
      printf("ch%d=%04d ", i, adcon);
    }
  //*((uint32_t *)((uint32_t)&ADCx->CH0_CLR + 0x10*i)) = 0x3ffff;
    /* clear it flag */
    ADC_ClearITFlag(ADCx, 1 << i);
  }
  printf("\r\n");
  #else
  ADCx->CTRL1_CLR = 0x3FF;
  #endif
}


/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "m1130_uart.h" 
#include "m1130_gpio.h" 
#include "m1130_rcc.h"	 
#include "misc.h"
#include "m1130_dma.h"
#include "m1130_wdg.h"
#include "m1130_rtc.h"
#include "m1130_utility_rtc.h"
#include "m1130_qspi.h"	 
#include "m1130_eval_qspi.h"
#include "m1130_utility_usart.h"
#include "m1130_tim.h"



#define FLASH_TEST_ADDR	0x30000	
#define TEST_DATA_SIZE		1024
//注意，如果使用了DMA，buff需要四字节对齐，定义时加上修饰符__align(4)
__align(4) uint8_t writeBuf[TEST_DATA_SIZE]={0};
__align(4) uint8_t tempBuf[TEST_DATA_SIZE]={0};
int main()
{

	 
   UART_Configuration(UART1, 115200, UART1_GP2);
	printf("Start Test XXX\r\n");

   
   	for(int i=0;i<TEST_DATA_SIZE;i++){
		writeBuf[i] = (i+5)&0xff;
	}
	qspi_flash_erase_block_4k(QSPI0,FLASH_TEST_ADDR);//擦除扇区
   qspi_flash_write(QSPI0,writeBuf,FLASH_TEST_ADDR,TEST_DATA_SIZE);//写flash
   qspi_flash_read(QSPI0,tempBuf,FLASH_TEST_ADDR,TEST_DATA_SIZE);//读flash
   for(int i=0;i<TEST_DATA_SIZE;i++){
	 printf("tempBuf[%x]= 0x%x",i,tempBuf[i]);
    printf("\r\n");
	}	

	printf("End Test XXX\r\n");
	while(1);
}



/**
  ******************************************************************************
  * @file    m1130_eval.h
  * @author  Alpscale Application Team
  * @version V1.0.0
  * @date    16-October-2013
  * @brief   This file contains definitions for ALPSCALE_EVAL's hardware resources.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alpscale</center></h2>
  ******************************************************************************  
  */ 
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __M1130_EVAL_H
#define __M1130_EVAL_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "m1130.h"
   


void QSPI_LowLevel_DeInit(QSPI_TypeDef* QSPIptr);
void QSPI_IRQ_Num_Enable(QSPI_TypeDef *QSPIptr);
int QSPI_LowLevel_DMA_TxConfig(QSPI_TypeDef* QSPIptr, uint8_t *BufferSRC, int bytes);
int QSPI_LowLevel_DMA_RxConfig(QSPI_TypeDef* QSPIptr, uint8_t *BufferDST, int bytes);


/**
  * @}
  */
  
#ifdef __cplusplus
}
#endif

#endif /* __M1130_EVAL_H */
/**
  * @}
  */ 

/**
  * @}
  */

/**
  * @}
  */  

/******************* (C) COPYRIGHT 2013 Alpscale *****END OF FILE****/

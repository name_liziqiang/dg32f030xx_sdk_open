/**
  ******************************************************************************
  * @file    m1130_wdg.h
  * @author  Alpscale Software Team
  * @version V1.0.0
  * @date    12/20/2013
  * @brief   This file contains all the functions prototypes for the WDG 
  *          firmware library.
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 Alpscale</center></h2>
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __M1130_RTC_H
#define __M1130_RTC_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "m1130.h"


/** @addtogroup m1130_StdPeriph_Driver
  * @{
  */

/** @addtogroup RTC
  * @{
  */

#define RTC_IT_SEC         ((uint32_t)0x00000002)
#define RTC_IT_ALARM       ((uint32_t)0x00000004)
#define IS_RTC_IT(IT)      ((((IT) & 0xFFFFFFF9) == 0) && ((IT) != 0))
#define IS_RTC_GET_IT(IT)  (((IT) == RTC_IT_ALARM) || \
                           ((IT) == RTC_IT_SEC))


#define RTC_CLKSOURCE_LSE  ((uint32_t)0x00000000)
#define RTC_CLKSOURCE_IRC  ((uint32_t)0x00000020)
#define RTC_CLKSOURCE_OSC  ((uint32_t)0x00000030)
#define IS_RTC_CLKSOURCE(CLKSOURCE)  (((CLKSOURCE) == RTC_CLKSOURCE_LSE) || \
                                      ((CLKSOURCE) == RTC_CLKSOURCE_IRC) || \
																			((CLKSOURCE) == RTC_CLKSOURCE_OSC))


#define RTC_FLAG_SEC       ((uint32_t)0x00000001)
#define RTC_FLAG_ALARM     ((uint32_t)0x00000002)
#define IS_RTC_CLEAR_FLAG(FLAG)  ((((FLAG) & 0xFFFFFFFC) == 0) && ((FLAG) != 0))
#define IS_RTC_GET_FLAG(FLAG)    (((FLAG) == RTC_FLAG_SEC) || (FLAG) == RTC_FLAG_ALARM)


void RTC_CLKSET(FunctionalState NewState);
void RTC_Reset(void);
void RTC_ITConfig(uint32_t RTC_IT, FunctionalState NewState);
void RTC_ClockSelect(uint32_t RTC_CLKSOURCE);
void RTC_Cmd(FunctionalState NewState);
void RTC_EnterConfigMode(void);
void RTC_ExitConfigMode(void);
uint32_t RTC_GetCounter(void);
void RTC_SetCounter(uint32_t CounterValue);
uint32_t RTC_GetAlarm(void);
void RTC_SetAlarm(uint32_t AlarmValue);
FlagStatus RTC_GetFlagStatus(uint16_t RTC_FLAG);
void RTC_ClearFlag(uint16_t RTC_FLAG);
ITStatus RTC_GetITStatus(uint16_t RTC_IT);
void RTC_ClearITPendingBit(uint16_t RTC_IT);


#ifdef __cplusplus
}
#endif

#endif /* __M1130_WDG_H */
/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/******************* (C) COPYRIGHT 2013 Alpscale *****END OF FILE****/


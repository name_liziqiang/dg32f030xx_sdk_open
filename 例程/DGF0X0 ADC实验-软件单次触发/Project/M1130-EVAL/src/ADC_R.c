#include "ADC_R.h"
#include "m1130_adc.h"
#include "m1130_gpio.h"
#include "m1130_rcc.h"
#include "stdio.h"
#include "m1130_tim.h"
#include "m1130_rcc.h"	 

uint16_t adbuf[16];
extern void delay_ms(uint16_t ms);
uint16_t last_channel;
 void myADC_Init(ADC_TypeDef *ADCx)
{
	ADC_InitTypeDef ADC_InitStruct;

	ADC_InitStruct.ADC_Channel =ADC_CHANNEL2|ADC_CHANNEL3;//使用通道2和通道3
	ADC_InitStruct.ADC_SingleTrigConvMode = ADC_SingleTrigConv_Disable;
	ADC_InitStruct.ADC_TrigConvSrcSel = ADC_Internal_TrigSrc_TIM1_CH4;
	ADC_InitStruct.ADC_TrigConvEdge =  ADC_TrigEdge_Software;//配置为软件触发
	ADC_InitStruct.ADC_First_DisSampleNum = 0;//第一次采用丢弃前面三个采样
	ADC_InitStruct.ADC_Exch_DisSampleNum =0;//切换通道 丢弃前面三个采样
	ADC_InitStruct.ADC_SampFrqSel = ADC_285K_SampFrq;
	ADC_InitStruct.ADC_ClkDiv = 12;
	ADC_Init(ADC0,&ADC_InitStruct);  
	ADC_Run(ADC0, ENABLE);
}

uint32_t Get_ADCVAL(uint16_t channel )
{
    ADC0->CTRL0=channel;	
    if(channel!=last_channel)                                 //adc 转换通道时，第一次触发需要触发一下前一个通道，并丢弃这个数据
    {
         ADC_StartSoftwareTrigConv(ADC0);//软件触发一次转换
         while((ADC0->CTRL1 & (last_channel )) != (last_channel ));//等待转换结束	
         ADC_ClearITFlag(ADC0, last_channel);
    }    
        ADC_StartSoftwareTrigConv(ADC0);//软件触发一次转换
        while((ADC0->CTRL1 & (channel )) != (channel ));//等待转换结束	
        ADC_ClearITFlag(ADC0, channel);
        uint32_t ad1_res= ADC_GetConvValue(ADC0, channel);
        last_channel=channel;
        return ad1_res;
}

void ADC_Test()
{
    for(int i=0;i<8;i++)
    {
      adbuf[i]=Get_ADCVAL(ADC_CHANNEL3);  
    }
    for(int i=0;i<8;i++)
    {
       printf("Adcval of channel3 adbuf[%d] is %04d\r\n",i,adbuf[i]); 
    }
    printf("\r\n");
    
    for(int i=0;i<8;i++)
    {
        adbuf[i]=Get_ADCVAL(ADC_CHANNEL2);
    }
    
      for(int i=0;i<8;i++)
    {
         printf("Adcval of channel2 adbuf[%d] is %04d\r\n",i,adbuf[i]);
    }
        printf("\r\n");
    delay_ms(4000);
    delay_ms(4000);
}

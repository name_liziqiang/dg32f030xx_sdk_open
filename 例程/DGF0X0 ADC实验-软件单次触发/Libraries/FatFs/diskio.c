/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2012        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control module to the FatFs module with a defined API.        */
/*-----------------------------------------------------------------------*/

#include "diskio.h"		/* FatFs lower layer API */
//#include "usbdisk.h"	/* Example: USB drive control */
//#include "atadrive.h"	/* Example: ATA drive control */
#include "ffconf.h"
#include "part.h"

/* Definitions of physical drive number for each media */
#define ATA		0
#define MMCSD		1
#define USB		2


int U_ReadBlock(uint32_t sector,uint8_t *buff,uint8_t count);
int U_WriteBlock(uint32_t sector,uint8_t *buff,uint8_t count);

/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/*-----------------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
extern block_dev_desc_t *stor_dev;


DSTATUS disk_initialize (
	BYTE drv				/* Physical drive nmuber (0..) */
)
{
 int Status = 0;
	
 switch (drv) 
 {
	case 0 :
	  
    if(Status == 0)
	  return 0;
    else 
	  return STA_NOINIT;
	
	case 1 :		
		return STA_NOINIT;
		  
	case 2 :
		return STA_NOINIT;
  }
	return STA_NOINIT;
}



/*-----------------------------------------------------------------------*/
/* Get Disk Status                                                       */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status (
	BYTE drv		/* Physical drive nmuber (0..) */
)
{
    switch (drv) 
	{
	  case 0 :
		
	  /* translate the reslut code here	*/

	    return 0;

	  case 1 :
	
	  /* translate the reslut code here	*/

	    return 0;

	  case 2 :
	
	  /* translate the reslut code here	*/

	    return 0;

	  default:

        break;
	}
	return STA_NOINIT;
}



/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */

DRESULT disk_read (
	BYTE drv,		/* Physical drive nmuber (0..) */
	BYTE *buff,		/* Data buffer to store read data */
	DWORD sector,	/* Sector address (LBA) */
	BYTE count		/* Number of sectors to read (1..255) */
)
{
  int Status;
  
  if( !count )
  {    
    return RES_PARERR;  /* count不能等于0，否则返回参数错误 */
  }

  switch (drv)
  {
	case 0:	
			if(count==1)            /* 1个sector的读操作 */      
		    {       
			  Status = U_ReadBlock( sector ,buff , 1);
		    }                                                
		    else                    /* 多个sector的读操作 */     
		    {   
		      Status = U_ReadBlock( sector , buff ,count);
		    }                                                
		    if(Status == 0)
		    {
		      return RES_OK;
		    }
		    else
		    {
		      return RES_ERROR;
		    }
	  //break;
	case 1:
   	  break;
		
    case 2:	
	  break;

    default:
      break;

  }
  
  return RES_ERROR;
}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if _USE_WRITE
DRESULT disk_write (
	BYTE drv,			/* Physical drive nmuber (0..) */
	const BYTE *buff,	        /* Data to be written */
	DWORD sector,		/* Sector address (LBA) */
	BYTE count			/* Number of sectors to write (1..128) */
)
{
  int Status;
  
  if( !count )
  {    
    return RES_PARERR;  /* count不能等于0，否则返回参数错误 */
  }

  switch (drv)
  {
    
    case 0:
			if(count==1)            /* 1个sector的写操作 */      
		    {   
		       Status = U_WriteBlock( sector , (unsigned char *)(&buff[0]),1 ); //todo
		    }                                                
		    else                    /* 多个sector的写操作 */    
		    {   
		       Status = U_WriteBlock( sector , (unsigned char *)(&buff[0]) , count );	//todo  
		    }                                                
		    if(Status == 0)
		    {
		       return RES_OK;
		    }
		    else
		    {
		       return RES_ERROR;
		    }
	  // break;
	   
	case 1:
		break;
		
    default :
       break;
  }
 return RES_ERROR;
}
#endif /* _READONLY */



/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

#if _USE_IOCTL
DRESULT disk_ioctl (
	BYTE drv,		/* Physical drive nmuber (0..) */
	BYTE ctrl,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
{	  

	
	switch (ctrl) 
	{
	  case CTRL_SYNC :
	      
		return RES_OK;

	  case GET_SECTOR_COUNT : 
	  	switch(drv)
			{
			case 0:
					*(DWORD*)buff = stor_dev->lba;
				break;
			case 1:
	  			break;
			default:
				break;
	  		}
	    return RES_OK;

	  case GET_BLOCK_SIZE :
	  	switch(drv)
			{
			case 0:
					*(DWORD*)buff = stor_dev->blksz;
				break;
			case 1:
	  			break;
			default:
				break;
	  		}
	    
	    return RES_OK;	

	  case CTRL_POWER :
		break;

	  case CTRL_LOCK :
		break;

	  case CTRL_EJECT :
		break;

      /* MMC/SDC command */
	  case MMC_GET_TYPE :
		break;

	  case MMC_GET_CSD :
		break;

	  case MMC_GET_CID :
		break;

	  case MMC_GET_OCR :
		break;

	  case MMC_GET_SDSTAT :
		break;	
	}
	return RES_PARERR;   
}
#endif

/* 得到文件Calendar格式的建立日期,是DWORD get_fattime (void) 逆变换 */							
/*-----------------------------------------------------------------------*/
/* User defined function to give a current time to fatfs module          */
/* 31-25: Year(0-127 org.1980), 24-21: Month(1-12), 20-16: Day(1-31) */                                                                                                                                                                                                                                          
/* 15-11: Hour(0-23), 10-5: Minute(0-59), 4-0: Second(0-29 *2) */                                                                                                                                                                                                                                                
DWORD get_fattime (void)
{
	/*该函数需要和硬件时钟配合，获得当前的年月日时分秒返回给文件系统，该示例固定返回2012年1月1日0点0分0秒*/

	return	  ((DWORD)(2012 - 1980) << 25)	/* Year = 2012 */
			| ((DWORD)1 << 21)				/* Month = 1 */
			| ((DWORD)1 << 16)				/* Day_m = 1*/
			| ((DWORD)0 << 11)				/* Hour = 0 */
			| ((DWORD)0 << 5)				/* Min = 0 */
			| ((DWORD)0 >> 1);				/* Sec = 0 */

}

/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "m1130_uart.h" 
#include "m1130_gpio.h" 
#include "m1130_rcc.h"	 
#include "misc.h"
#include "m1130_dma.h"
#include "m1130_wdg.h"
#include "m1130_utility_usart.h"



void WDG_Configuration(void)
{
	WDT_CLKSET(ENABLE);
	WDT_Reset();
	commonDelay(200);
	RCC_WDTCLKSel(RCC_WDTCLK_SOURCE_10KIRC);
	RCC_SETCLKDivider(RCC_CLOCKFREQ_WDTCLK,4);
	commonDelay(200);


	//WDG_SetMode(WDG_SETMODE_IT);			//IRQ
	WDG_SetMode(WDG_SETMODE_RESET);		//RESET
	
	WDG_SetReload(31250);							        /* reset time : 5s*/
	WDG_ReloadCounter();

}

void NMI_Handler(void)
{
	printf("hello\r\n");
	WDG_ClearTimeOutFlag();
	WDG_ReloadCounter();
}

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main()
{
	GPIO_SetPinMux(GPIO0, GPIO_Pin_0, GPIO_FUNCTION_0);
	GPIO_SetPinDir(GPIO0, GPIO_Pin_0, GPIO_Mode_OUT);
	GPIO_SetPin(GPIO0, GPIO_Pin_0);
  UART_Configuration(UART1, 115200, UART1_GP2);
	printf("\r\n-----------------|- m1130 Standard Peripheral Library Driver -|-----------------\r\n");
	WDG_Configuration();
	commonDelay(400000);
	GPIO_ClearPin(GPIO0, GPIO_Pin_0);
	
	//goSleep(SM_SLEEP);
	while(1)
	{
		WDG_ReloadCounter();
		commonDelay(400000);
	}
}


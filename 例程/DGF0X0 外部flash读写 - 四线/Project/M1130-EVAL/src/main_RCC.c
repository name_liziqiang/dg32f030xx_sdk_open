/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "m1130_uart.h" 
#include "m1130_gpio.h" 
#include "m1130_rcc.h"	 
#include "misc.h"
#include "m1130_dma.h"
#include "m1130_utility_usart.h"


void testCLKOUT(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC->OUTCLKSEL = 3;
	RCC->OUTCLKUEN = 0;
	RCC->OUTCLKUEN = 1;

	RCC->OUTCLKDIV = 1;
	//GP2_1
	GPIO_InitStructure.GPIO_Pin = 1<<17;
	GPIO_InitStructure.GPIO_Function = GPIO_FUNCTION_3;
	GPIO_Init(GPIO0, &GPIO_InitStructure);
}

void WKT_IRQHandler(void)
{
	uint32_t temp = RCC->STARTSRP0;
	RCC->STARTRSRP0 = temp;
	printf("WKT_IRQHandler\r\n");
}

//pinMask=0x2 , GP0_1 is wakeup pin
void enableWakeupPin(uint32_t pinMask)
{
	NVIC_InitTypeDef NVIC_InitStructure;

	RCC->STARTRSRP0 = pinMask;	//clean
	//RCC->STARTAPRP0 &= (~pinMask);	//falldown
	RCC->STARTAPRP0 |= pinMask;	  		//riseup
	RCC->STARTERP0 |= pinMask;	//enable
	
	NVIC_InitStructure.NVIC_IRQChannel = WKT_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

void testSysTick(void)
{
	RCC->SYSTICKCLKDIV = 40;
	*((uint32_t*)0xE000E010) = 0x1;		//enable SysTick
	//RCC->SYSTICKCAL = 0x3123456;
}


/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main()
{
  UART_Configuration(UART1, 115200, UART1_GP2);

	//testCLKOUT();
	//testSysTick();
	printf("hello\r\n");
	commonDelay(400000);
	
	enableWakeupPin(0x00000001);	//GP0_1
	
  RCC->PDAWAKECFG &= ~(1 << 4);
  RCC->PDSLEEPCFG &= ~(1 << 4);
  RCC->PDRUNCFG &= ~(1 << 4);
	goSleep(SM_DEEPSLEEP);

	printf("back\r\n");

	while(1)
	{
	}
}


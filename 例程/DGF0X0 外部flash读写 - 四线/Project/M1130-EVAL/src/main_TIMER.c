/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "m1130_uart.h" 
#include "m1130_gpio.h" 
#include "m1130_rcc.h"   
#include "misc.h"
#include "m1130_dma.h"
#include "m1130_wdg.h"
#include "m1130_tim.h"

/**
  * @brief  Configuare TIM.
  * @param  None
  * @retval None
  */
#define TIMx                TIM1
#define TIMx_IRQn           TIM1_IRQn
#define AHBCLK_BIT_TIMx     AHBCLK_BIT_TIM1
#define TIMx_IRQHandler     TIM1_IRQHandler

#define PINMUX_GROUP        1  //可以是1/2/3
#define SMCR_SMS            6 //可以是 0~7
#define SLAVER_TRGI         0 //0~7 触发输入选择
#define MASTER_TRGO         2  //0~7
#define COMP_OUT            1  //1=compare output; 0=input capture 在测试编码器模式1/2/3时，应该设置为input模式
#define CCMR_OCxCE_TEST     0  //CCMRx.OCxCE bit 测试, 0=disable; 1=enable
#define CCMR_OCxPE_TEST     0
#define CR1_ARPE_TEST       0  //CR1.ARPE bit 测试
#define CR1_OPM_TEST        0
#define CR1_UDIS_TEST       0
#define CR1_URS_TEST        0
#define CR2_CCUS_TEST       0
#define CR2_CCDS_TEST       0
#define CR2_CCPC_TEST       0
#define CR2_TI1S_TEST       0
#define SMCR_MSM_TEST       0
#define SMCR_ETC2_TEST      0 //外部时钟模式2 测试
#define TIM_DMA_TEST        0
#define DMA_CHANNEL_X       DMA_CHANNEL_3
#define TIM_CKD             TIM_CKD_DIV1  //CR1.CKD : TIM_CKD_DIV1/2/4
#define PWMCLK_MIX          0 //和PLL时钟混合后输出

#if (COMP_OUT == 0)
  #define PWM_INPUT         0 //输入模式时可设置是否使能PWM输入功能
#else
  #define PWM_INPUT         0
#endif

#if (SMCR_SMS == 1 || SMCR_SMS == 2 || SMCR_SMS == 3)
  #undef COMP_OUT
  #define COMP_OUT          0
#endif

TIM_TypeDef *ConnectList[5][4] =
{
  {TIM15, TIM16, TIM17, TIM4},
  {TIM15, TIM16, TIM17, TIM1},
  {TIM4, TIM16, TIM17, TIM1},
  {TIM15, TIM4, TIM17, TIM1},
  {TIM15, TIM16, TIM4, TIM1},
};


#define REGMEM_SIZE         7
const uint16_t RawRegMem[REGMEM_SIZE] = {
  0x0001, 0x3aa, 0x0003, 0x00c8, 0x00c7, 0x00c6, 0x00c5
};
uint16_t RegMem[REGMEM_SIZE];
/**
  * TIM DMA config
  */
void TIM_DMATransfer(TIM_TypeDef *TIM, DMAchannel channel, void *mem, int size, int dir)
{
  uint32_t temreg;
  DMAChannel_TypeDef *DMAChannel;
  DMAChannel = (DMAChannel_TypeDef *)(DMA_BASE + channel*0x58);

  DMA->DmaCfgReg = 1;
  temreg = 0x00000000 + (1 << DMA_CTRL_INT_EN)  //INT_EN, ch irq enable
                      + (1 << DMA_CTRL_DST_TR_WIDTH)      // DST_TR_WIDTH, des transfer width, should set to HSIZE, here is 000, means 8bit
                      + (1 << DMA_CTRL_SRC_TR_WIDTH)      // SRC_TR_WIDTH, sor transfer width, should set to HSIZE, here is 000, means 8bit
                      + (0 << DMA_DEST_MSIZE)     // DEST_MSIZE, des burst length, set to 001 means 4 DST_TR_WIDTH per burst transcation
                      + (0 << DMA_SRC_MSIZE)     // SRC_MSIZE, sor burst length, set to 001 means 4 SOR_TR_WIDTH per burst transcation
                      + (0 << DMA_LLP_DST_EN)     // LLP_DST_EN, des block chaining enable, set to 0 disable it
                      + (0 << DMA_LLP_SRC_EN) ;   // LLP_SOR_EN, sor block chaining enable, set to 0 disable it  
  
  if (TIM == TIM1)
  {
    DMAChannel->CFG_H = (REQ_TIM1 << DMA_CFG_SRC_PER) + (REQ_TIM1 << DMA_CFG_DEST_PER);
  }
  else if (TIM == TIM4)
  {
    DMAChannel->CFG_H = (REQ_TIM4 << DMA_CFG_SRC_PER) + (REQ_TIM4 << DMA_CFG_DEST_PER);
  }
  else if (TIM == TIM15)
  {
    RCC->DMA_CH_SEL |= 1 << 15;
    DMAChannel->CFG_H = (REQ_QSPI2RX_TIM15 << DMA_CFG_SRC_PER) + (REQ_QSPI2RX_TIM15 << DMA_CFG_DEST_PER);
  }
  else if (TIM == TIM16)
  {
    RCC->DMA_CH_SEL |= 1 << 0;
    DMAChannel->CFG_H = (REQ_UART0TX_TIM16 << DMA_CFG_SRC_PER) + (REQ_UART0TX_TIM16 << DMA_CFG_DEST_PER);
  }
  else
  {
    RCC->DMA_CH_SEL |= 1 << 1;
    DMAChannel->CFG_H = (REQ_UART0RX_TIM17 << DMA_CFG_SRC_PER) + (REQ_UART0RX_TIM17 << DMA_CFG_DEST_PER);
  }

  if (dir == 0)
  {
    temreg |= (2 << DMA_TTC_FC) | (0 << DMA_CTRL_DINC) | (2 << DMA_CTRL_SINC);
    DMAChannel->SAR = (uint32_t)(&TIM->DMAR);
    DMAChannel->DAR = (uint32_t)mem;
  }
  else
  {
    temreg |= (1 << DMA_TTC_FC) | (2 << DMA_CTRL_DINC) | (0 << DMA_CTRL_SINC);
    DMAChannel->DAR = (uint32_t)(&TIM->DMAR);
    DMAChannel->SAR = (uint32_t)mem;
  }
  DMAChannel->CFG_L &= ~((uint32_t)1 << 31 | (uint32_t)1 << 30);
  //DMAChannel->CFG_L |= ((uint32_t)1 << 31 | (uint32_t)1 << 30);
  DMAChannel->LLP = 0;
  DMAChannel->CTL_H = size;
  DMAChannel->CTL_L = temreg;

  DMA->ChEnReg = 0x101 << channel;
}

/**
  * Master Timer config 
  */
void MasterTIM_Config(TIM_TypeDef *TIM)
{
  TIM_BDTRInitTypeDef TIM_BDTRInitStruct;
  TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
  TIM_OCInitTypeDef TIM_OCInitStruct;
  TIM_ICInitTypeDef TIM_ICInitStruct;

  if (TIM == TIM1)
  {
    RCC_SetAHBCLK(1 << AHBCLK_BIT_TIM1, ENABLE);
    GPIO_SetPinMux(GPIO0, GPIO_Pin_11, GPIO_FUNCTION_2);     //GP1_3
    GPIO_SetPinMux(GPIO0, GPIO_Pin_10, GPIO_FUNCTION_2);     //GP1_2
  }
  else if (TIM == TIM4)
  {
    RCC_SetAHBCLK(1 << AHBCLK_BIT_TIM4, ENABLE);
    GPIO_SetPinMux(GPIO0, GPIO_Pin_2, GPIO_FUNCTION_3);      //GP0_2
    GPIO_SetPinMux(GPIO0, GPIO_Pin_7, GPIO_FUNCTION_2);      //GP0_7
  }
  else if (TIM == TIM15)
  {
    RCC_SetAHBCLK(1 << AHBCLK_BIT_TIM15, ENABLE);
    GPIO_SetPinMux(GPIO0, GPIO_Pin_6, GPIO_FUNCTION_2);     //GP0_6
    GPIO_SetPinMux(GPIO0, GPIO_Pin_5, GPIO_FUNCTION_2);     //GP0_5
  }
  else if (TIM == TIM16)
  {
    RCC_SetAHBCLK(1 << AHBCLK_BIT_TIM16, ENABLE);
    GPIO_SetPinMux(GPIO0, GPIO_Pin_22, GPIO_FUNCTION_3);    //GP2_6
  }
  else
  {
    RCC_SetAHBCLK(1 << AHBCLK_BIT_TIM17, ENABLE);
    GPIO_SetPinMux(GPIO0, GPIO_Pin_21, GPIO_FUNCTION_3);    //GP2_5
  }

  #if CR1_UDIS_TEST
  TIM_UpdateDisableConfig(TIM, ENABLE);
  #endif

  TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
  TIM_TimeBaseInitStruct.TIM_CounterMode   = TIM_CounterMode_Up;
  TIM_TimeBaseInitStruct.TIM_Period        = 1000;
  TIM_TimeBaseInitStruct.TIM_Prescaler     = 47;
  TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0;
  TIM_SelectOutputTrigger(TIM, MASTER_TRGO << 4);  //可更改TRGO
  TIM_TimeBaseInit(TIM, &TIM_TimeBaseInitStruct);
  /*清除更新中断标志（避免第一次立即进入中断）*/
  TIM_ClearITPendingBit(TIM, TIM_IT_Update);

  TIM_OCInitStruct.TIM_OCMode              = TIM_OCMode_PWM1;
  TIM_OCInitStruct.TIM_OCIdleState         = TIM_OCIdleState_Reset;
  TIM_OCInitStruct.TIM_OCNIdleState        = TIM_OCNIdleState_Reset;
  TIM_OCInitStruct.TIM_OutputState         = TIM_OutputState_Enable;
  TIM_OCInitStruct.TIM_OutputNState        = TIM_OutputNState_Enable;
  TIM_OCInitStruct.TIM_OCNPolarity         = TIM_OCNPolarity_High;
  TIM_OCInitStruct.TIM_OCPolarity          = TIM_OCPolarity_High;
  TIM_OCInitStruct.TIM_Pulse               = 200;
  TIM_OC1Init(TIM, &TIM_OCInitStruct);

  if(TIM != TIM4)
  {
    TIM_CtrlPWMOutputs(TIM, ENABLE);
  }

  #if SMCR_MSM_TEST
  TIM_SelectMasterSlaveMode(TIM, TIM_MasterSlaveMode_Enable);
  TIM_SelectSlaveMode(TIM, TIM_SlaveMode_Trigger);
  TIM_ICInitStruct.TIM_Channel = TIM_Channel_1;
  TIM_ICInitStruct.TIM_ICFilter = 0;
  TIM_ICInitStruct.TIM_ICPolarity = TIM_ICPolarity_Rising;
  TIM_ICInitStruct.TIM_ICPrescaler = TIM_ICPSC_DIV1;
  TIM_ICInitStruct.TIM_ICSelection = TIM_ICSelection_DirectTI;
  TIM_ICInit(TIM, &TIM_ICInitStruct);
  TIM_SelectInputTrigger(TIM, TIM_TS_TI1F_ED);
  #else
  TIM_Cmd(TIM, ENABLE);
  #endif
}

/**
  * Normal Timer config
  */
void TIM_Configuration(void)
{
  TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
  TIM_OCInitTypeDef TIM_OCInitStruct;
  TIM_ICInitTypeDef TIM_ICInitStruct;
  TIM_BDTRInitTypeDef TIM_BDTRInitStruct;
  NVIC_InitTypeDef NVIC_InitStruct;
  
  if(TIMx == TIM1)
  {
    #if (PINMUX_GROUP == 1)
    /*TIM1 CH1*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_11, GPIO_FUNCTION_2);     //GP1_3
    /*TIM1 CH2*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_10, GPIO_FUNCTION_2);     //GP1_2
    /*TIM1 CH3*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_9, GPIO_FUNCTION_2);      //GP1_1
    /*TIM1 CH4*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_8, GPIO_FUNCTION_2);      //GP1_0
    /*TIM1 CH1N*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_27, GPIO_FUNCTION_2);     //GP3_3
    /*TIM1 CH2N*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_26, GPIO_FUNCTION_2);     //GP3_2
    /*TIM1 CH3N*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_25, GPIO_FUNCTION_2);     //GP3_1
    /*TIM1 BKIN*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_28, GPIO_FUNCTION_2);     //GP3_4
    /*TIM1 ETR*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_16, GPIO_FUNCTION_2);     //GP6_0
    #elif (PINMUX_GROUP == 2)
    /*TIM1 CH1*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_20, GPIO_FUNCTION_3);   //GP2_4
    /*TIM1 CH2*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_10, GPIO_FUNCTION_2);     //GP1_2
    /*TIM1 CH3*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_9, GPIO_FUNCTION_2);      //GP1_1
    /*TIM1 CH4*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_8, GPIO_FUNCTION_2);      //GP1_0
    /*TIM1 CH1N*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_27, GPIO_FUNCTION_2);     //GP3_3
    /*TIM1 CH2N*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_26, GPIO_FUNCTION_2);     //GP3_2
    /*TIM1 CH3N*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_25, GPIO_FUNCTION_2);     //GP3_1
    /*TIM1 BKIN*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_14, GPIO_FUNCTION_2);   //GP5_6
    /*TIM1 ETR*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_16, GPIO_FUNCTION_2);     //GP6_0
    #else
    /*TIM1 CH1*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_24, GPIO_FUNCTION_3);   //GP3_0
    /*TIM1 CH2*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_19, GPIO_FUNCTION_3);   //GP2_3
    /*TIM1 CH3*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_18, GPIO_FUNCTION_3);   //GP2_2
    /*TIM1 CH4*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_8, GPIO_FUNCTION_2);   //GP1_0
    /*TIM1 CH1N*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_11, GPIO_FUNCTION_2);   //GP5_3
    /*TIM1 CH2N*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_9, GPIO_FUNCTION_2);    //GP5_1
    /*TIM1 CH3N*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_8, GPIO_FUNCTION_2);    //GP5_0
    /*TIM1 BKIN*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_17, GPIO_FUNCTION_2);   //GP6_1
    /*TIM1 ETR*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_16, GPIO_FUNCTION_2);   //GP6_0
    #endif
  }
  else if(TIMx == TIM4)
  {
    #if (PINMUX_GROUP == 1)
    /*TIM4 CH1*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_2, GPIO_FUNCTION_3);      //GP0_2
    /*TIM4 CH2*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_7, GPIO_FUNCTION_2);      //GP0_7
    /*TIM4 CH3*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_30, GPIO_FUNCTION_2);     //GP3_6
    /*TIM4 CH4*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_29, GPIO_FUNCTION_2);     //GP3_5
    /*TIM4 ETR*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_13, GPIO_FUNCTION_2);     //GP1_5
    #elif (PINMUX_GROUP == 2)
    /*TIM4 CH1*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_31, GPIO_FUNCTION_2);   //GP3_7
    /*TIM4 CH2*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_12, GPIO_FUNCTION_2);   //GP1_4
    /*TIM4 CH3*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_30, GPIO_FUNCTION_2);     //GP3_6
    /*TIM4 CH4*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_29, GPIO_FUNCTION_2);     //GP3_5
    /*TIM4 ETR*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_13, GPIO_FUNCTION_2);     //GP1_5
    #else
    /*TIM4 CH1*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_10, GPIO_FUNCTION_2);   //GP5_2
    /*TIM4 CH2*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_26, GPIO_FUNCTION_3);   //GP7_2
    /*TIM4 CH3*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_25, GPIO_FUNCTION_3);   //GP7_1
    /*TIM4 CH4*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_24, GPIO_FUNCTION_3);   //GP7_0
    /*TIM4 ETR*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_27, GPIO_FUNCTION_3);   //GP7_3
    #endif
  }
  else if(TIMx == TIM15)
  {
    #if (PINMUX_GROUP == 1)
    /*TIM15 CH1*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_6, GPIO_FUNCTION_2);     //GP0_6
    /*TIM15 CH2*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_5, GPIO_FUNCTION_2);     //GP0_5
    /*TIM15 CH1N*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_8, GPIO_FUNCTION_3);     //GP1_0
    /*TIM15 BKIN*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_16, GPIO_FUNCTION_3);    //GP6_0
    #elif (PINMUX_GROUP == 2)
    /*TIM15 CH1*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_24, GPIO_FUNCTION_2);    //GP3_0
    /*TIM15 CH2*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_23, GPIO_FUNCTION_3);    //GP2_7
    /*TIM15 CH1N*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_8, GPIO_FUNCTION_3);     //GP1_0
    /*TIM15 BKIN*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_16, GPIO_FUNCTION_3);    //GP6_0
    #else
    /*TIM15 CH1*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_13, GPIO_FUNCTION_2);    //GP5_5
    /*TIM15 CH2*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_12, GPIO_FUNCTION_2);    //GP5_4
    /*TIM15 CH1N*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_31, GPIO_FUNCTION_2);    //GP7_7
    /*TIM15 BKIN*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_30, GPIO_FUNCTION_2);    //GP7_6
    #endif
  }
  else if(TIMx == TIM16)
  {
    #if (PINMUX_GROUP == 1)
    /*TIM16 CH1*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_22, GPIO_FUNCTION_3);    //GP2_6
    /*TIM16 CH1N*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_11, GPIO_FUNCTION_3);    //GP1_3
    /*TIM16 BKIN*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_29, GPIO_FUNCTION_3);    //GP7_5
    #else
    /*TIM16 CH1*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_9, GPIO_FUNCTION_3);     //GP5_1
    /*TIM16 CH1N*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_31, GPIO_FUNCTION_3);    //GP7_7
    /*TIM16 BKIN*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_22, GPIO_FUNCTION_2);    //GP2_6
    #endif
  }
  else if(TIMx == TIM17)
  {
    #if (PINMUX_GROUP == 1)
    /*TIM17 CH1*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_21, GPIO_FUNCTION_3);    //GP2_5
    /*TIM17 CH1N*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_23, GPIO_FUNCTION_2);    //GP2_7
    /*TIM17 BKIN*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_28, GPIO_FUNCTION_3);    //GP7_4
    #else
    /*TIM17 CH1*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_8, GPIO_FUNCTION_3);     //GP5_0
    /*TIM17 CH1N*/
    GPIO_SetPinMux(GPIO1, GPIO_Pin_30, GPIO_FUNCTION_3);    //GP7_6
    /*TIM17 BKIN*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_21, GPIO_FUNCTION_2);    //GP2_5
    #endif
  }
  RCC_SetAHBCLK(1<<AHBCLK_BIT_TIMx, ENABLE);
  
  TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD;
  TIM_TimeBaseInitStruct.TIM_CounterMode   = TIM_CounterMode_Up;
  TIM_TimeBaseInitStruct.TIM_Period        = 1000;
  TIM_TimeBaseInitStruct.TIM_Prescaler     = 47;
  TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0;
  TIM_TimeBaseInit(TIMx, &TIM_TimeBaseInitStruct);
  /*ARPE位测试*/
  #if CR1_ARPE_TEST
  TIM_ARRPreloadConfig(TIMx, ENABLE);
  TIM_SetAutoreload(TIMx, 2000);
  #endif

  #if CR2_CCPC_TEST
  TIM_CCPreloadControl(TIMx, ENABLE);
  #endif

  /*清除更新中断标志（避免第一次立即进入中断）*/
  TIM_ClearITPendingBit(TIMx, TIM_IT_Update);
  
#if COMP_OUT
  /*输出比较*/
  //有效电平模式
  //TIM_OCInitStruct.TIM_OCMode              = TIM_OCMode_Active;
  //无效电平模式
  //TIM_OCInitStruct.TIM_OCMode              = TIM_OCMode_Inactive;
  //翻转模式
  //TIM_OCInitStruct.TIM_OCMode              = TIM_OCMode_Toggle;
  //PWM1模式
  TIM_OCInitStruct.TIM_OCMode              = TIM_OCMode_PWM1;
  //PWM2模式
  //TIM_OCInitStruct.TIM_OCMode              = TIM_OCMode_PWM2;
  TIM_OCInitStruct.TIM_OCIdleState         = TIM_OCIdleState_Reset;
  TIM_OCInitStruct.TIM_OCNIdleState        = TIM_OCNIdleState_Reset;
  TIM_OCInitStruct.TIM_OutputState         = TIM_OutputState_Enable;
  TIM_OCInitStruct.TIM_OutputNState        = TIM_OutputNState_Enable;
  TIM_OCInitStruct.TIM_OCPolarity          = TIM_OCPolarity_High;
  TIM_OCInitStruct.TIM_OCNPolarity         = TIM_OCNPolarity_High;
  TIM_OCInitStruct.TIM_Pulse               = 200;
  TIM_OC1Init(TIMx, &TIM_OCInitStruct);
  if((uint32_t)TIMx <= (uint32_t)TIM15)
  {
    TIM_OC2Init(TIMx, &TIM_OCInitStruct);
    if((uint32_t)TIMx <= (uint32_t)TIM4)
    {
      TIM_OC3Init(TIMx, &TIM_OCInitStruct);
      TIM_OC4Init(TIMx, &TIM_OCInitStruct);
    }
  }

  #if CCMR_OCxCE_TEST
  /*CCMRx OCxCE位测试，ETR输入高OCxREF为无效*/
  if((uint32_t)TIMx <= (uint32_t)TIM4)
  {
    TIM_ClearOC1Ref(TIMx, TIM_OCClear_Enable);
    TIM_ClearOC2Ref(TIMx, TIM_OCClear_Enable);
    TIM_ClearOC3Ref(TIMx, TIM_OCClear_Enable);
    TIM_ClearOC4Ref(TIMx, TIM_OCClear_Enable);
  }
  #endif

  #if CCMR_OCxPE_TEST
  /*CCRx预装载位测试*/
  TIM_OC1PreloadConfig(TIMx, TIM_OCPreload_Enable);
  TIMx->CCR1 = 400;
  if((uint32_t)TIMx <= (uint32_t)TIM15)
  {
    TIM_OC2PreloadConfig(TIMx, TIM_OCPreload_Enable);
    TIMx->CCR2 = 400;
    if((uint32_t)TIMx <= (uint32_t)TIM4)
    {
      TIM_OC3PreloadConfig(TIMx, TIM_OCPreload_Enable);
      TIMx->CCR3 = 400;
      TIM_OC4PreloadConfig(TIMx, TIM_OCPreload_Enable);
      TIMx->CCR4 = 400;
    }
  }
  #endif

  /*刹车和死区设置*/
  if(TIMx != TIM4)
  {
    TIM_BDTRInitStruct.TIM_AutomaticOutput = TIM_AutomaticOutput_Enable;  //AOE
    TIM_BDTRInitStruct.TIM_Break           = TIM_Break_Disable;  //BKE
    TIM_BDTRInitStruct.TIM_BreakPolarity   = TIM_BreakPolarity_High;  //BKP
    TIM_BDTRInitStruct.TIM_DeadTime        = 0x00;
    TIM_BDTRInitStruct.TIM_LOCKLevel       = TIM_LOCKLevel_OFF;
    TIM_BDTRInitStruct.TIM_OSSIState       = TIM_OSSIState_Enable;
    TIM_BDTRInitStruct.TIM_OSSRState       = TIM_OSSRState_Enable;
    TIM_BDTRConfig(TIMx, &TIM_BDTRInitStruct);
  }
  
  if(TIMx != TIM4)
  {
    TIM_CtrlPWMOutputs(TIMx, ENABLE);
  }
  
#else
  /*输入捕获测试*/
  TIM_ICInitStruct.TIM_Channel             = TIM_Channel_1; //Channel_1:CCMR1_CC1S, IC1PSC, IC1F  Channel_2:CCMR1_CC2S, IC1PSC, IC2F
  TIM_ICInitStruct.TIM_ICFilter            = 0x00;
  TIM_ICInitStruct.TIM_ICPolarity          = TIM_ICPolarity_Rising; //Rising:CCER_CCxP=0 Falling:CCER_CCxP=1
  TIM_ICInitStruct.TIM_ICPrescaler         = TIM_ICPSC_DIV1; //CCMR1_ICxPSC
  TIM_ICInitStruct.TIM_ICSelection         = TIM_ICSelection_DirectTI; //DirectTI:CCMR1_CCxS=01b  IndirectTI:CCMR1_CCxS=10b
  TIM_ICInit(TIMx, &TIM_ICInitStruct);

  #if CR2_TI1S_TEST
  TIM_SelectHallSensor(TIMx, ENABLE);
  #else

  if ((uint32_t)TIMx <= (uint32_t)TIM15)
  {
    TIM_ICInitStruct.TIM_Channel           = TIM_Channel_2; //Channel_1:CCMR1_CC1S, IC1PSC, IC1F  Channel_2:CCMR1_CC2S, IC1PSC, IC2F
    TIM_ICInit(TIMx, &TIM_ICInitStruct);
    if ((uint32_t)TIMx <= (uint32_t)TIM4)
    {
      TIM_ICInitStruct.TIM_Channel         = TIM_Channel_3; //Channel_1:CCMR1_CC1S, IC1PSC, IC1F  Channel_2:CCMR1_CC2S, IC1PSC, IC2F
      TIM_ICInit(TIMx, &TIM_ICInitStruct);
      TIM_ICInitStruct.TIM_Channel         = TIM_Channel_4;
      TIM_ICInit(TIMx, &TIM_ICInitStruct);  
    }
  }
  #endif
  
  #if PWM_INPUT
  TIM_ICInitStruct.TIM_Channel             = TIM_Channel_2;
  TIM_ICInitStruct.TIM_ICFilter            = 0x0C;
  TIM_ICInitStruct.TIM_ICPolarity          = TIM_ICPolarity_Falling;
  TIM_ICInitStruct.TIM_ICPrescaler         = TIM_ICPSC_DIV1;
  TIM_ICInitStruct.TIM_ICSelection         = TIM_ICSelection_IndirectTI;
  TIM_ICInit(TIMx, &TIM_ICInitStruct);
  #endif  
  
#endif
  
  #if CR2_CCUS_TEST
  TIM_SelectCOM(TIMx, ENABLE);
  #endif

  #if CR2_CCDS_TEST
  TIM_SelectCCDMA(TIMx, ENABLE);
  #endif

  #if CR1_OPM_TEST
  /*单脉冲模式*/
  TIM_SelectOnePulseMode(TIMx, TIM_OPMode_Single);
  #endif
  
  #if CR1_UDIS_TEST
  /*禁止更新事件*/
  TIM_UpdateDisableConfig(TIMx, ENABLE);
  #endif
  
  /*更新中断使能*/
  //TIM_ITConfig(TIMx, TIM_IT_Update, ENABLE);
  /*捕获比较1中断使能*/
  //TIM_ITConfig(TIMx, TIM_IT_CC1, ENABLE);
  /*捕获比较2中断使能*/
  //TIM_ITConfig(TIMx, TIM_IT_CC2, ENABLE);
  /*捕获比较3中断使能*/
  //TIM_ITConfig(TIMx, TIM_IT_CC3, ENABLE);
  /*捕获比较4中断使能*/
  //TIM_ITConfig(TIMx, TIM_IT_CC4, ENABLE);
  /*COM中断使能*/
  //TIM_ITConfig(TIMx, TIM_IT_COM, ENABLE);
  /*触发中断使能*/
  //TIM_ITConfig(TIMx, TIM_IT_Trigger, ENABLE);
  /*刹车中断使能*/
  //TIM_ITConfig(TIMx, TIM_IT_Break, ENABLE);

  #if CR1_URS_TEST  
  /*CR1 URS位测试，只有计数器溢出能产生中断或请求*/
  TIM_UpdateRequestConfig(TIMx, TIM_UpdateSource_Regular);
  #endif
  
  #if PWMCLK_MIX
  /*PWM & CLK位测试*/
  RCC->PWMCLKDIV = 96;
  TIMx->CR1 |= (uint32_t)1 << 31;
  #endif

  #if TIM_DMA_TEST
  /*TIM DMA test*/
  NVIC_InitStruct.NVIC_IRQChannel = DMA_IRQn;
  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
  NVIC_InitStruct.NVIC_IRQChannelPriority = 2;
  NVIC_Init(&NVIC_InitStruct);
  DmaChannelInterrupt(DMA_CHANNEL_X, 1);
  memcpy(RegMem, RawRegMem, sizeof(RegMem));
  TIM_DMAConfig(TIMx, TIM_DMABase_PSC, (REGMEM_SIZE-1) << 8);
  TIM_DMACmd(TIMx, TIM_DMA_CC1, ENABLE);
  TIM_DMATransfer(TIMx, DMA_CHANNEL_X, RegMem, REGMEM_SIZE, 1);
  
  #endif

  TIM_SelectInputTrigger(TIMx, SLAVER_TRGI << 4);

  #if (SMCR_SMS == 0)
  TIMx->SMCR &= ~0x07;
  #elif (SMCR_SMS == 1)
  TIM_EncoderInterfaceConfig(TIMx, TIM_EncoderMode_TI1, TIM_ICPolarity_Rising, TIM_ICPolarity_Rising);
  #elif (SMCR_SMS == 2)
  TIM_EncoderInterfaceConfig(TIMx, TIM_EncoderMode_TI2, TIM_ICPolarity_Rising, TIM_ICPolarity_Rising);
  #elif (SMCR_SMS == 3)
  TIM_EncoderInterfaceConfig(TIMx, TIM_EncoderMode_TI12, TIM_ICPolarity_Rising, TIM_ICPolarity_Rising);
  #elif (SMCR_SMS == 4)
  TIM_SelectSlaveMode(TIMx, TIM_SlaveMode_Reset);
  #elif (SMCR_SMS == 5)
  TIM_SelectSlaveMode(TIMx, TIM_SlaveMode_Gated);
  #elif (SMCR_SMS == 6)
  TIM_SelectSlaveMode(TIMx, TIM_SlaveMode_Trigger);
  #elif (SMCR_SMS == 7)
  TIM_SelectSlaveMode(TIMx, TIM_SlaveMode_External1);
  #endif

  if ((TIMx == TIM1) || (TIMx == TIM4))
  {
    #if SMCR_ETC2_TEST
    TIM_ETRClockMode2Config(TIMx, TIM_ExtTRGPSC_OFF, TIM_ExtTRGPolarity_NonInverted, 0x8);
    #else
    TIM_ETRConfig(TIMx, TIM_ExtTRGPSC_OFF, TIM_ExtTRGPolarity_NonInverted, 0x08);
    #endif
  }

  NVIC_InitStruct.NVIC_IRQChannel = TIMx_IRQn;
  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
  NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
  NVIC_Init(&NVIC_InitStruct);

  #if (SMCR_SMS != 6)
  TIM_Cmd(TIMx, ENABLE);
  #endif
  
  #if (SLAVER_TRGI < 4)
  if (TIMx == TIM1)
  {
    MasterTIM_Config(ConnectList[0][SLAVER_TRGI]);
  }
  else if (TIMx == TIM4)
  {
    MasterTIM_Config(ConnectList[1][SLAVER_TRGI]);
  }
  else if (TIMx == TIM15)
  {
    MasterTIM_Config(ConnectList[2][SLAVER_TRGI]);
  }
  else if (TIMx == TIM16)
  {
    MasterTIM_Config(ConnectList[3][SLAVER_TRGI]);
  }
  else if (TIMx == TIM17)
  {
    MasterTIM_Config(ConnectList[4][SLAVER_TRGI]);
  }
  #endif
}

void GPIO_Config(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  RCC_SetAHBCLK(1<<AHBCLK_BIT_IOCON,ENABLE);

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
  GPIO_InitStructure.GPIO_Function = GPIO_FUNCTION_0;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_Init(GPIO0, &GPIO_InitStructure);
  GPIO0->DT_CLR = 1 << 1;
}

/**
  * @brief  Main program.asd
  * @param  None
  * @retval None
  */

int main()
{
  GPIO_Config();
  commonDelay(40000);
  TIM_Configuration();
  commonDelay(4000);

  //GP0_1信号给timer输入
  GPIO0->DT_SET = 1 << 1; //↑
  commonDelay(19*7);
  GPIO0->DT_CLR = 1 << 1; //↓
  commonDelay(4000);
  GPIO0->DT_SET = 1 << 1; //↑
  commonDelay(4000);
  GPIO0->DT_CLR = 1 << 1; //↓
  commonDelay(4000);
  GPIO0->DT_SET = 1 << 1; //↑
  commonDelay(4000);
  GPIO0->DT_CLR = 1 << 1; //↓
  commonDelay(4000);
  GPIO0->DT_SET = 1 << 1; //↑
  commonDelay(4000);
  GPIO0->DT_CLR = 1 << 1; //↓
  commonDelay(4000);
  GPIO0->DT_SET = 1 << 1; //↑
  commonDelay(4000);
  GPIO0->DT_CLR = 1 << 1; //↓

  while(1)
  {
    /*产生更新事件*/
    //TIM_GenerateEvent(TIMx, TIM_EventSource_Update);
    /*产生捕获比较1事件*/
    //TIM_GenerateEvent(TIMx, TIM_EventSource_CC1);
    /*产生捕获比较2事件*/
    //TIM_GenerateEvent(TIMx, TIM_EventSource_CC2);
    /*产生捕获比较3事件*/
    //TIM_GenerateEvent(TIMx, TIM_EventSource_CC3);
    /*产生捕获比较4事件*/
    //TIM_GenerateEvent(TIMx, TIM_EventSource_CC4);
    /*产生COM事件*/
    //TIM_GenerateEvent(TIMx, TIM_EventSource_COM);
    /*产生触发事件*/
    //TIM_GenerateEvent(TIMx, TIM_EventSource_Trigger);
    /*产生刹车事件*/
    //TIM_GenerateEvent(TIMx, TIM_EventSource_Break);
//    GPIO0->DT_TOG = 1 << 2;
  }
}

uint8_t cnt = 0;
void TIMx_IRQHandler(void)
{
  if(TIM_GetITStatus(TIMx, TIM_IT_Update) && (TIMx->DIER & TIM_DIER_UIE))
  {
    TIM_ClearITPendingBit(TIMx, TIM_IT_Update);
  }
  if(TIM_GetITStatus(TIMx, TIM_IT_CC1) && (TIMx->DIER & TIM_DIER_CC1IE))
  {
    cnt++;
    TIM_ClearITPendingBit(TIMx, TIM_IT_CC1);
  }
  if(TIM_GetITStatus(TIMx, TIM_IT_CC2) && (TIMx->DIER & TIM_DIER_CC2IE))
  {
    cnt++;
    TIM_ClearITPendingBit(TIMx, TIM_IT_CC2);
  }
  if(TIM_GetITStatus(TIMx, TIM_IT_CC3) && (TIMx->DIER & TIM_DIER_CC3IE))
  {
    cnt++;
    TIM_ClearITPendingBit(TIMx, TIM_IT_CC3);
  }
  if(TIM_GetITStatus(TIMx, TIM_IT_CC4) && (TIMx->DIER & TIM_DIER_CC4IE))
  {
    cnt++;
    TIM_ClearITPendingBit(TIMx, TIM_IT_CC4);
  }
  if(TIM_GetITStatus(TIMx, TIM_IT_COM) && (TIMx->DIER & TIM_DIER_COMIE))
  {
    TIM_ClearITPendingBit(TIMx, TIM_IT_COM);
  }
  if(TIM_GetITStatus(TIMx, TIM_IT_Trigger) && (TIMx->DIER & TIM_DIER_TIE))
  {
    TIM_ClearITPendingBit(TIMx, TIM_IT_Trigger);
  }
  if(TIM_GetITStatus(TIMx, TIM_IT_Break) && (TIMx->DIER & TIM_DIER_BIE))
  {
    TIM_ClearITPendingBit(TIMx, TIM_IT_Break);
  }
}

void DMA_IRQHandler(void)
{
  int i;
  DmaChannelClearInterruptStatus(DMA_CHANNEL_X);
  DmaChannelInterrupt(DMA_CHANNEL_X, 0);
  memset(RegMem, 0x00, sizeof(RegMem));
  TIM_DMATransfer(TIMx, DMA_CHANNEL_X, RegMem, REGMEM_SIZE, 0);
  TIM_GenerateEvent(TIMx, TIM_EGR_COMG);
  while(DMA->ChEnReg & (1 << DMA_CHANNEL_X));
  for (i=0; i<REGMEM_SIZE; i++)
  {
    if (RegMem[i] != RawRegMem[i])
    {
      while(1);  //DMA读写错误
    }
  }
}


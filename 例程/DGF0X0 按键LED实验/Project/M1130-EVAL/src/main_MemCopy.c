/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include <string.h>
#include <stdio.h>
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "m1130_uart.h" 
#include "m1130_gpio.h" 
#include "m1130_rcc.h"     
#include "misc.h"
#include "m1130_dma.h"
#include "m1130_wdg.h"
#include "m1130_qspi.h"
#include "m1130_cmp.h"
#include "m1130_adc.h"
#include "m1130_rtc.h"
#include "m1130_eval_i2c.h"
#include "m1130_led.h"
#include "m1130_bitcopy.h"

#define DMA_TRAN_SIZE	64

__align(4) uint32_t sData[DMA_TRAN_SIZE];
__align(4) uint32_t dData[DMA_TRAN_SIZE];

int mem2mem(void)
{
	uint32_t temreg;	
	int channelready;	
	int ret;
	DMAChannel_TypeDef *__dmaChannel;

	if(DmaIsBusy(DMA_CHANNEL_0)==CHANNEL_BUSY)
	{
		DmaStop(DMA_CHANNEL_0);
	}
	
	ret = DmaInit(DMA_CHANNEL_0);
	if(ret!=0)
	{	
		printf("startDmaCopy fail\r\n");
		return -1;
	}

	__dmaChannel = (DMAChannel_TypeDef *)(DMA_BASE + 0x58 * DMA_CHANNEL_0);
	temreg = 0x0 + (0<<DMA_CTRL_INT_EN)
			     + (2<<DMA_CTRL_DST_TR_WIDTH)
			     + (2<<DMA_CTRL_SRC_TR_WIDTH)
			     + (0<<DMA_CTRL_DINC)
			     + (0<<DMA_CTRL_SINC)
			     + (1<<DMA_DEST_MSIZE)
			     + (1<<DMA_SRC_MSIZE)
			     + (0<<DMA_TTC_FC)//MEM TO MEM
			     + (0<<DMA_LLP_DST_EN)
			     + (0<<DMA_LLP_SRC_EN);

	__dmaChannel->SAR = (uint32_t)sData;
	__dmaChannel->DAR = (uint32_t)dData;
	__dmaChannel->CTL_L = temreg;
	__dmaChannel->CTL_H = DMA_TRAN_SIZE;
	__dmaChannel->LLP = 0;

	DmaStartRoutine(DMA_CHANNEL_0, 1);
	while(DmaIsBusy(DMA_CHANNEL_0)==CHANNEL_BUSY);

	return 0;
}


int main()
{
	uint32_t temp32;
	int i;
	//д�Ĵ���
	outl(0x10,0x40000090);
	
	//���Ĵ���
	temp32 = inl(0x40000090);

	//���ԼĴ����ǲ���ĳ��ֵ��ʧ��д��SIM_FLAG
	testReg32(0x40000090, 0xA);

	//���ԼĴ�����ĳЩλ��ʧ��д��SIM_FLAG
	testMask32(0x40000090, 0xFF, 0xA);

	//���Ĵ�����1bit��Ϊ1, +4��ʾд1
	outl(0x10,0x40000010+4);

	//���Ĵ�����1bit��Ϊ0, +8��ʾ��0
	outl(0x10,0x40000010+8);

	//����pinmux : GPIO0��GPIO_Pin_18 = GP2_2       FUNC_2 = UART1_RX
	GPIO_SetPinMux(GPIO0, GPIO_Pin_18, GPIO_FUNCTION_2);
	//����pinmux : GPIO0��GPIO_Pin_19 = GP2_3       FUNC_2 = UART1_TX
	GPIO_SetPinMux(GPIO0, GPIO_Pin_19, GPIO_FUNCTION_2);

	//����GP7_0Ϊģ���
	GPIO_ConfigAna(GPIO1, GPIO_Pin_24, ENABLE);

	//����GP7_0Ϊ����
	GPIO_ConfigPull(GPIO1, GPIO_Pin_24, GPIO_PULL_UP);

	//����GP7_0Ϊ��������1
	GPIO_ConfigDriver(GPIO1, GPIO_Pin_24, GPIO_DRIVER_1);

	GPIO_ConfigSlewRate(GPIO1, GPIO_Pin_24, GPIO_SLEWRATE_SLOW);

	//DMA�������ڴ浽�ڴ�
	memset(sData,0,DMA_TRAN_SIZE);
	for (i = 0; i < DMA_TRAN_SIZE; i++)
	{
		sData[i] = 0x1 + i;
	}
	mem2mem();
	
	while (1);
}




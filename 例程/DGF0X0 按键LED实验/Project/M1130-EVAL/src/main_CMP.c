/**
  ******************************************************************************
  * @file    Project/M1800-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "m1130_uart.h" 
#include "m1130_gpio.h" 
#include "m1130_rcc.h"	 
#include "misc.h"
#include "m1130_dma.h"
#include "m1130_adc.h"
#include "m1130_cmp.h"
#include "m1130_utility_usart.h"

	
#define CMP_Channel   CMP_Channel_1	//CMP_Channel_0

void CMP_Configuration(void)
{
	CMP_InitTypeDef  CMP_InitStruct;

	CMP_InitStruct.Channel = CMP_Channel;
	CMP_InitStruct.Hysen = DISABLE;
	CMP_InitStruct.SelectNegitave = ENABLE;
	CMP_Init(&CMP_InitStruct);
}

void CMP_IRQHandler(void)
{
	printf("res = %d\r\n", CMP_GetCompareResult(CMP_Channel));
	CMP_ClearITFlag(CMP_Channel);
}

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main()
{
	UART_Configuration(UART1, 115200, UART1_GP2);
	
	CMP_Configuration();
	printf("\r\n-----------------|- M1800 Standard Peripheral Library Driver -|-----------------\r\n");

	while(1)
	{
	}
}

